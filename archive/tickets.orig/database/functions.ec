/*
##############################################################################
##############################################################################
##                                                                          ##
##                                                                          ##
##      Copyright (c) 1991 by BBN Systems and Technologies, A Division of   ##
##      Bolt Beranek and Newman, Inc.                                       ##
##                                                                          ##
##      Permission to use, copy, modify, and distribute this software and   ##
##      its documentation for non-commercial purposes and without fee is    ##
##      hereby granted, provided that the above copyright notice and this   ##
##      permission appear in all copies and in supporting documentation,    ##
##      and that the name of Bolt Beranek and Newman Inc.  not be used in   ##
##      advertising or publicity pertaining to distribution of the          ##
##      software without specific, written prior permission. BBN makes no   ##
##      representations about the suitability of this software for any      ##
##      purposes.  It is provided "AS IS" without express or implied        ##
##      warranties.                                                         ##
##                                                                          ##
##                                                                          ##
##############################################################################
##############################################################################
**
**   ESQL utility functions -- functions.ec
**
**              Depends on:
**
**		     Informix "esql/c" database package
**
**                   A one-line mailer (e.g. /bin/mail or /usr/mmdf/lib/v6mail)
**			    -- Path of mail command compiled into mailto()
**
**              Called by:
**
**                   Various forms and reports including the NEARnet
**		            Ticket System form specification (tickets.per)
**
** *************************************************************************
*/

#include <sys/types.h>
#include <sys/errno.h>
#include <stdio.h>
#include <time.h>
#include <strings.h>
#include <ctype.h>
#include "ctools.h"
#include "decimal.h"
$include sqlca;

#define isstr(ptr) ((ptr) != 0 && (ptr)[0] != '\0')
#define NOTOK -1
#define FALSE 0
#define TRUE 1

valueptr timeofday();
valueptr username();
valueptr upcase();
valueptr mailto();
valueptr smailto();
valueptr mailit();
valueptr mailitfix();
valueptr mailitnl();
valueptr mailsend();
valueptr mailspace();
valueptr concat();
valueptr listdump();
valueptr getinfo();
valueptr getsiteinfo();
valueptr strlength();
valueptr clearmemory();
valueptr putmemory();
valueptr getmemory();
valueptr testmemory();
valueptr isnull();
valueptr truncdesc();
valueptr onetoken();
valueptr iaggregate();
valueptr laggregate();
valueptr daggregate();
valueptr sselect();
valueptr iselect();
valueptr fillinpattern();
valueptr substr();
valueptr flipaddr();
valueptr timethree();
valueptr patmatch();
/* valueptr localphone(); */

char *index();
char *strdup();
char *trimchar();
char *makestring();
char *compress();
char *replace_comma();
char *revdot();
FILE *pipeout;

static int itemcount = 0;
#define MEMORYSIZE 500
static char *keys[MEMORYSIZE];
static struct value vals[MEMORYSIZE];

struct ufunc userfuncs[] =
	{
/*	  "localphone", localphone, */
        "timethree", timethree,
	"timeofday", timeofday,
	"username", username,
	"upcase", upcase,
        "mailto", mailto,
        "smailto", smailto,
        "mailit", mailit,
	"mailitfix", mailitfix,
        "mailitnl", mailitnl,
        "mailsend", mailsend,
	"mailspace", mailspace,
	"concat", concat,
        "getinfo", getinfo,
	"getsiteinfo", getsiteinfo,
        "listdump", listdump,
        "strlength", strlength,
	"clearmemory", clearmemory,
	"putmemory", putmemory,
	"getmemory", getmemory,
	"testmemory", testmemory,
	"isnull", isnull,
        "onetoken", onetoken,
        "truncdesc", truncdesc,
	"iaggregate", iaggregate,
	"daggregate", daggregate,
	"laggregate", laggregate,
	"sselect", sselect,
	"iselect", iselect,
	"fillinpattern", fillinpattern,
        "substr", substr,
        "flipaddr", flipaddr,
        "patmatch", patmatch,
	0, 0
	};

extern int toerrno;
extern char *malloc ();

valueptr listdump(pnamev,emailv,listsv)
valueptr pnamev;
valueptr emailv;
valueptr listsv;
{
    char *pname;        
    char *email;
    char *lists;
    char *stop;
    char *end;
    char *start;
    char *ptr;
    int len;

    if (emailv->v_ind < 0)
	intreturn(1)
    if (listsv->v_ind < 0)
	intreturn(2)

    email=trimchar(emailv);

    if ((len=strlen(email)) == 0 || (len == 1 && *email==' ')) {
        free(email);
        intreturn(3)
    }
    lists=trimchar(listsv);    
    if ((len=strlen(lists)) == 0 || (len == 1 && *lists==' ')) {
	free(email);
	free(lists);
	intreturn(4)
    }

    pname=trimchar(pnamev);

    stop=lists-1;  /* as if the previous string ended just before lists */
    end=lists+strlen(lists); /* final null in original string */
    while ( (start=stop+1) < end ) { /* while start hasn't walked off end... */
        if ((stop=index(start,',')) == (char *) 0)
            stop=end;            /* this is the last one, stop is at end */
        *stop = '\0';            /* mark off this string */
	/* now start->stop is a mailing list name (possibly with whitespace */
	while (start < stop && isspace(*start))
	    start++;
	ptr=stop;
	while ( ptr >= start && isspace(*--ptr))
	    *ptr = '\0';

	if (*start == '-')
    	    printf ("nearnet%s:\"%s\" <%s>\n",start,pname,email);
        else
    	    printf ("%s:\"%s\" <%s>\n",start,pname,email);
    }
    free(pname);
    free(email);
    free(lists);
    intreturn(0)
}

valueptr getsiteinfo(key, field)
valueptr key;
valueptr field;
{
$   char buf[256];
$   char query[256];
    char *fieldp;
$   char *keyp;
    char *bufp;

    if (key->v_ind < 0)  /* is sitename null? */
        strreturn("", (short) 0)

    fieldp = trimchar(field);
    keyp = trimchar(key);
    
    if (strlen(fieldp) == 0)  /* is field blank? */
        strreturn("", (short) 0)

    sprintf(query, "select %s from sites where site=?", fieldp);

$   prepare qid1 from $query;

    if (sqlca.sqlcode) /* problem */
	strreturn("", (short) 0);

$   declare sitec cursor for qid1;
$   open sitec using $keyp;

    if (sqlca.sqlcode) /* problem */
	strreturn("Problem-With-Query", (short) 18)

$   fetch sitec into $buf;

    free(fieldp);
    free(keyp);

    if (sqlca.sqlcode) /* not found */
       strreturn("", (short) 0)

$   close sitec;

    if (sqlca.sqlcode) /* close failed */
       strreturn("", (short) 0)

    bufp=compress(buf,strlen(buf));
    strreturn(bufp, (short) strlen(bufp))
    /* bufp doesn't get freed :-( */
}




valueptr getinfo(key, field)
valueptr key;
valueptr field;
{
$   char buf[256];
$   char query[256];
    char *fieldp;
$   char *keyp;
    char *bufp;

    if (key->v_ind < 0)  /* is id null? */
        strreturn("", (short) 0)

    fieldp = trimchar(field);
    keyp = trimchar(key);
    
    if (strlen(fieldp) == 0)  /* is id blank? */
        strreturn("", (short) 0)

    sprintf(query, "select %s from people where id=?", fieldp);

$   prepare qid from $query;

    if (sqlca.sqlcode) /* problem */
	strreturn("", (short) 0);

$   declare person cursor for qid;
$   open person using $keyp;

    if (sqlca.sqlcode) /* problem */
	strreturn("Problem-With-Query", (short) 18)

$   fetch person into $buf;

    free(fieldp);
    free(keyp);

    if (sqlca.sqlcode) /* not found */
       strreturn("", (short) 0)

$   close person;

    if (sqlca.sqlcode) /* close failed */
       strreturn("", (short) 0)

    bufp=compress(buf,strlen(buf));
    strreturn(bufp, (short) strlen(bufp))
    /* bufp doesn't get freed :-( */
}

char *getstring(table, keyfield, key, field)
char *table;
char *keyfield;
char *key;
char *field;
{
$   char buf[256];
$   char query[256];
$   char *keyp;
    char *bufp;

    keyp = key;

    if (isstr(table) || isstr (keyfield) || isstr(key) || isstr(field))
        return((char *) 0);

    sprintf(query, "select %s from %s where %s=?", field, table, keyfield);

$   prepare query1 from $query;

    if (sqlca.sqlcode) /* problem */
	return("Problem-With-Query");

$   declare record1 cursor for query1;
$   open record1 using $keyp;

    if (sqlca.sqlcode) /* problem */
	return("Problem-With-Query");

$   fetch record1 into $buf;

    if (sqlca.sqlcode) /* not found */
       return((char *) 0);

$   close record1;

    if (sqlca.sqlcode) /* close failed */
        return("Problem-with-Query");

    bufp=compress(buf,strlen(buf));
    return(bufp);
}

putstring(table, keyfield, key, field, newstring)
char *table;
char *keyfield;
char *key;
char *field;
char *newstring;
{
$   char buf[256];
$   char query[256];
$   char *keyp;
    char *bufp;

    keyp = key;

    if (isstr(table) || isstr (keyfield) || isstr(key) || isstr(field) || 
	isstr(newstring))
        return(-1);

    sprintf(query, "UPDATE %s SET %s=\"%s\" where %s=?", table, field,
							 newstring, keyfield);

$   prepare query4 from $query;

    if (sqlca.sqlcode) /* problem */
       return(-2);

$   execute query4 using $keyp;

    if (sqlca.sqlcode) /* not found */
       return(-3);

    return(sqlca.sqlerrd[2]); /* number of rows processed */
}

getint(table, keyfield, key, field, valout)
char *table;
char *keyfield;
char *key;
char *field;
int *valout;
{
$   char buf[256];
$   char query[256];
$   char *keyp;
    char *bufp;
$   int  val;

    keyp = key;

    if (isstr(table) || isstr (keyfield) || isstr(key) || isstr(field) ||
   	(valout == (int *) 0))
        return(-1);

    sprintf(query, "select %s from %s where %s=?", field, table, keyfield);

$   prepare query2 from $query;

    if (sqlca.sqlcode) /* problem */
	return(-2);

$   declare record2 cursor for query2;
$   open record2 using $keyp;

    if (sqlca.sqlcode) /* problem */
	return(-3);

$   fetch record2 into $val;

    if (sqlca.sqlcode) /* not found */
       return(-4);

$   close record2;

    if (sqlca.sqlcode) /* close failed */
       return(-5);

    *valout = val;
    return(0);
}

putint(table, keyfield, key, field, newval)
char *table;
char *keyfield;
char *key;
char *field;
int newval;
{
$   char buf[256];
$   char query[256];
$   char *keyp;
    char *bufp;

    keyp = key;

    if (isstr(table) || isstr (keyfield) || isstr(key) || isstr(field))
        return(-1);

    sprintf(query, "UPDATE %s SET %s=%d where %s=?", table, field, 
						     newval, keyfield);

$   prepare query3 from $query;

    if (sqlca.sqlcode) /* problem */
	return(-2);

$   execute query3 using $keyp;

    if (sqlca.sqlcode) /* not found */
       return(-3);

    return(sqlca.sqlerrd[2]); /* number of rows processed */
}

valueptr timeofday()
{
    time_t seconds, time();
    char usertime[10], *getlogin();
    struct tm *timerec, *localtime();
    
    seconds=time((time_t *) 0);
    timerec=localtime(&seconds);

    sprintf(usertime, "%02d:%02d", timerec->tm_hour, timerec->tm_min);
    	               
    strreturn(usertime, (short) 5)
}

valueptr username()
{
    char *ptr;

    ptr=getlogin();

    strreturn(ptr, (short) strlen(ptr))
}

valueptr upcase(str)
valueptr str;
{
    char *ptr;

    ptr = trimchar(str);

    if (islower(*ptr))
	*ptr = toupper(*ptr);

    strreturn(ptr, (short) 1)
}

valueptr smailto(replyto, fromfield, recipients, subjecttext)
valueptr replyto;
valueptr fromfield;
valueptr recipients;
valueptr subjecttext;
{
    char command[512];
    char *recip;
    char str[512];

    recip = trimchar(recipients);
    replace_comma(recip);
    sprintf(command, "/bin/mail %s", recip);

    if ((pipeout = popen(command, "w")) == NULL) {
        intreturn(0)
	}
    else {
        sprintf(str, "To:       %s\nFrom:     %s \nSubject:  %s \nReply-to: %s\n\n",
	             trimchar(recipients), 
	             trimchar(fromfield), 
		     trimchar(subjecttext), 
		     trimchar(replyto));
        fputs(str, pipeout);
        intreturn(1)
    }
}    

valueptr mailto(replyto, fromfield, recipients, subjecttext)
valueptr replyto;
valueptr fromfield;
valueptr recipients;
valueptr subjecttext;
{
    char command[512];

    sprintf(command, "/usr/mmdf/lib/v6mail -f \"%s\"  -a \"%s\" --nl \"%s\" -s \"%s\"",
		     trimchar(fromfield),
		     trimchar(replyto),
		     trimchar(recipients), 
		     trimchar(subjecttext));

    if ((pipeout = popen(command, "w")) == NULL)
        intreturn(0)
    else
        intreturn(1)
}    

char *trimchar(val)     /* trim excess blanks and return ptr to copy of val */
valueptr val;
{
    return(compress(val->v_charp,val->v_len));
}

char *compress(str,len)  /* trim excess blanks and return ptr to copy of str */
register char *str;
register int len;
{ 
    register char *newstr;

    for (; (len > 1) && (*(str+len-1) == ' '); len--)
       ;

    if ((newstr = malloc ((unsigned) (len + 1))) == 0)
        return((char *) 0);

    strncpy(newstr,str,len);
    newstr[len] = '\0';

    return(newstr);
}

char *replace_comma(str)  /* replace commas with spaces */
register char *str;
{ 
    register char *newstr;
    register int i;
    i = 0;

    while (str[i++] != '\0') 
	if(str[i] == ',')
           str[i] = ' ';

}

valueptr mailspace(val)
valueptr val;
{
    register int spaces;

    if (val->v_ind < 0)
       intreturn(0)
    spaces=toint(val);
    if (toerrno != 0)    
       intreturn(0)
    while (spaces-- > 0)
	fputc(' ', pipeout);
    intreturn(0)
}

valueptr mailit(val)
valueptr val;
{
    char *str;

    str=makestring(val,0);
    fputs(str, pipeout);
    free(str);
    intreturn(0)
}

valueptr mailitfix(val,width)
valueptr val;
valueptr width;
{
    int iwidth;
    char *str;

    if (width->v_ind < 0)
       intreturn(0)
    iwidth=toint(width);
    if (toerrno != 0)    
       intreturn(0)
    str=makestring(val,iwidth);
    fputs(str, pipeout);
    free(str);
    intreturn(0)
}

/* This routing makes a printable string out of any random Informix variable */

char *makestring(val,width)
valueptr val;
int width;
{
    char temp[10];
    char *ptr;
    char str[512];
    dec_t decstruct;

    if (val->v_ind < 0)
        sprintf(str, "");
    else
        switch (val->v_type) {
    	    case SQLDATE:
    	        rfmtdate(val->v_long, "mm/dd/yy", temp);
    	        sprintf(str, "%s", temp);
    	        break;
     	     case SQLCHAR: 
    	        if (width) 
    	            sprintf(str, "%-*s", width, trimchar(val));
		else
		    sprintf(str, "%s", trimchar(val));

    	        break;
    	    case SQLSMINT:
		if (width)
                    sprintf(str, "%*d", width, val->v_int);
	        else
		    sprintf(str, "%d", val->v_int);
    	        break;
    	    case SQLINT:
    	    case SQLSERIAL:
		if (width)
                    sprintf(str, "%*ld", width, val->v_long);
	        else
		    sprintf(str, "%ld", val->v_long);
    	        break;
    	    case SQLSMFLOAT:
		if (width)
                    sprintf(str, "%*g", width, val->v_float);
	        else
		    sprintf(str, "%g", val->v_float);
    	        break;
    	    case SQLFLOAT:
		if (width)
                    sprintf(str, "%*g", width, val->v_double);
	        else
		    sprintf(str, "%g", val->v_double);
    	        break;
	    case SQLDECIMAL:
		todecimal(val,&decstruct);
		if (toerrno < 0) {
		    if (width)
                        sprintf(str, "%-*s", width,
				"???? (decimal conversion failure)");
		    else
                        sprintf(str, "%s", "???? (decimal conversion failure)");
		} else if (dectoasc(&decstruct, str, 10, -1) == -1) {
		    if (width)
                        sprintf(str, "%-*s", width,
				"???? (decimal conversion failure)");
		    else
                        sprintf(str, "%s", "???? (decimal conversion failure)");
		}
		if (ptr=index(str,' '))
		    *ptr = '\0';  /* bug in dectoasc (informix sucks) */   
		break;
    	    default:
                if (width)
	            sprintf(str, "%-*s %2d)", width-3, 
					"???? (type was",val->v_type);
		else
                    sprintf(str, "???? (type was %d)",val->v_type);
    	        break;
        }
    return(strdup(str));
}

valueptr mailitnl(val)
valueptr val;
{
    mailit(val);
    fprintf(pipeout, "\n");
    intreturn(0)
}

valueptr mailsend()
{
    if (pclose(pipeout) != 0)
        intreturn(0)
    else
        intreturn(1)
}

valueptr strlength(str)
valueptr str;
{
    intreturn(strlen(trimchar(str)))
}

valueptr concat(val1, val2)
valueptr val1;
valueptr val2;
{
    register char *str1;
    register char *str2;
    register char *str3;
    short len;

    str1=makestring(val1,0);
    str2=makestring(val2,0);
    len=strlen(str1)+strlen(str2);

    if ((str3 = malloc ((unsigned) (len+1))) == 0)
        strreturn("", (short) 0)

    strcpy(str3,str1);
    strcat(str3,str2);
    free(str1);
    free(str2);
    
    strreturn(str3, (short) len)
}

valueptr patmatch(val1, val2)
valueptr val1;
valueptr val2;
{
    register char *str1;
    register char *str2;
    short ret_val;

    str1=makestring(val1,0);
    str2=makestring(val2,0);
    ret_val = 0;

    if ((re_comp(str1) == 0) && (re_exec(str2) == 1))
      ret_val = 1;

    intreturn(ret_val)
}

char *
	strdup (str)
register char   *str;
{
    register char  *newptr,
		   *newstr;

    if ((newstr = malloc ((unsigned) (strlen (str) + 1))) == 0)
	return ((char *) 0);

    for (newptr = newstr; *newptr++ = *str++; );

    return (newstr);
}

    /* Clear the "memory" array by setting the itemcount back to 0 */

valueptr
	clearmemory()
{
    register int i;

#ifdef undef
    /* free string storage from previous entries */
    for (i=0; i < itemcount; i++)
	if (vals[i].v_type == SQLCHAR)
	    free(vals[i].v_charp);
#endif undef
    
    /* empty memory by setting the count of items stored to 0 */
    itemcount=0;

    intreturn(1)
}

    /* Store the value, val, in an array at location key (a la awk) */

valueptr
	putmemory(key,val)
valueptr key;
valueptr val;
{
    register char *keystr;
    register int valint, i;

    /* check input parameters; if null, return */
    if (key->v_ind < 0)
	intreturn(0)

    keystr=trimchar(key);

    /* Search for key in "memory" to update value if already there */
    for (i = 0; i < itemcount; i++)
        if (!strcmp(keys[i],keystr)) {
	    /* found! */
	    bcopy(val, (char *) &vals[i], (sizeof *val));
	    if (val->v_ind >= 0 && val->v_type == SQLCHAR)
    	        vals[i].v_charp = strdup(val->v_charp);
            intreturn(1)
        }

    /* not found; add it */
    if (itemcount == MEMORYSIZE)
	intreturn(0)
    keys[itemcount] = keystr;
    bcopy(val, (char *) &vals[itemcount], (sizeof *val));;
    if (val->v_ind >= 0 && val->v_type == SQLCHAR)
        vals[itemcount].v_charp = strdup(val->v_charp);
    itemcount++;
    intreturn(1)
}

valueptr
	getmemory(key)
valueptr key;
{    
        
    register char *keystr;
    register int i;

    /* check for null key */
    if (key->v_ind < 0)
	intreturn(0)

    keystr=trimchar(key);

    /* search for key in "memory"; return value */
    for (i = 0; i < itemcount; i++) {
        if (!strcmp(keys[i],keystr)) {
	    if (vals[i].v_ind < 0)
		strreturn("", (short) 0)
	    /* found! return raw value per ctools.h */
	    bcopy((char *) &vals[i], &retstack, (sizeof retstack));
	    if (vals[i].v_type == SQLCHAR)
    	        retstack.v_charp = strdup(vals[i].v_charp);
	    return(&retstack);
	}
    }

    /* not found; return -1 */
    intreturn(-1)
}

/* see if the value given is the same as that stored in memory */

valueptr
	testmemory(key,val)
valueptr key;
valueptr val;
{    
        
    valueptr memval;
    char *memstring;
    char *valstring;
    char *tmp;
    int retval;

    memval = getmemory(key); 

    /* check to see if key was missing in "memory" */
    if (memval->v_ind == 0 && memval->v_type == SQLINT && memval->v_int == -1)
	intreturn(-1)

    /* are both null? */
    if (memval->v_ind == -1 && val->v_ind == -1)
	intreturn(0)

    tmp=makestring(memval,0);
    memstring=compress(tmp,strlen(tmp));
    free(tmp);
    tmp=makestring(val,0);
    valstring=compress(tmp,strlen(tmp));
    free(tmp);

    retval=strcmp(memstring,valstring);

    free(memstring);
    free(valstring);

    intreturn(retval)
}

valueptr isnull(item)
valueptr item;
{   int len;
    char *ptr;

    if (item->v_ind < 0)
	intreturn(1)
    else if (item->v_type == SQLCHAR) {
	     len=strlen(ptr=trimchar(item));
	     (void) free(ptr);
	     if (len == 0)
		intreturn(1)
	     else
		intreturn(0)
	 }
    else
	 intreturn(0)
}

valueptr truncdesc(descv) /* return copy of string truncated at the first */
valueptr descv;	          /* of any of the characters "&+," or and/plus */
{
	char *ptr;
	char *desc;
	int col;

	desc = trimchar(descv);

	if (col=strindex(" plus ",desc) > 0)
	    desc[col] = '\0';
	if (col=strindex(" and ",desc) > 0)
	    desc[col] = '\0';
	if ((ptr=index(desc,'&')) > 0)
	    *ptr = '\0';
	if ((ptr=index(desc,'+')) > 0)
	    *ptr = '\0';
	if ((ptr=index(desc,',')) > 0)
	    *ptr = '\0';

	strreturn(desc, (short) strlen(desc))
}

valueptr onetoken(strv)   /* return copy of string with underscores in */
valueptr strv;            /* place of spaces */
{
	char *ptr;
	char *str;
	int col;

	str = trimchar(strv);
	ptr = str;

	while (*ptr) {
            if (isspace(*ptr))
		*ptr='_';
            ptr++;
     	}
	strreturn(str, (short) strlen(str))
}

valueptr
iaggregate(expr,table,keyfield,keyval)
valueptr expr;
valueptr table;
valueptr keyfield;
valueptr keyval;
{
$   char query[256];
    char *bufp;
$   int  ival;
    char *exprp;
    char *tablep;
    char *keyfieldp;
$   char *keyvalp;

    exprp = trimchar(expr);
    tablep = trimchar(table);
    keyfieldp = trimchar(keyfield);
    keyvalp = makestring(keyval,0);

    sprintf(query, "select %s from %s where %s=?", 
		   exprp, tablep, keyfieldp);

$   prepare query5 from $query;

    if (sqlca.sqlcode) /* problem */
	exit(-2);

$   declare record5 cursor for query5;
$   open record5 using $keyvalp;

    if (sqlca.sqlcode) /* problem */
	exit(-3);

$   fetch record5 into $ival;

    if (sqlca.sqlcode < 0) /* error */
       exit(-4);

    if (sqlca.sqlcode == SQLNOTFOUND)
	return(0);

$   close record5;

    if (sqlca.sqlcode) /* close failed */
       exit(-5);

    intreturn(ival)
}

valueptr
iselect(selstmt)
valueptr selstmt;
{
$   char query[256];
    char *selstmtp;
$   int  ival;

    selstmtp = trimchar(selstmt);
    strcpy(query, selstmtp);
    free(selstmtp);

$   prepare query8 from $query;

    if (sqlca.sqlcode) /* problem */
	exit(-2);

$   declare record8 cursor for query8;
$   open record8;

    if (sqlca.sqlcode) /* problem */
	exit(-3);

$   fetch record8 into $ival;

    if (sqlca.sqlcode < 0) /* error */
       exit(-4);

    if (sqlca.sqlcode == SQLNOTFOUND)
	return(0);

$   close record8;

    if (sqlca.sqlcode) /* close failed */
       exit(-5);

    intreturn(ival)
}

valueptr
sselect(selstmt)
valueptr selstmt;
{
$   char query[256];
    char *selstmtp;
$   string sval[1024];

    selstmtp = trimchar(selstmt);
    strcpy(query, selstmtp);
    free(selstmtp);

$   prepare query9 from $query;

    if (sqlca.sqlcode) /* problem */
	exit(-2);

$   declare record9 cursor for query9;
$   open record9;

    if (sqlca.sqlcode) /* problem */
	exit(-3);

$   fetch record9 into $sval;

    if (sqlca.sqlcode < 0) /* error */
       exit(-4);

    if (sqlca.sqlcode == SQLNOTFOUND)
	strreturn("", (short) 0);

$   close record9;

    if (sqlca.sqlcode) /* close failed */
       exit(-5);

    strreturn(sval, (short) strlen(sval))
}

valueptr
daggregate(expr,table,keyfield,keyval)
valueptr expr;
valueptr table;
valueptr keyfield;
valueptr keyval;
{
$   char query[256];
    char *bufp;
$   dec_t dval;
    char *exprp;
    char *tablep;
    char *keyfieldp;
$   char *keyvalp;

    exprp = trimchar(expr);
    tablep = trimchar(table);
    keyfieldp = trimchar(keyfield);
    keyvalp = makestring(keyval,0);

    sprintf(query, "select %s from %s where %s=?", 
		   exprp, tablep, keyfieldp);

$   prepare query6 from $query;

    if (sqlca.sqlcode) /* problem */
	exit(-2);

$   declare record6 cursor for query6;
$   open record6 using $keyvalp;

    if (sqlca.sqlcode) /* problem */
	exit(-3);

$   fetch record6 into $dval;

    if (sqlca.sqlcode < 0) /* error */
       exit(-4);

    if (sqlca.sqlcode == SQLNOTFOUND)
	return(0);

$   close record6;

    if (sqlca.sqlcode) /* close failed */
       exit(-5);

    decreturn(dval)
}

valueptr
laggregate(expr,table,keyfield,keyval)
valueptr expr;
valueptr table;
valueptr keyfield;
valueptr keyval;
{
$   char query[256];
$   long lval;

    char *exprp;
    char *tablep;
    char *keyfieldp;
$   char *keyvalp;

    exprp = trimchar(expr);
    tablep = trimchar(table);
    keyfieldp = trimchar(keyfield);
    keyvalp = makestring(keyval);

    sprintf(query, "select %s from %s where %s=?", 
		   exprp, tablep, keyfieldp);

$   prepare query7 from $query;

    if (sqlca.sqlcode) /* problem */
	exit(-2);

$   declare record7 cursor for query7;
$   open record7 using $keyvalp;

    if (sqlca.sqlcode) /* problem */
	exit(-3);

$   fetch record7 into $lval;

    if (sqlca.sqlcode < 0) /* error */
       exit(-4);

    if (sqlca.sqlcode == SQLNOTFOUND)
	return(0);

$   close record7;

    if (sqlca.sqlcode) /* close failed */
       exit(-5);

    lngreturn(lval)
}

strindex (str, target)           /* return column str starts in target */
register char   *str,
                *target;
{
    char *otarget;
    register short slen;

    for (otarget = target, slen = strlen (str); ; target++)
    {
        if (*target == '\0')
            return (-1);

        if (equal (str, target, slen))
            return (target - otarget);
    }
}

equal (str1, str2, length)
register char   *str1,
                *str2;
short              length;
{

    while (*str1++ == *str2++ && --length > 0);
    return (length == 0);
}

valueptr
fillinpattern(domain,psflag)
valueptr domain;
valueptr psflag;
{
$   char pattern[256];
    int col;
    char *domainp;
    char *psflagp;
$   string domainstr[256];
$   short pattlen;
    char *revnet;

    psflagp = trimchar(psflag);

    domainp = trimchar(domain);
    strcpy (domainstr, domainp);

    if ((col = strindex(".in-addr.arpa",domainp)) < 0)
	exit(-2);

    if (*psflagp == 'P') {
$       update zones set netpattern = NULL, patternlen = NULL where domain = $domainstr;

        if (sqlca.sqlcode) /* problem */
    	    exit(-3);

        if (sqlca.sqlcode == SQLNOTFOUND)
    	    exit(-5);

    } else {
        domainp[col] = '\0';
        revnet = revdot(domainp);
        sprintf(pattern,"%s.\*",revnet);
        pattlen = strlen(pattern);

$       update zones set netpattern = $pattern, patternlen = $pattlen where domain = $domainstr;

        if (sqlca.sqlcode) /* problem */
    	    exit(-3);

        if (sqlca.sqlcode == SQLNOTFOUND)
    	    exit(-5);
    }
}

char *
revdot(instr)
char *instr;
{
    char *incopy;
    char *outstr;
    char *inargv[6];
    int inargc;

    incopy = strdup(instr);
    outstr = strdup(instr);  /* just allocate the right sized string */

    if ((inargc = cstr2arg(incopy, 6, inargv, '.')) == -1 ) {
        printf("Parse error in address specification: %s\n", instr);
        exit(1);
    }

    outstr[0]='\0'; /* empty the output string */
    for (;inargc > 0; inargc--) {
        strcat(outstr,inargv[inargc-1]);
        if (inargc > 1) strcat(outstr,".");
    }

    (void) free(incopy);

    return(outstr);
}


/*  convert string into argument list
 *
 *  stash a pointer to each field into the passed array.
 *  any common seperators split the words.  extra white-space
 *  between fields is ignored.
 *
 *  if the separator is '=', then the current argument position in
 *  the array points to "=", the next the one is the key and the
 *  value follows it.  This permits detecting variable assignment,
 *  in addition to positional arguments.
 *      i.e.,  key=value ->  = key value
 *
 *  specially-interpreted characters:
 *      space, tab, double-quote, backslash, comma, equal, slash, period,
 *      semi-colon, colon, carriage return, and line-feed (newline).
 *      preceding a special char with a backslash removes its
 *      interpretation.  a backslash not followed by a special is used
 *      to preface an octal specification for one character
 *
 *      a string begun with double-quote has only double-quote and
 *      backslash as special characters.
 *
 *  a field which begins with semi-colon is interpreted as marking the
 *  rest of the line as a comment and it is skipped, as are blank lines
 */


/* Steve Kille
 * Modified version, which splits a string given a specific
 * separator
 */

extern int errno;

cstr2arg (srcptr, max, argv, dlmchar)/* convert srcptr to argument list */
	register char *srcptr;  /* source data */
	int max;                /* maximum number of permitted fields */
	char *argv[];           /* where to put the pointers */
	char dlmchar;           /* Delimiting character */
{
    char gotquote;      /* currently parsing quoted string */
    register int ind;
    register char *destptr;

    if (srcptr == 0)
    {
	errno = EINVAL;     /* emulate system-call failure */
	return (NOTOK);
    }

    for (ind = 0, max -= 2;; ind++)
    {
	if (ind >= max)
	{
	    errno = E2BIG;      /* emulate system-call failure */
	    return (NOTOK);
	}

	argv [ind] = srcptr;
	destptr = srcptr;

/**/

	for (gotquote = FALSE; ; )
	{
	    if (*srcptr == dlmchar)
	    {
		if (gotquote)
		{           /* don't interpret the char */
		    *destptr++ = *srcptr++;
		    continue;
		}

		srcptr++;
		*destptr = '\0';
		goto nextarg;
	    }
	    else
	    {
		switch (*srcptr)
		{
		    default:        /* just copy it                     */
			*destptr++ = *srcptr++;
			break;

		    case '\"':      /* beginning or end of string       */
			gotquote = (gotquote) ? FALSE : TRUE;
			srcptr++;   /* just toggle */
			break;

		    case '\\':      /* quote next character             */
			srcptr++;   /* just skip the back-slash         */
			if (*srcptr >= '0' && *srcptr <= '7')
			{
				*destptr = '\0';
				do
				    *destptr = (*destptr << 3) | (*srcptr++ - '0');
				while (*srcptr >= '0' && *srcptr <= '7');
				destptr++;
				break;
			}    /* otherwise DROP ON THROUGH */
			else
			     *destptr++ = *srcptr++;
			break;


		    case '\0':
			*destptr = '\0';
			ind++;
			argv[ind] = (char *) 0;
			return (ind);
		}
	    }
	}
    nextarg:
	continue;
    }
}

    
valueptr
substr(string,start,count)
valueptr string;
valueptr start;
valueptr count;
{
    char *stringp;
    int len;
    int startval;
    int countval;
   
    stringp = trimchar(string);

    if (start->v_ind < 0)
       strreturn("", (short) 0)
    if (count->v_ind < 0)
       strreturn("", (short) 0)

    startval=toint(start);
    if (toerrno != 0)    
       strreturn("", (short) 0)
    countval=toint(count);
    if (toerrno != 0)    
       strreturn("", (short) 0)

    if (startval > (len=strlen(stringp)))
       strreturn("", (short) 0)

    if ((startval-1+countval) > len)
	countval = len-startval+1;

    strreturn(stringp+startval-1, (short) countval)
}

valueptr
flipaddr(address)
valueptr address;
{
    char *instr;
    char *outstr;

    instr = trimchar(address);
    outstr = revdot(instr);
    (void) free(instr);

    strreturn(outstr, (short) strlen(outstr))
}

valueptr
timethree(timestr)
valueptr timestr;
{
    char *instr;
    char outstr[10];

    instr = trimchar(timestr);
    sprintf(outstr, "%03d", (int) (atoi(instr) * 0.417));
    (void) free(instr);

    strreturn(outstr, (short) strlen(outstr))
}
/* 
/* valueptr
/* localphone(phonestr)
/* valueptr phonestr;
/* {
/*     char *phone;
/*     char *ptr;
/* 
/*     phone = trimchar(phonestr);
/* 
/*     /* change parentheses to blanks */
/*     ptr = index(phone, '(');    
/*     if (ptr) *ptr=' ';    
/*     ptr = index(phone, ')');    
/*     if (ptr) *ptr=' ';
/*         
/*     /* eliminate extensions */
/*     ptr = index(phone, 'x');    
/*     if (ptr) *ptr='\0';
/*     ptr = index(phone, 'X');    
/*     if (ptr) *ptr='\0';
/*         
/* 
/*     /*         
/* 
/*     sprintf(outstr, "%03d", (int) (atoi(instr) * 0.417));
/*     (void) free(instr);
/* 
/*     strreturn(outstr, (short) strlen(outstr))
/* }
/* 
*/
