/*
 *                      finger_daemon.c
 *
 * This program is an adaptation of the MMDF SMTP daemon written by Doug
 * Kingston of BRL on 1/1/82 (the day of the NCP->TCP cutover for you
 * historians).  Mike Muus, also of BRL, also hda his hand in improving
 * the SMTP daemon.
 * 
 * Dan Long of BBN modified this to handle WHOIS requests for the CSNET
 * "User Name Server".
 *
 * Dan Long further modified this to handle FINGER requests for NEARnet's
 * customized finger server.
 *
 * Calling convention: finger_daemon -n ## /path/to/finger_server
 *
 * Where finger_server is a shell script or program which takes the 
 * following arguments:
 *
 *      finger_server remote-host-name-or-ip-address
 *
 * finger_server is run with its stdin and stdout set to the finger client on
 * the remote host.
 */

#include <stdio.h>
#include <ctype.h>
#include <setjmp.h>
#include <sys/types.h>
#include <errno.h>
#include <strings.h>

#define isstr(ptr) ((ptr) != 0 && (ptr)[0] != '\0')
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/wait.h>
#include <sys/ioctl.h>

#define FILENAMELEN 256         /* room for a file name with addons... */
#define HOSTNAMELEN 256         /* room for a full domain name */

extern  int errno;

struct sockaddr_in addr;

char    *Server = (char *) 0;   /* Actual server process, full path */
int     Maxconnections = 10;    /* Maximum simultaneous connections */
int     numconnections = 0;     /* Number of currently active connections */
int     debug = 0;              /* If nonzero, give verbose output on stderr */
char    errbuf[BUFSIZ];         /* Logging will be line buffered */
char    programid[FILENAMELEN]; /* identification string for log entries */

main (argc, argv)
int     argc;
char    **argv;
{
	register int    skt;
	int     pid;
	int     i;
	char    thishost[HOSTNAMELEN];
	char    workarea[32];
	struct  servent *sp;
	struct  hostent *hp;

	setbuf( stderr, errbuf );
	sprintf( programid, "%s(%5.5d): ", argv[0], getpid());
	gethostname( thishost, sizeof(thishost));       /* current hostname */
	if ((sp = getservbyname("finger", "tcp")) == NULL) {
		fprintf(stderr, "Cannot find service finger/tcp\n");
		exit(-1);
	}

	/*
	 * try to get full name for thishost
	 */
	if( (hp = gethostbyname(thishost)) != NULL)
	    strcpy(thishost, hp->h_name);

	/* Parse args; args will override configuration file... */
	arginit( argc, argv );

	addr.sin_addr.s_addr = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = (short)sp->s_port;

	if( (i = open("/dev/tty", 2)) >= 0) {
		(void) ioctl(i, TIOCNOTTY, (char *)0);
		(void) close(i);
	}

	/*
	 *  Create a socket to accept a SMTP connection on.
	 *  The doloop is due to some sort of IPC bug in BSD 4.1a.
	 */
	if (debug) log("opening socket...");
	i = 60;
	do{
		skt = socket( AF_INET, SOCK_STREAM, 0, 0 );
		if( debug ) log( "socket returned %d", skt);
		if( skt < 0 )
			sleep( 5 );
	} while( skt < 0 && i-- > 0 );
	if( skt < 0 ) {
		if (debug) log( "can't open socket (errno %d)", errno);
		exit (99);
	}
	if( debug ) log( "socket open on %d", skt );

	if (bind (skt, (char *)&addr, sizeof addr, 0) < 0) {
		if (debug) log( "can't bind socket (errno %d)", errno);
		exit (98);
	}
	listen (skt, Maxconnections+1);

	while (1)
	{
		struct sockaddr_in rmtaddr;
		int     len_rmtaddr = sizeof rmtaddr;
		int     tmpskt;
		int     status;
		extern	char *inet_ntoa();

		/*
		 *  Accept a connection.
		 */
		if(( tmpskt = accept( skt, &rmtaddr, &len_rmtaddr, 0 )) < 0) {
			if( debug ) log( "accept error (%d)", errno);
			sleep(1);
			continue;
		}

		/* We have a valid open connection, start a server... */
		if(( pid = fork()) < 0 ) {
			if (debug) log( "could not fork (%d)", errno);
			close( tmpskt );
		} else if( pid == 0 ) {
			/*
			 *  Child
			 */
			char    *rmt;

			hp = gethostbyaddr(&rmtaddr.sin_addr,
					sizeof(rmtaddr.sin_addr), AF_INET);
			if((hp == NULL) || !isstr(hp->h_name)) {
				strcpy(workarea, inet_ntoa(rmtaddr.sin_addr));
				rmt = workarea;
			}
			else rmt = hp->h_name;

			if (debug) log( "%s started", rmt);
			dup2( tmpskt, 0 );
			dup2( tmpskt, 1 );
			if( tmpskt > 1 )
				close( tmpskt );
			execl (Server, "fingerserver",
					rmt, thishost, (char *)0);
			if (debug) log( "server exec error (%d)", errno);
			exit (99);
		}

		/*
		 *  Parent
		 */
		close( tmpskt );
		numconnections++;

		/*
		 *  This code collects ZOMBIES and implements load
		 *  limiting by staying in the do loop while the
		 *  Maxconnections active.
		 */
		while (wait3 (&status, numconnections < Maxconnections
		    ? WNOHANG : 0, 0) > 0)
			numconnections--;
	}
}

arginit( argc, argv )
int     argc;
char    **argv;
{
	register int    i;
	register char   *arg;

	if( argc == 1 ) {
		log( "Usage: %s [-d] [-n #connections] server",
			argv[0]);
		exit( 1 );
	}

	for( i = 1; i < argc; i++ ) {
		if( *argv[i] != '-' )
			break;
		arg = argv[i];
		arg++;
		switch( *arg ) {
		case 'd':
			debug++;
			break;

		case 'n':
			if( ++i == argc ) {
				if (debug) log( "missing number of connections" );
				exit( 99 );
			}
			Maxconnections = atoi( argv[i] );
			if( Maxconnections <= 0 ) {
				if (debug) log( "Bad number of connections '%s'",
				    argv[i] );
				exit( 99 );
			}
			if (debug) log( "Maxconnection now %d", Maxconnections );
			break;
		}
	}

	if( i < argc ) {
		if( access( argv[i], 01 ) < 0 ) {       /* execute privs? */
			if (debug) log( "Cannot access server program '%s'", argv[i] );
			exit( 99 );
		}
		Server = argv[i];
		if( debug ) log( "server is '%s'", Server );
	} else {
		if (debug) log( "Server program not specified!" );
		exit( 99 );
	}

}

/* VARARGS */
log( fmt, a, b, c, d )
char *fmt;
{
	fputs( programid, stderr );
	fprintf( stderr, fmt, a, b, c, d );
	fputc( '\n', stderr );
	fflush( stderr );
}
