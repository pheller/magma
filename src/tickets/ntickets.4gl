# vim: set ts=2 sw=2 et nu colorcolumn=80:
DATABASE nearnet
GLOBALS "ticket_globals.4gl"
MAIN
  DEFINE criteria CHAR(32000)
  DEFINE query CHAR(32767)
  DEFINE cur_ticket_no INT
  DEFINE cur_note_no INT
  DEFINE view CHAR(32)
  DEFINE ticket_cursor_open INT
  DEFINE note_cursor_open INT
  DEFINE next_ticket_no SMALLINT
  DEFINE next_note_no SMALLINT
  DEFINE menu_exit INT
  DEFINE menu_ticket INT
  DEFINE menu_note INT
  DEFINE menu_action INT

  LET menu_exit = 0
  LET menu_ticket = 1
  LET menu_note = 2

  LET view = "tickets"
  LET cur_ticket_no = 0
  LET cur_note_no = 0

  LET ticket_cursor_open = FALSE
  LET note_cursor_open = FALSE

  OPTIONS ACCEPT KEY ESC

  CALL form_is_compiled(tickets, "MEMPACKED", "GENERIC")
  CALL form_is_compiled(notes, "MEMPACKED", "GENERIC")

  OPEN FORM ticket_form FROM "tickets"
  OPEN FORM note_form FROM "notes"

  LET menu_action = menu_ticket
  PREPARE stmt FROM "SELECT false"
  DECLARE _variable("ticket_cursor") SCROLL CURSOR FOR stmt
  OPEN _variable("ticket_cursor")
  DECLARE _variable("note_cursor") SCROLL CURSOR FOR stmt
  OPEN _variable("note_cursor")

  WHILE TRUE
    CASE menu_action
      WHEN menu_ticket
        DISPLAY FORM ticket_form
        CALL show_ticket()
        MENU "PERFORM"
          COMMAND KEY ("Q") "Query" "Searches the active database table."
            CALL query_ticket()
          COMMAND KEY("N") "Next"
            CALL next_ticket()
          COMMAND KEY("P") "Previous"
            CALL prev_ticket()
          COMMAND KEY("D")
            LET menu_action = menu_note
            EXIT MENU
          COMMAND KEY("V") "View"
          COMMAND KEY("A") "Add"
            CALL add_ticket()
          COMMAND KEY("U") "Update"
          COMMAND KEY("R") "Remove"
          COMMAND KEY("T") "Table"
          COMMAND KEY("S") "Screen"
          COMMAND KEY("O") "Output"
          COMMAND KEY("C") "Current"
          COMMAND KEY("E") "Exit"
            LET menu_action = menu_exit
            EXIT MENU
        END MENU
      EXIT CASE
      WHEN menu_note
        DISPLAY FORM note_form
        CALL show_note()
        MENU "PERFORM"
          COMMAND KEY ("Q") "Query" "Searches the active database table."
          COMMAND KEY("N") "Next"
            CALL next_note()
          COMMAND KEY("P") "Previous"
            CALL prev_note()
          COMMAND KEY("M")
            LET menu_action = menu_ticket
            EXIT MENU
          COMMAND KEY("V") "View"
          COMMAND KEY("A") "Add"
            IF ticket.ticket_no > 0 THEN
              CALL add_note()
            END IF
          COMMAND KEY("U") "Update"
          COMMAND KEY("R") "Remove"
          COMMAND KEY("T") "Table"
          COMMAND KEY("S") "Screen"
          COMMAND KEY("O") "Output"
          COMMAND KEY("C") "Current"
          COMMAND KEY("E") "Exit"
            LET menu_action = menu_exit
            EXIT MENU
        END MENU
      WHEN menu_exit
        EXIT WHILE
    END CASE
  END WHILE
  CLEAR SCREEN
END MAIN

FUNCTION add_ticket()
  LET ticket = NULL
  LET ticket.status = "open"
  LET ticket.ttype = "unplanned"
  LET ticket.source = "noc"
  LET ticket.scope = "1site"
  LET ticket.owner = "fixme"
  LET ticket.date_open = today
  LET ticket.time_open = time
  LET ticket.date_start = today
  LET ticket.time_start = time
  LET ticket.notifications = "nearnet-ops,nearnet-outages"
  LET ticket.notify = "N"
  INPUT BY NAME ticket.* WITHOUT DEFAULTS
    BEFORE FIELD ticket_no NEXT FIELD NEXT
    AFTER FIELD notify NEXT FIELD status
  END INPUT

  SELECT (1 + MAX(ticket_no)) INTO ticket.ticket_no FROM tickets
  
  INSERT INTO tickets ( ticket_no, status, ttype, source, scope, site,
    owner, date_open, time_open, date_start, time_start, date_closed,
    time_closed, date_end, time_end, notifications, problem, closecode,
    fixer, solution, notify
  ) VALUES (
    ticket.ticket_no, ticket.status, ticket.ttype, ticket.source,
    ticket.scope, ticket.site, ticket.owner, ticket.date_open,
    ticket.time_open, ticket.date_start, ticket.time_start,
    ticket.date_closed, ticket.time_closed, ticket.date_end,
    ticket.time_end, ticket.notifications, ticket.problem,
    ticket.closecode, ticket.fixer, ticket.solution, ticket.notify
  ) 
  
  CALL display_ticket()
END FUNCTION

FUNCTION add_note()
  DEFINE next_note_no INT

  LET note = NULL
  LET note.ticket_no = ticket.ticket_no
  LET note.name = "fixme"
  LET note.note_date = today
  LET note.note_time = time
  LET note.note = ""
  LET note.notify = "Y"
  INPUT BY NAME note.* WITHOUT DEFAULTS
    BEFORE FIELD ticket_no NEXT FIELD NEXT
    BEFORE FIELD note_no NEXT FIELD NEXT
    AFTER FIELD notify NEXT FIELD name
  END INPUT

  SELECT (1 + MAX(note_no)) INTO next_note_no FROM ticket_notes
    WHERE ticket_no = ticket.ticket_no

  IF next_note_no IS NULL THEN
    LET next_note_no = 1
  END IF

  LET note.note_no = next_note_no

  INSERT
  INTO ticket_notes (
    ticket_no, note_no, name, note_date, note_time, note, notify
  ) VALUES (
    note.ticket_no, note.note_no, note.name, note.note_date,
    note.note_time, note.note, note.notify
  )

  IF note.notify == "Y" THEN
    START REPORT email_ticket_note TO PIPE "/bin/mailx -t"
      OUTPUT TO REPORT email_ticket_note()
    FINISH REPORT email_ticket_note
  END IF

  CALL get_notes_for_current_ticket()
  CALL last_note()
  CALL display_note()
END FUNCTION

FUNCTION query_ticket()
  DEFINE criteria CHAR(32000)
  DEFINE query CHAR(767)
  CLOSE _variable("ticket_cursor")
  CONSTRUCT criteria ON tickets.* FROM tickets.*
  LET query = "SELECT ticket_no, status, ttype, source, scope, site, owner, date_open, time_open, date_start, time_start, date_closed, time_closed, date_end, time_end, notifications, problem, closecode, fixer, solution, notify from tickets WHERE ", criteria
  PREPARE statement FROM query
  DECLARE _variable("ticket_cursor") CURSOR FOR statement
  OPEN _variable("ticket_cursor")
  EXECUTE statement
  CALL first_ticket()
END FUNCTION

FUNCTION get_notes_for_current_ticket()
  DEFINE query CHAR(767)
  CLOSE _variable("note_cursor")
  LET note = NULL
  LET query = "SELECT * FROM ticket_notes WHERE ticket_notes.ticket_no=", ticket.ticket_no, " ORDER BY note_no"
  PREPARE statement FROM query
  DECLARE _variable("note_cursor") CURSOR FOR statement
  OPEN _variable("note_cursor")
  EXECUTE statement
END FUNCTION

FUNCTION first_ticket()
  FETCH FIRST _variable("ticket_cursor") INTO ticket
  CALL get_notes_for_current_ticket()
  CALL first_note()
  CALL display_ticket()
END FUNCTION

FUNCTION next_ticket()
  FETCH NEXT _variable("ticket_cursor") INTO ticket
  CALL get_notes_for_current_ticket()
  CALL first_note()
  CALL display_ticket()
END FUNCTION

FUNCTION prev_ticket()
  FETCH PREVIOUS _variable("ticket_cursor") INTO ticket
  CALL get_notes_for_current_ticket()
  CALL first_note()
  CALL display_ticket()
END FUNCTION

FUNCTION show_ticket()
  FETCH CURRENT _variable("ticket_cursor") INTO ticket
  CALL display_ticket()
END FUNCTION

FUNCTION display_ticket()
  IF ticket.ticket_no > 0 THEN
    DISPLAY BY NAME ticket.*
  END IF
END FUNCTION

FUNCTION next_note()
  FETCH NEXT _variable("note_cursor") INTO note
  CALL display_note()
END FUNCTION

FUNCTION prev_note()
  FETCH PREVIOUS _variable("note_cursor") INTO note
  CALL display_note()
END FUNCTION

FUNCTION first_note()
  FETCH FIRST _variable("note_cursor") INTO note
END FUNCTION

FUNCTION last_note()
  FETCH LAST _variable("note_cursor") INTO note
END FUNCTION

FUNCTION show_note()
  FETCH CURRENT _variable("note_cursor") INTO note
  CALL display_note()
END FUNCTION

FUNCTION display_note()
  IF note.note_no > 0 THEN
    DISPLAY BY NAME note.*
  END IF
END FUNCTION

REPORT email_ticket_note()
  OUTPUT
    TOP MARGIN 0
    BOTTOM MARGIN 3
    LEFT MARGIN 0
    RIGHT MARGIN 132
    PAGE LENGTH 999
  FORMAT
  PAGE HEADER
    PRINT "From: Phillip Heller <pheller@gmail.com>"
    PRINT "To: Phillip Heller <pheller@me.com>"
    PRINT "Subject: Ticket #", ticket.ticket_no, " - Note #", note.note_no
    PRINT ""
    PRINT "Note:"
    PRINT note.note
END REPORT
