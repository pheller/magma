#include "a4gl_incl_4glhdr.h"
extern int a4gl_lib_status;
static char *_module_name="ticket_globals.4gl";
#define fgldate long
static char const _rcsid[]="$FGLIdent: ticket_globals.4gl Compiler-1.240  Log:Not Set $";
static void a4gl_show_compiled_version(void) {
printf("Log: Not Set\n");
printf("Aubit4GL Version: 1.2.40\n");
exit(0);
}

static const char *_CompileTimeSQLType="pg8";
static const struct sDependantTable _dependantTables[]= {
  {0,0}
};

extern struct {
   long sqlcode ;
   char sqlerrm [72+1];
   char sqlerrp [8+1];
   long sqlerrd[6] ;
   char sqlawarn [8+1];
   char sqlstate [9+1];
} a4gl_sqlca;
extern long a4gl_status ;
extern long aiplib_status ;
extern long curr_form ;
extern long curr_hwnd ;
extern char err_file_name [32+1];
extern long err_line_no ;
extern long err_status ;
extern long int_flag ;
extern long quit_flag ;
struct {
   short ticket_no ;
   short note_no ;
   char name [20+1];
   fgldate note_date ;
   char note_time [5+1];
   char note [540+1];
   char notify [1+1];
} note;
struct {
   short ticket_no ;
   char status [10+1];
   char ttype [10+1];
   char source [10+1];
   char scope [10+1];
   char site [25+1];
   char owner [10+1];
   fgldate date_open ;
   char time_open [5+1];
   fgldate date_start ;
   char time_start [5+1];
   fgldate date_closed ;
   char time_closed [5+1];
   fgldate date_end ;
   char time_end [5+1];
   char notifications [120+1];
   char problem [300+1];
   char closecode [15+1];
   char fixer [10+1];
   char solution [300+1];
   char notify [1+1];
} ticket;

static int _done_init_global_variables=1;
A4GL_INTERNAL_FUNCTION void init_global_variables_ticketglobals_d0a(void) {
 static void *_objData[]={
NULL};
   if (_done_init_global_variables==0) return;
   _done_init_global_variables=0;
   A4GL_register_global_objects(_module_name, _objData);
   A4GL_check_version(_module_name,"1.2",40);
   A4GL_check_dependant_tables(_module_name, _CompileTimeSQLType, _dependantTables);
   /* Print nullify */
   note.ticket_no=0;
   note.note_no=0;
   A4GL_setnull(0,&note.name,0x14); /*1 */
   A4GL_setnull(7,&note.note_date,0x0); /*1 */
   A4GL_setnull(0,&note.note_time,0x5); /*1 */
   A4GL_setnull(0,&note.note,0x21c); /*1 */
   A4GL_setnull(0,&note.notify,0x1); /*1 */
   ticket.ticket_no=0;
   A4GL_setnull(0,&ticket.status,0xa); /*1 */
   A4GL_setnull(0,&ticket.ttype,0xa); /*1 */
   A4GL_setnull(0,&ticket.source,0xa); /*1 */
   A4GL_setnull(0,&ticket.scope,0xa); /*1 */
   A4GL_setnull(0,&ticket.site,0x19); /*1 */
   A4GL_setnull(0,&ticket.owner,0xa); /*1 */
   A4GL_setnull(7,&ticket.date_open,0x0); /*1 */
   A4GL_setnull(0,&ticket.time_open,0x5); /*1 */
   A4GL_setnull(7,&ticket.date_start,0x0); /*1 */
   A4GL_setnull(0,&ticket.time_start,0x5); /*1 */
   A4GL_setnull(7,&ticket.date_closed,0x0); /*1 */
   A4GL_setnull(0,&ticket.time_closed,0x5); /*1 */
   A4GL_setnull(7,&ticket.date_end,0x0); /*1 */
   A4GL_setnull(0,&ticket.time_end,0x5); /*1 */
   A4GL_setnull(0,&ticket.notifications,0x78); /*1 */
   A4GL_setnull(0,&ticket.problem,0x12c); /*1 */
   A4GL_setnull(0,&ticket.closecode,0xf); /*1 */
   A4GL_setnull(0,&ticket.fixer,0xa); /*1 */
   A4GL_setnull(0,&ticket.solution,0x12c); /*1 */
   A4GL_setnull(0,&ticket.notify,0x1); /*1 */
}


static int _done_init_module_variables=1;
A4GL_INTERNAL_FUNCTION static void init_module_variables(void) {
 static void *_objData[]={
NULL};
   if (_done_init_module_variables==0) return;
   _done_init_module_variables=0;
   A4GL_register_module_objects(_module_name, _objData);
   A4GL_check_version(_module_name,"1.2",40);
   A4GL_check_dependant_tables(_module_name, _CompileTimeSQLType, _dependantTables);
   /* Print nullify */
   // Initialise the current global variables
   init_global_variables_ticketglobals_d0a();
   // Initialise any other global variables
}

/* END OF MODULE */
