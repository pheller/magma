#include "a4gl_incl_4glhdr.h"
extern int a4gl_lib_status;
static char *_module_name="ntickets.4gl";


A4GL_MAIN int main(int argc,char *argv[]);
A4GL_FUNCTION int aclfgl_add_ticket (int n);
A4GL_FUNCTION int aclfgl_add_note (int n);
A4GL_FUNCTION int aclfgl_query_ticket (int n);
A4GL_FUNCTION int aclfgl_get_notes_for_current_ticket (int n);
A4GL_FUNCTION int aclfgl_first_ticket (int n);
A4GL_FUNCTION int aclfgl_next_ticket (int n);
A4GL_FUNCTION int aclfgl_prev_ticket (int n);
A4GL_FUNCTION int aclfgl_show_ticket (int n);
A4GL_FUNCTION int aclfgl_display_ticket (int n);
A4GL_FUNCTION int aclfgl_next_note (int n);
A4GL_FUNCTION int aclfgl_prev_note (int n);
A4GL_FUNCTION int aclfgl_first_note (int n);
A4GL_FUNCTION int aclfgl_last_note (int n);
A4GL_FUNCTION int aclfgl_show_note (int n);
A4GL_FUNCTION int aclfgl_display_note (int n);
A4GL_REPORT void aclfgl_email_ticket_note (int n,int a);
void init_global_variables_ticketglobals_d0a(void);
extern char compiled_form_tickets[];
extern char compiled_form_notes[];
#define fgldate long
static char const _rcsid[]="$FGLIdent: ntickets.4gl Compiler-1.240  Log:Not Set $";
static void a4gl_show_compiled_version(void) {
printf("Log: Not Set\n");
printf("Aubit4GL Version: 1.2.40\n");
exit(0);
}

static const char *_CompileTimeSQLType="pg8";
static const struct sDependantTable _dependantTables[]= {
  {0,0}
};

extern struct {
   long sqlcode ;
   char sqlerrm [72+1];
   char sqlerrp [8+1];
   long sqlerrd[6] ;
   char sqlawarn [8+1];
   char sqlstate [9+1];
} a4gl_sqlca;
extern long a4gl_status ;
extern long aiplib_status ;
extern long curr_form ;
extern long curr_hwnd ;
extern char err_file_name [32+1];
extern long err_line_no ;
extern long err_status ;
extern long int_flag ;
extern struct {
   short ticket_no ;
   short note_no ;
   char name [20+1];
   fgldate note_date ;
   char note_time [5+1];
   char note [540+1];
   char notify [1+1];
} note;
extern long quit_flag ;
extern struct {
   short ticket_no ;
   char status [10+1];
   char ttype [10+1];
   char source [10+1];
   char scope [10+1];
   char site [25+1];
   char owner [10+1];
   fgldate date_open ;
   char time_open [5+1];
   fgldate date_start ;
   char time_start [5+1];
   fgldate date_closed ;
   char time_closed [5+1];
   fgldate date_end ;
   char time_end [5+1];
   char notifications [120+1];
   char problem [300+1];
   char closecode [15+1];
   char fixer [10+1];
   char solution [300+1];
   char notify [1+1];
} ticket;

static int _done_init_module_variables=1;
A4GL_INTERNAL_FUNCTION static void init_module_variables(void) {
 static void *_objData[]={
NULL};
   if (_done_init_module_variables==0) return;
   _done_init_module_variables=0;
   A4GL_register_module_objects(_module_name, _objData);
   A4GL_check_version(_module_name,"1.2",40);
   A4GL_check_dependant_tables(_module_name, _CompileTimeSQLType, _dependantTables);
   /* Print nullify */
   // Initialise the current global variables
   // No global variables in use
   // Initialise any other global variables
   init_global_variables_ticketglobals_d0a();
}




A4GL_MAIN int main(int argc,char *argv[]) {
char *_paramnames[1]={""};
void *_blobdata=0;
int _nargs=0;
static char *_functionName = "MAIN";
   char criteria [32000+1];
   long cur_note_no ;
   long cur_ticket_no ;
   long menu_action ;
   long menu_exit ;
   long menu_note ;
   long menu_ticket ;
   short next_note_no ;
   short next_ticket_no ;
   long note_cursor_open ;
   char query [32767+1];
   long ticket_cursor_open ;
   char view [32+1];
    void *_objData[]={
   NULL};
   A4GLSTK_setCurrentLine(0,0);
   if (A4GL_fgl_start(argc,argv)) {a4gl_show_compiled_version();}
   aclfgli_clr_err_flg();
   {char *_s; char *_sqltype=NULL;
   A4GL_push_char("nearnet");
   _s=A4GL_char_pop();A4GL_trim(_s);
   A4GL_init_connection_with_sqltype(_s, _sqltype);
   free(_s);
   if (_sqltype) free(_sqltype);
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(4,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(4,_module_name); }
   if (a4gl_sqlca.sqlcode<0) A4GL_chk_err(0,_module_name);

   init_module_variables();
   /* Print nullify */
   A4GL_setnull(0,&criteria,0x7d00); /*1 */
   cur_note_no=0;
   cur_ticket_no=0;
   menu_action=0;
   menu_exit=0;
   menu_note=0;
   menu_ticket=0;
   next_note_no=0;
   next_ticket_no=0;
   note_cursor_open=0;
   A4GL_setnull(0,&query,0x7fff); /*1 */
   ticket_cursor_open=0;
   A4GL_setnull(0,&view,0x20); /*1 */
   A4GLSTK_initFunctionCallStack();
   A4GLSTK_pushFunction_v2(_functionName,_paramnames,_nargs,_module_name,4,_objData);

   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 19 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,19);
   aclfgli_clr_err_flg();

   menu_exit=0;
      ERR_CHK_ERROR  { A4GL_chk_err(19,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 20 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,20);
   aclfgli_clr_err_flg();
   A4GL_push_long(1);

   A4GL_pop_var2(&menu_ticket,2,0);
      ERR_CHK_ERROR  { A4GL_chk_err(20,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 21 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,21);
   aclfgli_clr_err_flg();
   A4GL_push_long(2);

   A4GL_pop_var2(&menu_note,2,0);
      ERR_CHK_ERROR  { A4GL_chk_err(21,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 23 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,23);
   aclfgli_clr_err_flg();
   A4GL_push_char("tickets");

   A4GL_pop_var2(&view,0,32);
      ERR_CHK_ERROR  { A4GL_chk_err(23,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 24 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,24);
   aclfgli_clr_err_flg();

   cur_ticket_no=0;
      ERR_CHK_ERROR  { A4GL_chk_err(24,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 25 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,25);
   aclfgli_clr_err_flg();

   cur_note_no=0;
      ERR_CHK_ERROR  { A4GL_chk_err(25,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 27 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,27);
   aclfgli_clr_err_flg();
   A4GL_push_int(0);

   A4GL_pop_var2(&ticket_cursor_open,2,0);
      ERR_CHK_ERROR  { A4GL_chk_err(27,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 28 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,28);
   aclfgli_clr_err_flg();
   A4GL_push_int(0);

   A4GL_pop_var2(&note_cursor_open,2,0);
      ERR_CHK_ERROR  { A4GL_chk_err(28,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_OPTIONS_CMD Line 30 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,30);
   aclfgli_clr_err_flg();
   A4GL_set_option_value('A',A4GL_key_val("escape"));
      ERR_CHK_ERROR  { A4GL_chk_err(30,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 32 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,32);
   aclfgli_clr_err_flg();
   {char *_packer; char *_formtype;int _retvars=0;
   A4GL_push_char("MEMPACKED");
   _packer=A4GL_char_pop();
   A4GL_push_char("GENERIC");
   _formtype=A4GL_char_pop();
   A4GL_add_compiled_form("tickets",_formtype,_packer,compiled_form_tickets);
   free(_packer);free(_formtype);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(32,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 33 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,33);
   aclfgli_clr_err_flg();
   {char *_packer; char *_formtype;int _retvars=0;
   A4GL_push_char("MEMPACKED");
   _packer=A4GL_char_pop();
   A4GL_push_char("GENERIC");
   _formtype=A4GL_char_pop();
   A4GL_add_compiled_form("notes",_formtype,_packer,compiled_form_notes);
   free(_packer);free(_formtype);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(33,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_OPEN_FORM_CMD Line 35 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,35);
   aclfgli_clr_err_flg();

   A4GL_push_char("tickets");A4GL_open_form("ticket_form");
      ERR_CHK_ERROR  { A4GL_chk_err(35,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_OPEN_FORM_CMD Line 36 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,36);
   aclfgli_clr_err_flg();

   A4GL_push_char("notes");A4GL_open_form("note_form");
      ERR_CHK_ERROR  { A4GL_chk_err(36,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 38 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,38);
   aclfgli_clr_err_flg();

   menu_action=menu_ticket;
      ERR_CHK_ERROR  { A4GL_chk_err(38,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_PREPARE_CMD Line 39 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,39);
   aclfgli_clr_err_flg();
   {char *_sql;
   A4GL_push_char("SELECT false");
   _sql=A4GL_char_pop();
   A4GL_add_prepare(A4GL_get_ident("ntickets","stmt","ntickets_stmt"),(void *)A4GL_prepare_select(0,0,0,0,_sql,_module_name,39,0,0));
   free(_sql);
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(39,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(39,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_DECLARE_CMD Line 40 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,40);
   aclfgli_clr_err_flg();
   {
   A4GL_declare_cursor(0,A4GL_find_prepare(A4GL_get_ident("ntickets","stmt","ntickets_stmt")),1,aclfgli_str_to_id("ticket_cursor"));
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(40,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(40,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_OPEN_CURSOR_CMD Line 41 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,41);
   aclfgli_clr_err_flg();
   A4GL_open_cursor(aclfgli_str_to_id("ticket_cursor"),0,0);
      ERR_CHK_SQLERROR  { A4GL_chk_err(41,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(41,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_DECLARE_CMD Line 42 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,42);
   aclfgli_clr_err_flg();
   {
   A4GL_declare_cursor(0,A4GL_find_prepare(A4GL_get_ident("ntickets","stmt","ntickets_stmt")),1,aclfgli_str_to_id("note_cursor"));
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(42,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(42,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_OPEN_CURSOR_CMD Line 43 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,43);
   aclfgli_clr_err_flg();
   A4GL_open_cursor(aclfgli_str_to_id("note_cursor"),0,0);
      ERR_CHK_SQLERROR  { A4GL_chk_err(43,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(43,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_WHILE_CMD Line 45 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,45);
   aclfgli_clr_err_flg();
   while (1==1) {
      A4GL_push_int(1);
      if (!(A4GL_pop_bool())) break;
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_CASE_CMD Line 46 E_MET_MAIN_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,46);
      aclfgli_clr_err_flg();
      {char *_s=0;

      A4GL_push_long(menu_action);
      while (1==1) {if (_s==0) {_s=A4GL_char_pop();}

         A4GL_push_long(menu_ticket);
         A4GL_push_char(_s);
         A4GL_pushop(OP_EQUAL);
         if (A4GL_pop_bool()) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_DISPLAY_FORM_CMD Line 48 E_MET_MAIN_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,48);
            aclfgli_clr_err_flg();

            A4GL_disp_form("ticket_form",-1);
               ERR_CHK_ERROR  { A4GL_chk_err(48,_module_name); }
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_CALL_CMD Line 49 E_MET_MAIN_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,49);
            aclfgli_clr_err_flg();
            /* done print expr */
            {int _retvars;A4GL_set_status(0,0);
            A4GLSTK_setCurrentLine(_module_name,49);
            _retvars=aclfgl_show_ticket(0);
            /* pr 1 */
            CHECK_NO_RETURN;
            }
               ERR_CHK_ERROR  { A4GL_chk_err(49,_module_name); }
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_MENU_CMD Line 72 E_MET_MAIN_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,72);
            aclfgli_clr_err_flg();
            {
            void *m_3=0;
            int cmd_no_3= -1; /* n=3 */
               MENU_START_3: ;
               while (cmd_no_3!= -3) {
                   if (cmd_no_3==0) {
                     A4GL_clr_ignore_error_list();
                     /* CMD : E_CMD_CALL_CMD Line 52 E_MET_MAIN_DEFINITION */
                     A4GLSTK_setCurrentLine(_module_name,52);
                     aclfgli_clr_err_flg();
                     /* done print expr */
                     {int _retvars;A4GL_set_status(0,0);
                     A4GLSTK_setCurrentLine(_module_name,52);
                     _retvars=aclfgl_query_ticket(0);
                     /* pr 1 */
                     CHECK_NO_RETURN;
                     }
                        ERR_CHK_ERROR  { A4GL_chk_err(52,_module_name); }
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_3= -4;goto MENU_START_3;  /* 3 */
                  }
                   if (cmd_no_3==1) {
                     A4GL_clr_ignore_error_list();
                     /* CMD : E_CMD_CALL_CMD Line 54 E_MET_MAIN_DEFINITION */
                     A4GLSTK_setCurrentLine(_module_name,54);
                     aclfgli_clr_err_flg();
                     /* done print expr */
                     {int _retvars;A4GL_set_status(0,0);
                     A4GLSTK_setCurrentLine(_module_name,54);
                     _retvars=aclfgl_next_ticket(0);
                     /* pr 1 */
                     CHECK_NO_RETURN;
                     }
                        ERR_CHK_ERROR  { A4GL_chk_err(54,_module_name); }
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_3= -4;goto MENU_START_3;  /* 3 */
                  }
                   if (cmd_no_3==2) {
                     A4GL_clr_ignore_error_list();
                     /* CMD : E_CMD_CALL_CMD Line 56 E_MET_MAIN_DEFINITION */
                     A4GLSTK_setCurrentLine(_module_name,56);
                     aclfgli_clr_err_flg();
                     /* done print expr */
                     {int _retvars;A4GL_set_status(0,0);
                     A4GLSTK_setCurrentLine(_module_name,56);
                     _retvars=aclfgl_prev_ticket(0);
                     /* pr 1 */
                     CHECK_NO_RETURN;
                     }
                        ERR_CHK_ERROR  { A4GL_chk_err(56,_module_name); }
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_3= -4;goto MENU_START_3;  /* 3 */
                  }
                   if (cmd_no_3==3) {
                     A4GL_clr_ignore_error_list();
                     /* CMD : E_CMD_LET_CMD Line 58 E_MET_MAIN_DEFINITION */
                     A4GLSTK_setCurrentLine(_module_name,58);
                     aclfgli_clr_err_flg();

                     menu_action=menu_note;
                        ERR_CHK_ERROR  { A4GL_chk_err(58,_module_name); }
                     A4GL_clr_ignore_error_list();
                     /* CMD : E_CMD_EXT_CMD Line 59 E_MET_MAIN_DEFINITION */
                     A4GLSTK_setCurrentLine(_module_name,59);
                     cmd_no_3= -3;goto MENU_START_3;
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_3= -4;goto MENU_START_3;  /* 3 */
                  }
                   if (cmd_no_3==4) {
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_3= -4;goto MENU_START_3;  /* 3 */
                  }
                   if (cmd_no_3==5) {
                     A4GL_clr_ignore_error_list();
                     /* CMD : E_CMD_CALL_CMD Line 62 E_MET_MAIN_DEFINITION */
                     A4GLSTK_setCurrentLine(_module_name,62);
                     aclfgli_clr_err_flg();
                     /* done print expr */
                     {int _retvars;A4GL_set_status(0,0);
                     A4GLSTK_setCurrentLine(_module_name,62);
                     _retvars=aclfgl_add_ticket(0);
                     /* pr 1 */
                     CHECK_NO_RETURN;
                     }
                        ERR_CHK_ERROR  { A4GL_chk_err(62,_module_name); }
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_3= -4;goto MENU_START_3;  /* 3 */
                  }
                   if (cmd_no_3==6) {
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_3= -4;goto MENU_START_3;  /* 3 */
                  }
                   if (cmd_no_3==7) {
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_3= -4;goto MENU_START_3;  /* 3 */
                  }
                   if (cmd_no_3==8) {
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_3= -4;goto MENU_START_3;  /* 3 */
                  }
                   if (cmd_no_3==9) {
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_3= -4;goto MENU_START_3;  /* 3 */
                  }
                   if (cmd_no_3==10) {
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_3= -4;goto MENU_START_3;  /* 3 */
                  }
                   if (cmd_no_3==11) {
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_3= -4;goto MENU_START_3;  /* 3 */
                  }
                   if (cmd_no_3==12) {
                     A4GL_clr_ignore_error_list();
                     /* CMD : E_CMD_LET_CMD Line 70 E_MET_MAIN_DEFINITION */
                     A4GLSTK_setCurrentLine(_module_name,70);
                     aclfgli_clr_err_flg();

                     menu_action=menu_exit;
                        ERR_CHK_ERROR  { A4GL_chk_err(70,_module_name); }
                     A4GL_clr_ignore_error_list();
                     /* CMD : E_CMD_EXT_CMD Line 71 E_MET_MAIN_DEFINITION */
                     A4GLSTK_setCurrentLine(_module_name,71);
                     cmd_no_3= -3;goto MENU_START_3;
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_3= -4;goto MENU_START_3;  /* 3 */
                  }
                   if (cmd_no_3==13) {
                     A4GL_clr_ignore_error_list();
                     /* CMD : E_CMD_EXT_CMD Line 72 E_MET_MAIN_DEFINITION */
                     A4GLSTK_setCurrentLine(_module_name,72);
                     cmd_no_3= -3;goto MENU_START_3;
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_3= -4;goto MENU_START_3;  /* 3 */
                  }
                  if (cmd_no_3== -1) {
                     char *_mntitle;
                     char *_mnoptiontitle;
                     /* PRINT EXPR */
                     A4GL_push_char("PERFORM");
                     /* END PRINT EXPR */
                     _mntitle=A4GL_char_pop(); /* ... */
                     m_3=(void *)A4GL_new_menu_create_with_attr(_mntitle,1,1,2,0,"","","","","");
                     free(_mntitle);

                     A4GL_add_menu_option(m_3, "Query","Q","Searches the active database table.",0,0);

                     A4GL_add_menu_option(m_3, "Next","N","",0,0);

                     A4GL_add_menu_option(m_3, "Previous","P","",0,0);

                     A4GL_add_menu_option(m_3, "","D","",0,0);

                     A4GL_add_menu_option(m_3, "View","V","",0,0);

                     A4GL_add_menu_option(m_3, "Add","A","",0,0);

                     A4GL_add_menu_option(m_3, "Update","U","",0,0);

                     A4GL_add_menu_option(m_3, "Remove","R","",0,0);

                     A4GL_add_menu_option(m_3, "Table","T","",0,0);

                     A4GL_add_menu_option(m_3, "Screen","S","",0,0);

                     A4GL_add_menu_option(m_3, "Output","O","",0,0);

                     A4GL_add_menu_option(m_3, "Current","C","",0,0);

                     A4GL_add_menu_option(m_3, "Exit","E","",0,0);
                     A4GL_add_menu_action(m_3, "fgl_exit_menu",13);
                     A4GL_finish_create_menu(m_3);
                     cmd_no_3= -2;
                     continue;
                  }
                  CONTINUE_BLOCK_3:    ;

                  A4GL_ensure_menu_option(0, m_3, "Query","Q","Searches the active database table.",0,0);

                  A4GL_ensure_menu_option(1, m_3, "Next","N","",0,0);

                  A4GL_ensure_menu_option(2, m_3, "Previous","P","",0,0);

                  A4GL_ensure_menu_option(3, m_3, "","D","",0,0);

                  A4GL_ensure_menu_option(4, m_3, "View","V","",0,0);

                  A4GL_ensure_menu_option(5, m_3, "Add","A","",0,0);

                  A4GL_ensure_menu_option(6, m_3, "Update","U","",0,0);

                  A4GL_ensure_menu_option(7, m_3, "Remove","R","",0,0);

                  A4GL_ensure_menu_option(8, m_3, "Table","T","",0,0);

                  A4GL_ensure_menu_option(9, m_3, "Screen","S","",0,0);

                  A4GL_ensure_menu_option(10, m_3, "Output","O","",0,0);

                  A4GL_ensure_menu_option(11, m_3, "Current","C","",0,0);

                  A4GL_ensure_menu_option(12, m_3, "Exit","E","",0,0);
                  cmd_no_3=A4GL_menu_loop_v2(m_3,0);
               }
               A4GL_free_menu(m_3);
            }
            END_BLOCK_3:    ;
               ERR_CHK_ERROR  { A4GL_chk_err(72,_module_name); }
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_EXT_CMD Line 73 E_MET_MAIN_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,73);
            goto END_BLOCK_2;
         break;
         }

         A4GL_push_long(menu_note);
         A4GL_push_char(_s);
         A4GL_pushop(OP_EQUAL);
         if (A4GL_pop_bool()) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_DISPLAY_FORM_CMD Line 75 E_MET_MAIN_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,75);
            aclfgli_clr_err_flg();

            A4GL_disp_form("note_form",-1);
               ERR_CHK_ERROR  { A4GL_chk_err(75,_module_name); }
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_CALL_CMD Line 76 E_MET_MAIN_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,76);
            aclfgli_clr_err_flg();
            /* done print expr */
            {int _retvars;A4GL_set_status(0,0);
            A4GLSTK_setCurrentLine(_module_name,76);
            _retvars=aclfgl_show_note(0);
            /* pr 1 */
            CHECK_NO_RETURN;
            }
               ERR_CHK_ERROR  { A4GL_chk_err(76,_module_name); }
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_MENU_CMD Line 100 E_MET_MAIN_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,100);
            aclfgli_clr_err_flg();
            {
            void *m_4=0;
            int cmd_no_4= -1; /* n=4 */
               MENU_START_4: ;
               while (cmd_no_4!= -3) {
                   if (cmd_no_4==0) {
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_4= -4;goto MENU_START_4;  /* 4 */
                  }
                   if (cmd_no_4==1) {
                     A4GL_clr_ignore_error_list();
                     /* CMD : E_CMD_CALL_CMD Line 80 E_MET_MAIN_DEFINITION */
                     A4GLSTK_setCurrentLine(_module_name,80);
                     aclfgli_clr_err_flg();
                     /* done print expr */
                     {int _retvars;A4GL_set_status(0,0);
                     A4GLSTK_setCurrentLine(_module_name,80);
                     _retvars=aclfgl_next_note(0);
                     /* pr 1 */
                     CHECK_NO_RETURN;
                     }
                        ERR_CHK_ERROR  { A4GL_chk_err(80,_module_name); }
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_4= -4;goto MENU_START_4;  /* 4 */
                  }
                   if (cmd_no_4==2) {
                     A4GL_clr_ignore_error_list();
                     /* CMD : E_CMD_CALL_CMD Line 82 E_MET_MAIN_DEFINITION */
                     A4GLSTK_setCurrentLine(_module_name,82);
                     aclfgli_clr_err_flg();
                     /* done print expr */
                     {int _retvars;A4GL_set_status(0,0);
                     A4GLSTK_setCurrentLine(_module_name,82);
                     _retvars=aclfgl_prev_note(0);
                     /* pr 1 */
                     CHECK_NO_RETURN;
                     }
                        ERR_CHK_ERROR  { A4GL_chk_err(82,_module_name); }
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_4= -4;goto MENU_START_4;  /* 4 */
                  }
                   if (cmd_no_4==3) {
                     A4GL_clr_ignore_error_list();
                     /* CMD : E_CMD_LET_CMD Line 84 E_MET_MAIN_DEFINITION */
                     A4GLSTK_setCurrentLine(_module_name,84);
                     aclfgli_clr_err_flg();

                     menu_action=menu_ticket;
                        ERR_CHK_ERROR  { A4GL_chk_err(84,_module_name); }
                     A4GL_clr_ignore_error_list();
                     /* CMD : E_CMD_EXT_CMD Line 85 E_MET_MAIN_DEFINITION */
                     A4GLSTK_setCurrentLine(_module_name,85);
                     cmd_no_4= -3;goto MENU_START_4;
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_4= -4;goto MENU_START_4;  /* 4 */
                  }
                   if (cmd_no_4==4) {
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_4= -4;goto MENU_START_4;  /* 4 */
                  }
                   if (cmd_no_4==5) {
                     A4GL_clr_ignore_error_list();
                     /* CMD : E_CMD_IF_CMD Line 88 E_MET_MAIN_DEFINITION */
                     A4GLSTK_setCurrentLine(_module_name,88);
                     aclfgli_clr_err_flg();

                     A4GL_push_int(ticket.ticket_no);
                     A4GL_push_long(0);
                     A4GL_pushop(OP_GREATER_THAN);
                     if (A4GL_pop_bool()) {
                        A4GL_clr_ignore_error_list();
                        /* CMD : E_CMD_CALL_CMD Line 89 E_MET_MAIN_DEFINITION */
                        A4GLSTK_setCurrentLine(_module_name,89);
                        aclfgli_clr_err_flg();
                        /* done print expr */
                        {int _retvars;A4GL_set_status(0,0);
                        A4GLSTK_setCurrentLine(_module_name,89);
                        _retvars=aclfgl_add_note(0);
                        /* pr 1 */
                        CHECK_NO_RETURN;
                        }
                           ERR_CHK_ERROR  { A4GL_chk_err(89,_module_name); }
                     }
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_4= -4;goto MENU_START_4;  /* 4 */
                  }
                   if (cmd_no_4==6) {
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_4= -4;goto MENU_START_4;  /* 4 */
                  }
                   if (cmd_no_4==7) {
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_4= -4;goto MENU_START_4;  /* 4 */
                  }
                   if (cmd_no_4==8) {
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_4= -4;goto MENU_START_4;  /* 4 */
                  }
                   if (cmd_no_4==9) {
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_4= -4;goto MENU_START_4;  /* 4 */
                  }
                   if (cmd_no_4==10) {
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_4= -4;goto MENU_START_4;  /* 4 */
                  }
                   if (cmd_no_4==11) {
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_4= -4;goto MENU_START_4;  /* 4 */
                  }
                   if (cmd_no_4==12) {
                     A4GL_clr_ignore_error_list();
                     /* CMD : E_CMD_LET_CMD Line 98 E_MET_MAIN_DEFINITION */
                     A4GLSTK_setCurrentLine(_module_name,98);
                     aclfgli_clr_err_flg();

                     menu_action=menu_exit;
                        ERR_CHK_ERROR  { A4GL_chk_err(98,_module_name); }
                     A4GL_clr_ignore_error_list();
                     /* CMD : E_CMD_EXT_CMD Line 99 E_MET_MAIN_DEFINITION */
                     A4GLSTK_setCurrentLine(_module_name,99);
                     cmd_no_4= -3;goto MENU_START_4;
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_4= -4;goto MENU_START_4;  /* 4 */
                  }
                   if (cmd_no_4==13) {
                     A4GL_clr_ignore_error_list();
                     /* CMD : E_CMD_EXT_CMD Line 100 E_MET_MAIN_DEFINITION */
                     A4GLSTK_setCurrentLine(_module_name,100);
                     cmd_no_4= -3;goto MENU_START_4;
                     /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_4= -4;goto MENU_START_4;  /* 4 */
                  }
                  if (cmd_no_4== -1) {
                     char *_mntitle;
                     char *_mnoptiontitle;
                     /* PRINT EXPR */
                     A4GL_push_char("PERFORM");
                     /* END PRINT EXPR */
                     _mntitle=A4GL_char_pop(); /* ... */
                     m_4=(void *)A4GL_new_menu_create_with_attr(_mntitle,1,1,2,0,"","","","","");
                     free(_mntitle);

                     A4GL_add_menu_option(m_4, "Query","Q","Searches the active database table.",0,0);

                     A4GL_add_menu_option(m_4, "Next","N","",0,0);

                     A4GL_add_menu_option(m_4, "Previous","P","",0,0);

                     A4GL_add_menu_option(m_4, "","M","",0,0);

                     A4GL_add_menu_option(m_4, "View","V","",0,0);

                     A4GL_add_menu_option(m_4, "Add","A","",0,0);

                     A4GL_add_menu_option(m_4, "Update","U","",0,0);

                     A4GL_add_menu_option(m_4, "Remove","R","",0,0);

                     A4GL_add_menu_option(m_4, "Table","T","",0,0);

                     A4GL_add_menu_option(m_4, "Screen","S","",0,0);

                     A4GL_add_menu_option(m_4, "Output","O","",0,0);

                     A4GL_add_menu_option(m_4, "Current","C","",0,0);

                     A4GL_add_menu_option(m_4, "Exit","E","",0,0);
                     A4GL_add_menu_action(m_4, "fgl_exit_menu",13);
                     A4GL_finish_create_menu(m_4);
                     cmd_no_4= -2;
                     continue;
                  }
                  CONTINUE_BLOCK_4:    ;

                  A4GL_ensure_menu_option(0, m_4, "Query","Q","Searches the active database table.",0,0);

                  A4GL_ensure_menu_option(1, m_4, "Next","N","",0,0);

                  A4GL_ensure_menu_option(2, m_4, "Previous","P","",0,0);

                  A4GL_ensure_menu_option(3, m_4, "","M","",0,0);

                  A4GL_ensure_menu_option(4, m_4, "View","V","",0,0);

                  A4GL_ensure_menu_option(5, m_4, "Add","A","",0,0);

                  A4GL_ensure_menu_option(6, m_4, "Update","U","",0,0);

                  A4GL_ensure_menu_option(7, m_4, "Remove","R","",0,0);

                  A4GL_ensure_menu_option(8, m_4, "Table","T","",0,0);

                  A4GL_ensure_menu_option(9, m_4, "Screen","S","",0,0);

                  A4GL_ensure_menu_option(10, m_4, "Output","O","",0,0);

                  A4GL_ensure_menu_option(11, m_4, "Current","C","",0,0);

                  A4GL_ensure_menu_option(12, m_4, "Exit","E","",0,0);
                  cmd_no_4=A4GL_menu_loop_v2(m_4,0);
               }
               A4GL_free_menu(m_4);
            }
            END_BLOCK_4:    ;
               ERR_CHK_ERROR  { A4GL_chk_err(100,_module_name); }
         break;
         }

         A4GL_push_long(menu_exit);
         A4GL_push_char(_s);
         A4GL_pushop(OP_EQUAL);
         if (A4GL_pop_bool()) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_EXT_CMD Line 102 E_MET_MAIN_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,102);
            goto END_BLOCK_1;
         break;
         }
         /* OTHERWISE... */
         break;
      } /* end of the while for the case */
      END_BLOCK_2: ;
      if (_s) free(_s);
      }
         ERR_CHK_ERROR  { A4GL_chk_err(101,_module_name); }
      CONTINUE_BLOCK_1:    ;
   }
   END_BLOCK_1:    ;
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CLEAR_CMD Line 105 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,105);
   aclfgli_clr_err_flg();
   A4GL_current_window("screen");
   A4GL_clr_window("screen");
      ERR_CHK_ERROR  { A4GL_chk_err(105,_module_name); }
   A4GLSTK_popFunction_nl(0, 107);
   A4GL_fgl_end_4gl_0();
   return 0;
}



A4GL_FUNCTION int aclfgl_add_ticket (int _nargs){
void *_blobdata=0;
static char *_functionName = "add_ticket";

struct BINDING _fbind[1]={ /* 0 print_param_g */
{NULL,0,0,0,0,0,NULL}
};
char *_paramnames[1]={ /* 0 */ /*print_param_g */
0
};
 void *_objData[]={
NULL};
A4GLSTK_pushFunction_v2(_functionName,_paramnames,_nargs,_module_name,108,_objData);
if (_nargs!=0) {A4GL_set_status(-3002,0);A4GL_pop_args(_nargs);A4GLSTK_popFunction_nl(0,108);return -1;}

   init_module_variables();
   /* Print nullify */
   {int _lstatus=a4gl_status;
   A4GL_pop_params_and_save_blobs(_fbind,0,&_blobdata);
   if (_lstatus!=a4gl_status) { A4GL_chk_err(108,_module_name);  }
   }

   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 109 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,109);
   aclfgli_clr_err_flg();
   aclfgli_clr_err_flg();
   A4GL_setnull(1,&ticket.ticket_no,0x0); /*1 */
   A4GL_setnull(0,&ticket.status,0xa); /*1 */
   A4GL_setnull(0,&ticket.ttype,0xa); /*1 */
   A4GL_setnull(0,&ticket.source,0xa); /*1 */
   A4GL_setnull(0,&ticket.scope,0xa); /*1 */
   A4GL_setnull(0,&ticket.site,0x19); /*1 */
   A4GL_setnull(0,&ticket.owner,0xa); /*1 */
   A4GL_setnull(7,&ticket.date_open,0x0); /*1 */
   A4GL_setnull(0,&ticket.time_open,0x5); /*1 */
   A4GL_setnull(7,&ticket.date_start,0x0); /*1 */
   A4GL_setnull(0,&ticket.time_start,0x5); /*1 */
   A4GL_setnull(7,&ticket.date_closed,0x0); /*1 */
   A4GL_setnull(0,&ticket.time_closed,0x5); /*1 */
   A4GL_setnull(7,&ticket.date_end,0x0); /*1 */
   A4GL_setnull(0,&ticket.time_end,0x5); /*1 */
   A4GL_setnull(0,&ticket.notifications,0x78); /*1 */
   A4GL_setnull(0,&ticket.problem,0x12c); /*1 */
   A4GL_setnull(0,&ticket.closecode,0xf); /*1 */
   A4GL_setnull(0,&ticket.fixer,0xa); /*1 */
   A4GL_setnull(0,&ticket.solution,0x12c); /*1 */
   A4GL_setnull(0,&ticket.notify,0x1); /*1 */
      ERR_CHK_ERROR  { A4GL_chk_err(109,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(109,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 110 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,110);
   aclfgli_clr_err_flg();
   A4GL_push_char("open");

   A4GL_pop_var2(&ticket.status,0,10);
      ERR_CHK_ERROR  { A4GL_chk_err(110,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 111 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,111);
   aclfgli_clr_err_flg();
   A4GL_push_char("unplanned");

   A4GL_pop_var2(&ticket.ttype,0,10);
      ERR_CHK_ERROR  { A4GL_chk_err(111,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 112 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,112);
   aclfgli_clr_err_flg();
   A4GL_push_char("noc");

   A4GL_pop_var2(&ticket.source,0,10);
      ERR_CHK_ERROR  { A4GL_chk_err(112,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 113 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,113);
   aclfgli_clr_err_flg();
   A4GL_push_char("1site");

   A4GL_pop_var2(&ticket.scope,0,10);
      ERR_CHK_ERROR  { A4GL_chk_err(113,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 114 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,114);
   aclfgli_clr_err_flg();
   A4GL_push_char("fixme");

   A4GL_pop_var2(&ticket.owner,0,10);
      ERR_CHK_ERROR  { A4GL_chk_err(114,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 115 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,115);
   aclfgli_clr_err_flg();
   A4GL_push_today();

   A4GL_pop_var2(&ticket.date_open,7,0);
      ERR_CHK_ERROR  { A4GL_chk_err(115,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 116 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,116);
   aclfgli_clr_err_flg();
   A4GL_push_time_expr();

   A4GL_pop_var2(&ticket.time_open,0,5);
      ERR_CHK_ERROR  { A4GL_chk_err(116,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 117 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,117);
   aclfgli_clr_err_flg();
   A4GL_push_today();

   A4GL_pop_var2(&ticket.date_start,7,0);
      ERR_CHK_ERROR  { A4GL_chk_err(117,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 118 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,118);
   aclfgli_clr_err_flg();
   A4GL_push_time_expr();

   A4GL_pop_var2(&ticket.time_start,0,5);
      ERR_CHK_ERROR  { A4GL_chk_err(118,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 119 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,119);
   aclfgli_clr_err_flg();
   A4GL_push_char("nearnet-ops,nearnet-outages");

   A4GL_pop_var2(&ticket.notifications,0,120);
      ERR_CHK_ERROR  { A4GL_chk_err(119,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 120 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,120);
   aclfgli_clr_err_flg();
   A4GL_push_char("N");

   A4GL_pop_var2(&ticket.notify,0,1);
      ERR_CHK_ERROR  { A4GL_chk_err(120,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_INPUT_CMD Line 121 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,121);
   aclfgli_clr_err_flg();
   {
   int _fld_dr= -100;
   int _exec_block= 0;
   char *_fldname;char *_curr_win;int _sf;
   int _attr=-1;
   static struct BINDING ibind[21]={

   {NULL,1,0,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,25,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,0,120,0,0,0,NULL},
   {NULL,0,300,0,0,0,NULL},
   {NULL,0,15,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,300,0,0,0,NULL},
   {NULL,0,1,0,0,0,NULL}
   };
   char _sio_2[610];
   char _inp_io_type='I';
   char *_sio_kw_2="s_screenio";
   struct s_field_name _fldlist[]={
      {"ticket_no",1},
      {"status",1},
      {"ttype",1},
      {"source",1},
      {"scope",1},
      {"site",1},
      {"owner",1},
      {"date_open",1},
      {"time_open",1},
      {"date_start",1},
      {"time_start",1},
      {"date_closed",1},
      {"time_closed",1},
      {"date_end",1},
      {"time_end",1},
      {"notifications",1},
      {"problem",1},
      {"closecode",1},
      {"fixer",1},
      {"solution",1},
      {"notify",1},
      {NULL,0}
   };
   int _forminit=1;
   static struct aclfgl_event_list _sio_evt[]={
   {-97,1,0,"ticket_no"},
   {-98,2,0,"notify"},
   {-94,3,0,""},
   {0}
   };
   memset(_sio_2,0,sizeof(_sio_2));
   while(_fld_dr!=0){
      if (_fld_dr== -100) {
         /* input by name */

         ibind[0].ptr=&ticket.ticket_no;

         ibind[1].ptr=&ticket.status;

         ibind[2].ptr=&ticket.ttype;

         ibind[3].ptr=&ticket.source;

         ibind[4].ptr=&ticket.scope;

         ibind[5].ptr=&ticket.site;

         ibind[6].ptr=&ticket.owner;

         ibind[7].ptr=&ticket.date_open;

         ibind[8].ptr=&ticket.time_open;

         ibind[9].ptr=&ticket.date_start;

         ibind[10].ptr=&ticket.time_start;

         ibind[11].ptr=&ticket.date_closed;

         ibind[12].ptr=&ticket.time_closed;

         ibind[13].ptr=&ticket.date_end;

         ibind[14].ptr=&ticket.time_end;

         ibind[15].ptr=&ticket.notifications;

         ibind[16].ptr=&ticket.problem;

         ibind[17].ptr=&ticket.closecode;

         ibind[18].ptr=&ticket.fixer;

         ibind[19].ptr=&ticket.solution;

         ibind[20].ptr=&ticket.notify;
         _curr_win=A4GL_get_currwin_name();
         SET("s_screenio",&_sio_2,"currform",A4GL_get_curr_form(1));
         if (GET_AS_INT("s_screenio",&_sio_2,"currform")==0) break;
         SET("s_screenio",&_sio_2,"vars",ibind);
         SET("s_screenio",&_sio_2,"attrib",_attr);
         SET("s_screenio",&_sio_2,"novars",21);
         SET("s_screenio",&_sio_2,"help_no",0);
         SET("s_screenio",&_sio_2,"processed_onkey",0);
         SET("s_screenio",&_sio_2,"field_list",0);
         SET("s_screenio",&_sio_2,"current_field_display",A4GL_get_option_value('c'));
         SET("s_screenio",&_sio_2,"currentfield",0);
         SET("s_screenio",&_sio_2,"currentmetrics",0);
         SET("s_screenio",&_sio_2,"mode",2);
         SET("s_screenio",&_sio_2,"nfields",A4GL_gen_field_chars((void ***)GETPTR("s_screenio",&_sio_2,"field_list"),(void *)GET("s_screenio",&_sio_2,"currform"),"ticket_no",0,"status",0,"ttype",0,"source",0,"scope",0,"site",0,"owner",0,"date_open",0,"time_open",0,"date_start",0,"time_start",0,"date_closed",0,"time_closed",0,"date_end",0,"time_end",0,"notifications",0,"problem",0,"closecode",0,"fixer",0,"solution",0,"notify",0,NULL));
         if (GET_AS_INT("s_screenio",&_sio_2,"nfields")==-1) break;
         _sf=A4GL_set_fields(&_sio_2); A4GL_debug("_sf=%d",_sf);if(_sf==0) {_fld_dr=0;break;}
         _fld_dr= -1;_exec_block=0;
      } /* end of initialization */
      if (_exec_block==1) {
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_NEXT_FIELD_CMD Line 122 E_MET_FUNCTION_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,122);
         aclfgli_clr_err_flg();

         A4GL_req_field(&_sio_2,_inp_io_type,'+',"+",NULL,0);
         _fld_dr= -1;_exec_block= -1;
         goto CONTINUE_BLOCK_5;
            ERR_CHK_ERROR  { A4GL_chk_err(122,_module_name); }
      }
      if (_exec_block==2) {
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_NEXT_FIELD_CMD Line 123 E_MET_FUNCTION_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,123);
         aclfgli_clr_err_flg();

         A4GL_req_field(&_sio_2,_inp_io_type,'!',"status",0,NULL,0);
         _fld_dr= -1;_exec_block= -1;
         goto CONTINUE_BLOCK_5;
            ERR_CHK_ERROR  { A4GL_chk_err(123,_module_name); }
      }
      if (_exec_block==3) { break; }
      A4GL_ensure_current_window_is(_curr_win);
      _exec_block=A4GL_form_loop_v2(&_sio_2,_forminit,_sio_evt);_forminit=0;
      if (_exec_block>0) _fld_dr=A4GL_get_event_type(_sio_evt,_exec_block); else _fld_dr= -1;
      CONTINUE_BLOCK_5:    ;   /* add_continue */
   }
   END_BLOCK_5:    ;   /* add_continue */
   A4GL_finish_screenio(&_sio_2,_sio_kw_2);
   }
      ERR_CHK_ERROR  { A4GL_chk_err(121,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_SELECT_CMD Line 126 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,126);
   aclfgli_clr_err_flg();
   {
   static struct BINDING obind[1]={

   {NULL,1,0,0,0,0,NULL}
   };

   obind[0].ptr= & ticket.ticket_no;
   A4GL_execute_implicit_select(A4GL_prepare_select(NULL,0,obind,1,"SELECT (1+MAX(ticket_no))  FROM tickets",_module_name,126,0,0),1); /* 0 */
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(126,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(126,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_INSERT_CMD Line 128 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,128);
   aclfgli_clr_err_flg();
   {
   static struct BINDING ibind[21]={

   {NULL,1,0,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,25,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,0,120,0,0,0,NULL},
   {NULL,0,300,0,0,0,NULL},
   {NULL,0,15,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,300,0,0,0,NULL},
   {NULL,0,1,0,0,0,NULL}
   };

   ibind[0].ptr=&ticket.ticket_no;

   ibind[1].ptr=&ticket.status;

   ibind[2].ptr=&ticket.ttype;

   ibind[3].ptr=&ticket.source;

   ibind[4].ptr=&ticket.scope;

   ibind[5].ptr=&ticket.site;

   ibind[6].ptr=&ticket.owner;

   ibind[7].ptr=&ticket.date_open;

   ibind[8].ptr=&ticket.time_open;

   ibind[9].ptr=&ticket.date_start;

   ibind[10].ptr=&ticket.time_start;

   ibind[11].ptr=&ticket.date_closed;

   ibind[12].ptr=&ticket.time_closed;

   ibind[13].ptr=&ticket.date_end;

   ibind[14].ptr=&ticket.time_end;

   ibind[15].ptr=&ticket.notifications;

   ibind[16].ptr=&ticket.problem;

   ibind[17].ptr=&ticket.closecode;

   ibind[18].ptr=&ticket.fixer;

   ibind[19].ptr=&ticket.solution;

   ibind[20].ptr=&ticket.notify;
   A4GL_execute_implicit_sql(A4GL_prepare_select(ibind,21,0,0,"INSERT INTO tickets (ticket_no,status,ttype,source,scope,site,owner,date_open,time_open,date_start,time_start,date_closed,time_closed,date_end,time_end,notifications,problem,closecode,fixer,solution,notify) VALUES (   ?,   ?,   ?,   ?,   ?,   ?,   ?,   ?,   ?,   ?,   ?,   ?,   ?,   ?,   ?,   ?,   ?,   ?,   ?,   ?,   ?)",_module_name,128,0,0),1,0,0);
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(128,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(128,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 141 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,141);
   aclfgli_clr_err_flg();
   /* done print expr */
   {int _retvars;A4GL_set_status(0,0);
   A4GLSTK_setCurrentLine(_module_name,141);
   _retvars=aclfgl_display_ticket(0);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(141,_module_name); }
   A4GLSTK_popFunction_nl(0,143);
   A4GL_copy_back_blobs(_blobdata,0);
   return 0;
}




A4GL_FUNCTION int aclfgl_add_note (int _nargs){
void *_blobdata=0;
static char *_functionName = "add_note";
long next_note_no ;

struct BINDING _fbind[1]={ /* 0 print_param_g */
{NULL,0,0,0,0,0,NULL}
};
char *_paramnames[1]={ /* 0 */ /*print_param_g */
0
};
 void *_objData[]={
NULL};
A4GLSTK_pushFunction_v2(_functionName,_paramnames,_nargs,_module_name,144,_objData);
if (_nargs!=0) {A4GL_set_status(-3002,0);A4GL_pop_args(_nargs);A4GLSTK_popFunction_nl(0,144);return -1;}

   init_module_variables();
   /* Print nullify */
   next_note_no=0;
   {int _lstatus=a4gl_status;
   A4GL_pop_params_and_save_blobs(_fbind,0,&_blobdata);
   if (_lstatus!=a4gl_status) { A4GL_chk_err(144,_module_name);  }
   }

   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 147 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,147);
   aclfgli_clr_err_flg();
   aclfgli_clr_err_flg();
   A4GL_setnull(1,&note.ticket_no,0x0); /*1 */
   A4GL_setnull(1,&note.note_no,0x0); /*1 */
   A4GL_setnull(0,&note.name,0x14); /*1 */
   A4GL_setnull(7,&note.note_date,0x0); /*1 */
   A4GL_setnull(0,&note.note_time,0x5); /*1 */
   A4GL_setnull(0,&note.note,0x21c); /*1 */
   A4GL_setnull(0,&note.notify,0x1); /*1 */
      ERR_CHK_ERROR  { A4GL_chk_err(147,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(147,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 148 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,148);
   aclfgli_clr_err_flg();

   A4GL_push_int(ticket.ticket_no);

   A4GL_pop_var2(&note.ticket_no,1,0);
      ERR_CHK_ERROR  { A4GL_chk_err(148,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 149 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,149);
   aclfgli_clr_err_flg();
   A4GL_push_char("fixme");

   A4GL_pop_var2(&note.name,0,20);
      ERR_CHK_ERROR  { A4GL_chk_err(149,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 150 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,150);
   aclfgli_clr_err_flg();
   A4GL_push_today();

   A4GL_pop_var2(&note.note_date,7,0);
      ERR_CHK_ERROR  { A4GL_chk_err(150,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 151 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,151);
   aclfgli_clr_err_flg();
   A4GL_push_time_expr();

   A4GL_pop_var2(&note.note_time,0,5);
      ERR_CHK_ERROR  { A4GL_chk_err(151,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 152 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,152);
   aclfgli_clr_err_flg();
   A4GL_push_empty_char();

   A4GL_pop_var2(&note.note,0,540);
      ERR_CHK_ERROR  { A4GL_chk_err(152,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 153 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,153);
   aclfgli_clr_err_flg();
   A4GL_push_char("Y");

   A4GL_pop_var2(&note.notify,0,1);
      ERR_CHK_ERROR  { A4GL_chk_err(153,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_INPUT_CMD Line 154 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,154);
   aclfgli_clr_err_flg();
   {
   int _fld_dr= -100;
   int _exec_block= 0;
   char *_fldname;char *_curr_win;int _sf;
   int _attr=-1;
   static struct BINDING ibind[7]={

   {NULL,1,0,0,0,0,NULL},
   {NULL,1,0,0,0,0,NULL},
   {NULL,0,20,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,0,540,0,0,0,NULL},
   {NULL,0,1,0,0,0,NULL}
   };
   char _sio_2[610];
   char _inp_io_type='I';
   char *_sio_kw_2="s_screenio";
   struct s_field_name _fldlist[]={
      {"ticket_no",1},
      {"note_no",1},
      {"name",1},
      {"note_date",1},
      {"note_time",1},
      {"note",1},
      {"notify",1},
      {NULL,0}
   };
   int _forminit=1;
   static struct aclfgl_event_list _sio_evt[]={
   {-97,1,0,"ticket_no"},
   {-97,2,0,"note_no"},
   {-98,3,0,"notify"},
   {-94,4,0,""},
   {0}
   };
   memset(_sio_2,0,sizeof(_sio_2));
   while(_fld_dr!=0){
      if (_fld_dr== -100) {
         /* input by name */

         ibind[0].ptr=&note.ticket_no;

         ibind[1].ptr=&note.note_no;

         ibind[2].ptr=&note.name;

         ibind[3].ptr=&note.note_date;

         ibind[4].ptr=&note.note_time;

         ibind[5].ptr=&note.note;

         ibind[6].ptr=&note.notify;
         _curr_win=A4GL_get_currwin_name();
         SET("s_screenio",&_sio_2,"currform",A4GL_get_curr_form(1));
         if (GET_AS_INT("s_screenio",&_sio_2,"currform")==0) break;
         SET("s_screenio",&_sio_2,"vars",ibind);
         SET("s_screenio",&_sio_2,"attrib",_attr);
         SET("s_screenio",&_sio_2,"novars",7);
         SET("s_screenio",&_sio_2,"help_no",0);
         SET("s_screenio",&_sio_2,"processed_onkey",0);
         SET("s_screenio",&_sio_2,"field_list",0);
         SET("s_screenio",&_sio_2,"current_field_display",A4GL_get_option_value('c'));
         SET("s_screenio",&_sio_2,"currentfield",0);
         SET("s_screenio",&_sio_2,"currentmetrics",0);
         SET("s_screenio",&_sio_2,"mode",2);
         SET("s_screenio",&_sio_2,"nfields",A4GL_gen_field_chars((void ***)GETPTR("s_screenio",&_sio_2,"field_list"),(void *)GET("s_screenio",&_sio_2,"currform"),"ticket_no",0,"note_no",0,"name",0,"note_date",0,"note_time",0,"note",0,"notify",0,NULL));
         if (GET_AS_INT("s_screenio",&_sio_2,"nfields")==-1) break;
         _sf=A4GL_set_fields(&_sio_2); A4GL_debug("_sf=%d",_sf);if(_sf==0) {_fld_dr=0;break;}
         _fld_dr= -1;_exec_block=0;
      } /* end of initialization */
      if (_exec_block==1) {
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_NEXT_FIELD_CMD Line 155 E_MET_FUNCTION_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,155);
         aclfgli_clr_err_flg();

         A4GL_req_field(&_sio_2,_inp_io_type,'+',"+",NULL,0);
         _fld_dr= -1;_exec_block= -1;
         goto CONTINUE_BLOCK_6;
            ERR_CHK_ERROR  { A4GL_chk_err(155,_module_name); }
      }
      if (_exec_block==2) {
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_NEXT_FIELD_CMD Line 156 E_MET_FUNCTION_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,156);
         aclfgli_clr_err_flg();

         A4GL_req_field(&_sio_2,_inp_io_type,'+',"+",NULL,0);
         _fld_dr= -1;_exec_block= -1;
         goto CONTINUE_BLOCK_6;
            ERR_CHK_ERROR  { A4GL_chk_err(156,_module_name); }
      }
      if (_exec_block==3) {
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_NEXT_FIELD_CMD Line 157 E_MET_FUNCTION_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,157);
         aclfgli_clr_err_flg();

         A4GL_req_field(&_sio_2,_inp_io_type,'!',"name",0,NULL,0);
         _fld_dr= -1;_exec_block= -1;
         goto CONTINUE_BLOCK_6;
            ERR_CHK_ERROR  { A4GL_chk_err(157,_module_name); }
      }
      if (_exec_block==4) { break; }
      A4GL_ensure_current_window_is(_curr_win);
      _exec_block=A4GL_form_loop_v2(&_sio_2,_forminit,_sio_evt);_forminit=0;
      if (_exec_block>0) _fld_dr=A4GL_get_event_type(_sio_evt,_exec_block); else _fld_dr= -1;
      CONTINUE_BLOCK_6:    ;   /* add_continue */
   }
   END_BLOCK_6:    ;   /* add_continue */
   A4GL_finish_screenio(&_sio_2,_sio_kw_2);
   }
      ERR_CHK_ERROR  { A4GL_chk_err(154,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_SELECT_CMD Line 160 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,160);
   aclfgli_clr_err_flg();
   {
   static struct BINDING ibind[1]={

   {NULL,1,0,0,0,0,NULL}
   };
   static struct BINDING obind[1]={

   {NULL,2,0,0,0,0,NULL}
   };

   ibind[0].ptr=&ticket.ticket_no;

   obind[0].ptr= & next_note_no;
   A4GL_execute_implicit_select(A4GL_prepare_select(ibind,1,obind,1,"SELECT (1+MAX(note_no))  FROM ticket_notes WHERE ticket_no=? ",_module_name,160,0,0),1); /* 0 */
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(160,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(160,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_IF_CMD Line 163 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,163);
   aclfgli_clr_err_flg();

   A4GL_push_long(next_note_no);
   A4GL_pushop(OP_ISNULL);
   if (A4GL_pop_bool()) {
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_LET_CMD Line 164 E_MET_FUNCTION_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,164);
      aclfgli_clr_err_flg();
      A4GL_push_long(1);

      A4GL_pop_var2(&next_note_no,2,0);
         ERR_CHK_ERROR  { A4GL_chk_err(164,_module_name); }
   }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 167 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,167);
   aclfgli_clr_err_flg();

   A4GL_push_long(next_note_no);

   A4GL_pop_var2(&note.note_no,1,0);
      ERR_CHK_ERROR  { A4GL_chk_err(167,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_INSERT_CMD Line 170 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,170);
   aclfgli_clr_err_flg();
   {
   static struct BINDING ibind[7]={

   {NULL,1,0,0,0,0,NULL},
   {NULL,1,0,0,0,0,NULL},
   {NULL,0,20,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,0,540,0,0,0,NULL},
   {NULL,0,1,0,0,0,NULL}
   };

   ibind[0].ptr=&note.ticket_no;

   ibind[1].ptr=&note.note_no;

   ibind[2].ptr=&note.name;

   ibind[3].ptr=&note.note_date;

   ibind[4].ptr=&note.note_time;

   ibind[5].ptr=&note.note;

   ibind[6].ptr=&note.notify;
   A4GL_execute_implicit_sql(A4GL_prepare_select(ibind,7,0,0,"INSERT INTO ticket_notes (ticket_no,note_no,name,note_date,note_time,note,notify) VALUES (   ?,   ?,   ?,   ?,   ?,   ?,   ?)",_module_name,170,0,0),1,0,0);
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(170,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(170,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_IF_CMD Line 177 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,177);
   aclfgli_clr_err_flg();

   A4GL_push_variable(&note.notify,0x10000);
   A4GL_push_char("Y");
   A4GL_pushop(OP_EQUAL);
   if (A4GL_pop_bool()) {
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_START_CMD Line 178 E_MET_FUNCTION_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,178);
      aclfgli_clr_err_flg();
      A4GL_push_char("|");
      A4GL_push_char("/bin/mailx -t");
      {int _pl; int _rm; int _lm; int _tm; int _bm;
      A4GL_push_long(-1);
      _pl=A4GL_pop_long();
      A4GL_push_long(-1);
      _lm=A4GL_pop_long();
      A4GL_push_long(-1);
      _rm=A4GL_pop_long();
      A4GL_push_long(-1);
      _tm=A4GL_pop_long();
      A4GL_push_long(-1);
      _bm=A4GL_pop_long();
      A4GL_set_report_dim(_pl,_lm,_rm,_tm,_bm,"");
      aclfgl_email_ticket_note(2,REPORT_START);
         ERR_CHK_SQLERROR  { A4GL_chk_err(178,_module_name); }
         ERR_CHK_ERROR  { A4GL_chk_err(178,_module_name); }
      }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_OUTPUT_CMD Line 179 E_MET_FUNCTION_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,179);
      aclfgli_clr_err_flg();
      aclfgl_email_ticket_note(0,REPORT_SENDDATA);
      if (aclfgli_get_err_flg()) {
         ERR_CHK_SQLERROR  { A4GL_chk_err(179,_module_name); }
         ERR_CHK_ERROR  { A4GL_chk_err(179,_module_name); }
      } else {
         ERR_CHK_ERROR  { A4GL_chk_err(179,_module_name); }
      }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_FINISH_CMD Line 180 E_MET_FUNCTION_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,180);
      aclfgli_clr_err_flg();
      aclfgl_email_ticket_note(0,REPORT_FINISH);
      if (aclfgli_get_err_flg()) {
         ERR_CHK_SQLERROR  { A4GL_chk_err(180,_module_name); }
         ERR_CHK_ERROR  { A4GL_chk_err(180,_module_name); }
      } else {
         ERR_CHK_ERROR  { A4GL_chk_err(180,_module_name); }
      }
   }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 183 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,183);
   aclfgli_clr_err_flg();
   /* done print expr */
   {int _retvars;A4GL_set_status(0,0);
   A4GLSTK_setCurrentLine(_module_name,183);
   _retvars=aclfgl_get_notes_for_current_ticket(0);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(183,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 184 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,184);
   aclfgli_clr_err_flg();
   /* done print expr */
   {int _retvars;A4GL_set_status(0,0);
   A4GLSTK_setCurrentLine(_module_name,184);
   _retvars=aclfgl_last_note(0);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(184,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 185 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,185);
   aclfgli_clr_err_flg();
   /* done print expr */
   {int _retvars;A4GL_set_status(0,0);
   A4GLSTK_setCurrentLine(_module_name,185);
   _retvars=aclfgl_display_note(0);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(185,_module_name); }
   A4GLSTK_popFunction_nl(0,187);
   A4GL_copy_back_blobs(_blobdata,0);
   return 0;
}




A4GL_FUNCTION int aclfgl_query_ticket (int _nargs){
void *_blobdata=0;
static char *_functionName = "query_ticket";
char criteria [32000+1];
char query [767+1];

struct BINDING _fbind[1]={ /* 0 print_param_g */
{NULL,0,0,0,0,0,NULL}
};
char *_paramnames[1]={ /* 0 */ /*print_param_g */
0
};
 void *_objData[]={
NULL};
A4GLSTK_pushFunction_v2(_functionName,_paramnames,_nargs,_module_name,188,_objData);
if (_nargs!=0) {A4GL_set_status(-3002,0);A4GL_pop_args(_nargs);A4GLSTK_popFunction_nl(0,188);return -1;}

   init_module_variables();
   /* Print nullify */
   A4GL_setnull(0,&criteria,0x7d00); /*1 */
   A4GL_setnull(0,&query,0x2ff); /*1 */
   {int _lstatus=a4gl_status;
   A4GL_pop_params_and_save_blobs(_fbind,0,&_blobdata);
   if (_lstatus!=a4gl_status) { A4GL_chk_err(188,_module_name);  }
   }

   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CLOSE_SQL_CMD Line 185 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,185);
   aclfgli_clr_err_flg();
   A4GL_close_cursor(aclfgli_str_to_id("ticket_cursor"),1);
      ERR_CHK_SQLERROR  { A4GL_chk_err(185,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(185,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CONSTRUCT_CMD Line 193 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,193);
   aclfgli_clr_err_flg();
   {
      static struct BINDING ibind[1]={

      {NULL,0,32000,0,0,0,NULL}
      };
      struct s_constr_list constr_flds[21]={
      {"tickets","ticket_no"}
      ,
      {"tickets","status"}
      ,
      {"tickets","ttype"}
      ,
      {"tickets","owner"}
      ,
      {"tickets","scope"}
      ,
      {"tickets","date_open"}
      ,
      {"tickets","time_open"}
      ,
      {"tickets","date_closed"}
      ,
      {"tickets","time_closed"}
      ,
      {"tickets","notifications"}
      ,
      {"tickets","problem"}
      ,
      {"tickets","solution"}
      ,
      {"tickets","fixer"}
      ,
      {"tickets","notify"}
      ,
      {"tickets","date_start"}
      ,
      {"tickets","time_start"}
      ,
      {"tickets","date_end"}
      ,
      {"tickets","time_end"}
      ,
      {"tickets","site"}
      ,
      {"tickets","source"}
      ,
      {"tickets","closecode"}
      };
      int _attr=-1;
      int _fld_dr= -100;int _exec_block= 0;char *_fldname;int _sf;
      char _sio_2[610]; char *_curr_win=0; char _inp_io_type='C'; char *_sio_kw_2="s_screenio";
      void *_filterfunc=NULL;
      int _forminit=1;
      static struct aclfgl_event_list _sio_evt[]={
      {-94,1,0,""},
       {0}};
      struct s_field_name _fldlist[]={
         {"tickets.*",1},
         {NULL,0}
      };

      ibind[0].ptr=&criteria;
      memset(_sio_2,0,sizeof(_sio_2));
      while(_fld_dr!=0){
         if (_exec_block == 0) {
            _curr_win=A4GL_get_currwin_name();
            SET("s_screenio",_sio_2,"vars",ibind);
            SET("s_screenio",_sio_2,"novars",21);
            SET("s_screenio",_sio_2,"attrib",_attr);
            SET("s_screenio",_sio_2,"currform",A4GL_get_curr_form(1));
            SET("s_screenio",_sio_2,"currentfield",0);
            SET("s_screenio",&_sio_2,"help_no",0);
            SET("s_screenio",_sio_2,"currentmetrics",0);
            SET("s_screenio",_sio_2,"constr",constr_flds);
            SET("s_screenio",_sio_2,"mode",3);
            SET("s_screenio",_sio_2,"processed_onkey",0);
            SET("s_screenio",_sio_2,"field_list",0);
            SET("s_screenio",&_sio_2,"current_field_display",A4GL_get_option_value('c'));
            SET("s_screenio",_sio_2,"callback_function", _filterfunc);
            SET("s_screenio",_sio_2,"nfields",A4GL_gen_field_chars((void ***)GETPTR("s_screenio",_sio_2,"field_list"),(void *)GET("s_screenio",_sio_2,"currform"),"tickets.*",0,NULL));
            _sf=A4GL_set_fields(&_sio_2); A4GL_debug("_sf=%d",_sf);if(_sf==0) {_fld_dr=0;break;}
            _fld_dr= -1;
         } /* end of initialization */
         if (_exec_block== 1) { break; }
         A4GL_ensure_current_window_is(_curr_win);
         _exec_block = A4GL_form_loop_v2(&_sio_2,_forminit,_sio_evt);_forminit=0;
         if (_exec_block>0) _fld_dr=A4GL_get_event_type(_sio_evt,_exec_block); else _fld_dr= -1;
         CONTINUE_BLOCK_7:    ;   /* add_continue */
      }
      END_BLOCK_7:    ;   /* add_continue */
       A4GL_push_constr(&_sio_2);
       A4GL_pop_params(ibind,1);
      A4GL_finish_screenio(&_sio_2,_sio_kw_2);
   }
      ERR_CHK_ERROR  { A4GL_chk_err(193,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 193 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,193);
   aclfgli_clr_err_flg();
   A4GL_push_char("SELECT ticket_no, status, ttype, source, scope, site, owner, date_open, time_open, date_start, time_start, date_closed, time_closed, date_end, time_end, notifications, problem, closecode, fixer, solution, notify from tickets WHERE ");

   A4GL_push_variable(&criteria,0x7d000000);
   A4GL_pushop(OP_CONCAT); /* 1 */

   A4GL_pop_var2(&query,0,767);
      ERR_CHK_ERROR  { A4GL_chk_err(193,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_PREPARE_CMD Line 194 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,194);
   aclfgli_clr_err_flg();
   {char *_sql;

   A4GL_push_variable(&query,0x2ff0000);
   _sql=A4GL_char_pop();
   A4GL_add_prepare(A4GL_get_ident("ntickets","statement","ntickets_statement"),(void *)A4GL_prepare_select(0,0,0,0,_sql,_module_name,194,0,0));
   free(_sql);
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(194,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(194,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_DECLARE_CMD Line 195 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,195);
   aclfgli_clr_err_flg();
   {
   A4GL_declare_cursor(0,A4GL_find_prepare(A4GL_get_ident("ntickets","statement","ntickets_statement")),0,aclfgli_str_to_id("ticket_cursor"));
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(195,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(195,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_OPEN_CURSOR_CMD Line 196 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,196);
   aclfgli_clr_err_flg();
   A4GL_open_cursor(aclfgli_str_to_id("ticket_cursor"),0,0);
      ERR_CHK_SQLERROR  { A4GL_chk_err(196,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(196,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_EXECUTE_CMD Line 197 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,197);
   aclfgli_clr_err_flg();
   A4GL_execute_sql(A4GL_get_ident("ntickets","statement","ntickets_statement"),0,0);
      ERR_CHK_SQLERROR  { A4GL_chk_err(197,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(197,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 198 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,198);
   aclfgli_clr_err_flg();
   /* done print expr */
   {int _retvars;A4GL_set_status(0,0);
   A4GLSTK_setCurrentLine(_module_name,198);
   _retvars=aclfgl_first_ticket(0);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(198,_module_name); }
   A4GLSTK_popFunction_nl(0,200);
   A4GL_copy_back_blobs(_blobdata,0);
   return 0;
}




A4GL_FUNCTION int aclfgl_get_notes_for_current_ticket (int _nargs){
void *_blobdata=0;
static char *_functionName = "get_notes_for_current_ticket";
char query [767+1];

struct BINDING _fbind[1]={ /* 0 print_param_g */
{NULL,0,0,0,0,0,NULL}
};
char *_paramnames[1]={ /* 0 */ /*print_param_g */
0
};
 void *_objData[]={
NULL};
A4GLSTK_pushFunction_v2(_functionName,_paramnames,_nargs,_module_name,201,_objData);
if (_nargs!=0) {A4GL_set_status(-3002,0);A4GL_pop_args(_nargs);A4GLSTK_popFunction_nl(0,201);return -1;}

   init_module_variables();
   /* Print nullify */
   A4GL_setnull(0,&query,0x2ff); /*1 */
   {int _lstatus=a4gl_status;
   A4GL_pop_params_and_save_blobs(_fbind,0,&_blobdata);
   if (_lstatus!=a4gl_status) { A4GL_chk_err(201,_module_name);  }
   }

   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CLOSE_SQL_CMD Line 198 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,198);
   aclfgli_clr_err_flg();
   A4GL_close_cursor(aclfgli_str_to_id("note_cursor"),1);
      ERR_CHK_SQLERROR  { A4GL_chk_err(198,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(198,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 204 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,204);
   aclfgli_clr_err_flg();
   aclfgli_clr_err_flg();
   A4GL_setnull(1,&note.ticket_no,0x0); /*1 */
   A4GL_setnull(1,&note.note_no,0x0); /*1 */
   A4GL_setnull(0,&note.name,0x14); /*1 */
   A4GL_setnull(7,&note.note_date,0x0); /*1 */
   A4GL_setnull(0,&note.note_time,0x5); /*1 */
   A4GL_setnull(0,&note.note,0x21c); /*1 */
   A4GL_setnull(0,&note.notify,0x1); /*1 */
      ERR_CHK_ERROR  { A4GL_chk_err(204,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(204,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 205 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,205);
   aclfgli_clr_err_flg();
   A4GL_push_char("SELECT * FROM ticket_notes WHERE ticket_notes.ticket_no=");

   A4GL_push_int(ticket.ticket_no);
   A4GL_pushop(OP_CONCAT); /* 1 */
   A4GL_push_char(" ORDER BY note_no");
   A4GL_pushop(OP_CONCAT); /* 1 */

   A4GL_pop_var2(&query,0,767);
      ERR_CHK_ERROR  { A4GL_chk_err(205,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_PREPARE_CMD Line 206 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,206);
   aclfgli_clr_err_flg();
   {char *_sql;

   A4GL_push_variable(&query,0x2ff0000);
   _sql=A4GL_char_pop();
   A4GL_add_prepare(A4GL_get_ident("ntickets","statement","ntickets_statement"),(void *)A4GL_prepare_select(0,0,0,0,_sql,_module_name,206,0,0));
   free(_sql);
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(206,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(206,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_DECLARE_CMD Line 207 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,207);
   aclfgli_clr_err_flg();
   {
   A4GL_declare_cursor(0,A4GL_find_prepare(A4GL_get_ident("ntickets","statement","ntickets_statement")),0,aclfgli_str_to_id("note_cursor"));
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(207,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(207,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_OPEN_CURSOR_CMD Line 208 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,208);
   aclfgli_clr_err_flg();
   A4GL_open_cursor(aclfgli_str_to_id("note_cursor"),0,0);
      ERR_CHK_SQLERROR  { A4GL_chk_err(208,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(208,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_EXECUTE_CMD Line 209 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,209);
   aclfgli_clr_err_flg();
   A4GL_execute_sql(A4GL_get_ident("ntickets","statement","ntickets_statement"),0,0);
      ERR_CHK_SQLERROR  { A4GL_chk_err(209,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(209,_module_name); }
   A4GLSTK_popFunction_nl(0,211);
   A4GL_copy_back_blobs(_blobdata,0);
   return 0;
}




A4GL_FUNCTION int aclfgl_first_ticket (int _nargs){
void *_blobdata=0;
static char *_functionName = "first_ticket";

struct BINDING _fbind[1]={ /* 0 print_param_g */
{NULL,0,0,0,0,0,NULL}
};
char *_paramnames[1]={ /* 0 */ /*print_param_g */
0
};
 void *_objData[]={
NULL};
A4GLSTK_pushFunction_v2(_functionName,_paramnames,_nargs,_module_name,212,_objData);
if (_nargs!=0) {A4GL_set_status(-3002,0);A4GL_pop_args(_nargs);A4GLSTK_popFunction_nl(0,212);return -1;}

   init_module_variables();
   /* Print nullify */
   {int _lstatus=a4gl_status;
   A4GL_pop_params_and_save_blobs(_fbind,0,&_blobdata);
   if (_lstatus!=a4gl_status) { A4GL_chk_err(212,_module_name);  }
   }

   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_FETCH_CMD Line 213 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,213);
   aclfgli_clr_err_flg();
   {
   static struct BINDING obind[21]={

   {NULL,1,0,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,25,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,0,120,0,0,0,NULL},
   {NULL,0,300,0,0,0,NULL},
   {NULL,0,15,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,300,0,0,0,NULL},
   {NULL,0,1,0,0,0,NULL}
   };

   obind[0].ptr= & ticket.ticket_no;

   obind[1].ptr= & ticket.status;

   obind[2].ptr= & ticket.ttype;

   obind[3].ptr= & ticket.source;

   obind[4].ptr= & ticket.scope;

   obind[5].ptr= & ticket.site;

   obind[6].ptr= & ticket.owner;

   obind[7].ptr= & ticket.date_open;

   obind[8].ptr= & ticket.time_open;

   obind[9].ptr= & ticket.date_start;

   obind[10].ptr= & ticket.time_start;

   obind[11].ptr= & ticket.date_closed;

   obind[12].ptr= & ticket.time_closed;

   obind[13].ptr= & ticket.date_end;

   obind[14].ptr= & ticket.time_end;

   obind[15].ptr= & ticket.notifications;

   obind[16].ptr= & ticket.problem;

   obind[17].ptr= & ticket.closecode;

   obind[18].ptr= & ticket.fixer;

   obind[19].ptr= & ticket.solution;

   obind[20].ptr= & ticket.notify;
   A4GL_fetch_cursor(aclfgli_str_to_id("ticket_cursor"),1,1,21,obind);
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(213,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(213,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 214 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,214);
   aclfgli_clr_err_flg();
   /* done print expr */
   {int _retvars;A4GL_set_status(0,0);
   A4GLSTK_setCurrentLine(_module_name,214);
   _retvars=aclfgl_get_notes_for_current_ticket(0);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(214,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 215 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,215);
   aclfgli_clr_err_flg();
   /* done print expr */
   {int _retvars;A4GL_set_status(0,0);
   A4GLSTK_setCurrentLine(_module_name,215);
   _retvars=aclfgl_first_note(0);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(215,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 216 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,216);
   aclfgli_clr_err_flg();
   /* done print expr */
   {int _retvars;A4GL_set_status(0,0);
   A4GLSTK_setCurrentLine(_module_name,216);
   _retvars=aclfgl_display_ticket(0);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(216,_module_name); }
   A4GLSTK_popFunction_nl(0,218);
   A4GL_copy_back_blobs(_blobdata,0);
   return 0;
}




A4GL_FUNCTION int aclfgl_next_ticket (int _nargs){
void *_blobdata=0;
static char *_functionName = "next_ticket";

struct BINDING _fbind[1]={ /* 0 print_param_g */
{NULL,0,0,0,0,0,NULL}
};
char *_paramnames[1]={ /* 0 */ /*print_param_g */
0
};
 void *_objData[]={
NULL};
A4GLSTK_pushFunction_v2(_functionName,_paramnames,_nargs,_module_name,219,_objData);
if (_nargs!=0) {A4GL_set_status(-3002,0);A4GL_pop_args(_nargs);A4GLSTK_popFunction_nl(0,219);return -1;}

   init_module_variables();
   /* Print nullify */
   {int _lstatus=a4gl_status;
   A4GL_pop_params_and_save_blobs(_fbind,0,&_blobdata);
   if (_lstatus!=a4gl_status) { A4GL_chk_err(219,_module_name);  }
   }

   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_FETCH_CMD Line 220 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,220);
   aclfgli_clr_err_flg();
   {
   static struct BINDING obind[21]={

   {NULL,1,0,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,25,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,0,120,0,0,0,NULL},
   {NULL,0,300,0,0,0,NULL},
   {NULL,0,15,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,300,0,0,0,NULL},
   {NULL,0,1,0,0,0,NULL}
   };

   obind[0].ptr= & ticket.ticket_no;

   obind[1].ptr= & ticket.status;

   obind[2].ptr= & ticket.ttype;

   obind[3].ptr= & ticket.source;

   obind[4].ptr= & ticket.scope;

   obind[5].ptr= & ticket.site;

   obind[6].ptr= & ticket.owner;

   obind[7].ptr= & ticket.date_open;

   obind[8].ptr= & ticket.time_open;

   obind[9].ptr= & ticket.date_start;

   obind[10].ptr= & ticket.time_start;

   obind[11].ptr= & ticket.date_closed;

   obind[12].ptr= & ticket.time_closed;

   obind[13].ptr= & ticket.date_end;

   obind[14].ptr= & ticket.time_end;

   obind[15].ptr= & ticket.notifications;

   obind[16].ptr= & ticket.problem;

   obind[17].ptr= & ticket.closecode;

   obind[18].ptr= & ticket.fixer;

   obind[19].ptr= & ticket.solution;

   obind[20].ptr= & ticket.notify;
   A4GL_fetch_cursor(aclfgli_str_to_id("ticket_cursor"),2,1,21,obind);
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(220,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(220,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 221 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,221);
   aclfgli_clr_err_flg();
   /* done print expr */
   {int _retvars;A4GL_set_status(0,0);
   A4GLSTK_setCurrentLine(_module_name,221);
   _retvars=aclfgl_get_notes_for_current_ticket(0);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(221,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 222 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,222);
   aclfgli_clr_err_flg();
   /* done print expr */
   {int _retvars;A4GL_set_status(0,0);
   A4GLSTK_setCurrentLine(_module_name,222);
   _retvars=aclfgl_first_note(0);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(222,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 223 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,223);
   aclfgli_clr_err_flg();
   /* done print expr */
   {int _retvars;A4GL_set_status(0,0);
   A4GLSTK_setCurrentLine(_module_name,223);
   _retvars=aclfgl_display_ticket(0);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(223,_module_name); }
   A4GLSTK_popFunction_nl(0,225);
   A4GL_copy_back_blobs(_blobdata,0);
   return 0;
}




A4GL_FUNCTION int aclfgl_prev_ticket (int _nargs){
void *_blobdata=0;
static char *_functionName = "prev_ticket";

struct BINDING _fbind[1]={ /* 0 print_param_g */
{NULL,0,0,0,0,0,NULL}
};
char *_paramnames[1]={ /* 0 */ /*print_param_g */
0
};
 void *_objData[]={
NULL};
A4GLSTK_pushFunction_v2(_functionName,_paramnames,_nargs,_module_name,226,_objData);
if (_nargs!=0) {A4GL_set_status(-3002,0);A4GL_pop_args(_nargs);A4GLSTK_popFunction_nl(0,226);return -1;}

   init_module_variables();
   /* Print nullify */
   {int _lstatus=a4gl_status;
   A4GL_pop_params_and_save_blobs(_fbind,0,&_blobdata);
   if (_lstatus!=a4gl_status) { A4GL_chk_err(226,_module_name);  }
   }

   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_FETCH_CMD Line 227 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,227);
   aclfgli_clr_err_flg();
   {
   static struct BINDING obind[21]={

   {NULL,1,0,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,25,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,0,120,0,0,0,NULL},
   {NULL,0,300,0,0,0,NULL},
   {NULL,0,15,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,300,0,0,0,NULL},
   {NULL,0,1,0,0,0,NULL}
   };

   obind[0].ptr= & ticket.ticket_no;

   obind[1].ptr= & ticket.status;

   obind[2].ptr= & ticket.ttype;

   obind[3].ptr= & ticket.source;

   obind[4].ptr= & ticket.scope;

   obind[5].ptr= & ticket.site;

   obind[6].ptr= & ticket.owner;

   obind[7].ptr= & ticket.date_open;

   obind[8].ptr= & ticket.time_open;

   obind[9].ptr= & ticket.date_start;

   obind[10].ptr= & ticket.time_start;

   obind[11].ptr= & ticket.date_closed;

   obind[12].ptr= & ticket.time_closed;

   obind[13].ptr= & ticket.date_end;

   obind[14].ptr= & ticket.time_end;

   obind[15].ptr= & ticket.notifications;

   obind[16].ptr= & ticket.problem;

   obind[17].ptr= & ticket.closecode;

   obind[18].ptr= & ticket.fixer;

   obind[19].ptr= & ticket.solution;

   obind[20].ptr= & ticket.notify;
   A4GL_fetch_cursor(aclfgli_str_to_id("ticket_cursor"),2,-1,21,obind);
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(227,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(227,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 228 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,228);
   aclfgli_clr_err_flg();
   /* done print expr */
   {int _retvars;A4GL_set_status(0,0);
   A4GLSTK_setCurrentLine(_module_name,228);
   _retvars=aclfgl_get_notes_for_current_ticket(0);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(228,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 229 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,229);
   aclfgli_clr_err_flg();
   /* done print expr */
   {int _retvars;A4GL_set_status(0,0);
   A4GLSTK_setCurrentLine(_module_name,229);
   _retvars=aclfgl_first_note(0);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(229,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 230 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,230);
   aclfgli_clr_err_flg();
   /* done print expr */
   {int _retvars;A4GL_set_status(0,0);
   A4GLSTK_setCurrentLine(_module_name,230);
   _retvars=aclfgl_display_ticket(0);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(230,_module_name); }
   A4GLSTK_popFunction_nl(0,232);
   A4GL_copy_back_blobs(_blobdata,0);
   return 0;
}




A4GL_FUNCTION int aclfgl_show_ticket (int _nargs){
void *_blobdata=0;
static char *_functionName = "show_ticket";

struct BINDING _fbind[1]={ /* 0 print_param_g */
{NULL,0,0,0,0,0,NULL}
};
char *_paramnames[1]={ /* 0 */ /*print_param_g */
0
};
 void *_objData[]={
NULL};
A4GLSTK_pushFunction_v2(_functionName,_paramnames,_nargs,_module_name,233,_objData);
if (_nargs!=0) {A4GL_set_status(-3002,0);A4GL_pop_args(_nargs);A4GLSTK_popFunction_nl(0,233);return -1;}

   init_module_variables();
   /* Print nullify */
   {int _lstatus=a4gl_status;
   A4GL_pop_params_and_save_blobs(_fbind,0,&_blobdata);
   if (_lstatus!=a4gl_status) { A4GL_chk_err(233,_module_name);  }
   }

   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_FETCH_CMD Line 234 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,234);
   aclfgli_clr_err_flg();
   {
   static struct BINDING obind[21]={

   {NULL,1,0,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,25,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,0,120,0,0,0,NULL},
   {NULL,0,300,0,0,0,NULL},
   {NULL,0,15,0,0,0,NULL},
   {NULL,0,10,0,0,0,NULL},
   {NULL,0,300,0,0,0,NULL},
   {NULL,0,1,0,0,0,NULL}
   };

   obind[0].ptr= & ticket.ticket_no;

   obind[1].ptr= & ticket.status;

   obind[2].ptr= & ticket.ttype;

   obind[3].ptr= & ticket.source;

   obind[4].ptr= & ticket.scope;

   obind[5].ptr= & ticket.site;

   obind[6].ptr= & ticket.owner;

   obind[7].ptr= & ticket.date_open;

   obind[8].ptr= & ticket.time_open;

   obind[9].ptr= & ticket.date_start;

   obind[10].ptr= & ticket.time_start;

   obind[11].ptr= & ticket.date_closed;

   obind[12].ptr= & ticket.time_closed;

   obind[13].ptr= & ticket.date_end;

   obind[14].ptr= & ticket.time_end;

   obind[15].ptr= & ticket.notifications;

   obind[16].ptr= & ticket.problem;

   obind[17].ptr= & ticket.closecode;

   obind[18].ptr= & ticket.fixer;

   obind[19].ptr= & ticket.solution;

   obind[20].ptr= & ticket.notify;
   A4GL_fetch_cursor(aclfgli_str_to_id("ticket_cursor"),2,0,21,obind);
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(234,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(234,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 235 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,235);
   aclfgli_clr_err_flg();
   /* done print expr */
   {int _retvars;A4GL_set_status(0,0);
   A4GLSTK_setCurrentLine(_module_name,235);
   _retvars=aclfgl_display_ticket(0);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(235,_module_name); }
   A4GLSTK_popFunction_nl(0,237);
   A4GL_copy_back_blobs(_blobdata,0);
   return 0;
}




A4GL_FUNCTION int aclfgl_display_ticket (int _nargs){
void *_blobdata=0;
static char *_functionName = "display_ticket";

struct BINDING _fbind[1]={ /* 0 print_param_g */
{NULL,0,0,0,0,0,NULL}
};
char *_paramnames[1]={ /* 0 */ /*print_param_g */
0
};
 void *_objData[]={
NULL};
A4GLSTK_pushFunction_v2(_functionName,_paramnames,_nargs,_module_name,238,_objData);
if (_nargs!=0) {A4GL_set_status(-3002,0);A4GL_pop_args(_nargs);A4GLSTK_popFunction_nl(0,238);return -1;}

   init_module_variables();
   /* Print nullify */
   {int _lstatus=a4gl_status;
   A4GL_pop_params_and_save_blobs(_fbind,0,&_blobdata);
   if (_lstatus!=a4gl_status) { A4GL_chk_err(238,_module_name);  }
   }

   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_IF_CMD Line 239 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,239);
   aclfgli_clr_err_flg();

   A4GL_push_int(ticket.ticket_no);
   A4GL_push_long(0);
   A4GL_pushop(OP_GREATER_THAN);
   if (A4GL_pop_bool()) {
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_DISPLAY_CMD Line 240 E_MET_FUNCTION_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,240);
      aclfgli_clr_err_flg();

      A4GL_push_int(ticket.ticket_no);

      A4GL_push_variable(&ticket.status,0xa0000);

      A4GL_push_variable(&ticket.ttype,0xa0000);

      A4GL_push_variable(&ticket.source,0xa0000);

      A4GL_push_variable(&ticket.scope,0xa0000);

      A4GL_push_variable(&ticket.site,0x190000);

      A4GL_push_variable(&ticket.owner,0xa0000);

      A4GL_push_variable(&ticket.date_open,0x7);

      A4GL_push_variable(&ticket.time_open,0x50000);

      A4GL_push_variable(&ticket.date_start,0x7);

      A4GL_push_variable(&ticket.time_start,0x50000);

      A4GL_push_variable(&ticket.date_closed,0x7);

      A4GL_push_variable(&ticket.time_closed,0x50000);

      A4GL_push_variable(&ticket.date_end,0x7);

      A4GL_push_variable(&ticket.time_end,0x50000);

      A4GL_push_variable(&ticket.notifications,0x780000);

      A4GL_push_variable(&ticket.problem,0x12c0000);

      A4GL_push_variable(&ticket.closecode,0xf0000);

      A4GL_push_variable(&ticket.fixer,0xa0000);

      A4GL_push_variable(&ticket.solution,0x12c0000);

      A4GL_push_variable(&ticket.notify,0x10000);
      A4GL_disp_fields(21,0xffffffff,"ticket_no",0,"status",0,"ttype",0,"source",0,"scope",0,"site",0,"owner",0,"date_open",0,"time_open",0,"date_start",0,"time_start",0,"date_closed",0,"time_closed",0,"date_end",0,"time_end",0,"notifications",0,"problem",0,"closecode",0,"fixer",0,"solution",0,"notify",0,NULL);
         ERR_CHK_ERROR  { A4GL_chk_err(240,_module_name); }
   }
   A4GLSTK_popFunction_nl(0,243);
   A4GL_copy_back_blobs(_blobdata,0);
   return 0;
}




A4GL_FUNCTION int aclfgl_next_note (int _nargs){
void *_blobdata=0;
static char *_functionName = "next_note";

struct BINDING _fbind[1]={ /* 0 print_param_g */
{NULL,0,0,0,0,0,NULL}
};
char *_paramnames[1]={ /* 0 */ /*print_param_g */
0
};
 void *_objData[]={
NULL};
A4GLSTK_pushFunction_v2(_functionName,_paramnames,_nargs,_module_name,244,_objData);
if (_nargs!=0) {A4GL_set_status(-3002,0);A4GL_pop_args(_nargs);A4GLSTK_popFunction_nl(0,244);return -1;}

   init_module_variables();
   /* Print nullify */
   {int _lstatus=a4gl_status;
   A4GL_pop_params_and_save_blobs(_fbind,0,&_blobdata);
   if (_lstatus!=a4gl_status) { A4GL_chk_err(244,_module_name);  }
   }

   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_FETCH_CMD Line 245 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,245);
   aclfgli_clr_err_flg();
   {
   static struct BINDING obind[7]={

   {NULL,1,0,0,0,0,NULL},
   {NULL,1,0,0,0,0,NULL},
   {NULL,0,20,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,0,540,0,0,0,NULL},
   {NULL,0,1,0,0,0,NULL}
   };

   obind[0].ptr= & note.ticket_no;

   obind[1].ptr= & note.note_no;

   obind[2].ptr= & note.name;

   obind[3].ptr= & note.note_date;

   obind[4].ptr= & note.note_time;

   obind[5].ptr= & note.note;

   obind[6].ptr= & note.notify;
   A4GL_fetch_cursor(aclfgli_str_to_id("note_cursor"),2,1,7,obind);
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(245,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(245,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 246 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,246);
   aclfgli_clr_err_flg();
   /* done print expr */
   {int _retvars;A4GL_set_status(0,0);
   A4GLSTK_setCurrentLine(_module_name,246);
   _retvars=aclfgl_display_note(0);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(246,_module_name); }
   A4GLSTK_popFunction_nl(0,248);
   A4GL_copy_back_blobs(_blobdata,0);
   return 0;
}




A4GL_FUNCTION int aclfgl_prev_note (int _nargs){
void *_blobdata=0;
static char *_functionName = "prev_note";

struct BINDING _fbind[1]={ /* 0 print_param_g */
{NULL,0,0,0,0,0,NULL}
};
char *_paramnames[1]={ /* 0 */ /*print_param_g */
0
};
 void *_objData[]={
NULL};
A4GLSTK_pushFunction_v2(_functionName,_paramnames,_nargs,_module_name,249,_objData);
if (_nargs!=0) {A4GL_set_status(-3002,0);A4GL_pop_args(_nargs);A4GLSTK_popFunction_nl(0,249);return -1;}

   init_module_variables();
   /* Print nullify */
   {int _lstatus=a4gl_status;
   A4GL_pop_params_and_save_blobs(_fbind,0,&_blobdata);
   if (_lstatus!=a4gl_status) { A4GL_chk_err(249,_module_name);  }
   }

   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_FETCH_CMD Line 250 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,250);
   aclfgli_clr_err_flg();
   {
   static struct BINDING obind[7]={

   {NULL,1,0,0,0,0,NULL},
   {NULL,1,0,0,0,0,NULL},
   {NULL,0,20,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,0,540,0,0,0,NULL},
   {NULL,0,1,0,0,0,NULL}
   };

   obind[0].ptr= & note.ticket_no;

   obind[1].ptr= & note.note_no;

   obind[2].ptr= & note.name;

   obind[3].ptr= & note.note_date;

   obind[4].ptr= & note.note_time;

   obind[5].ptr= & note.note;

   obind[6].ptr= & note.notify;
   A4GL_fetch_cursor(aclfgli_str_to_id("note_cursor"),2,-1,7,obind);
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(250,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(250,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 251 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,251);
   aclfgli_clr_err_flg();
   /* done print expr */
   {int _retvars;A4GL_set_status(0,0);
   A4GLSTK_setCurrentLine(_module_name,251);
   _retvars=aclfgl_display_note(0);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(251,_module_name); }
   A4GLSTK_popFunction_nl(0,253);
   A4GL_copy_back_blobs(_blobdata,0);
   return 0;
}




A4GL_FUNCTION int aclfgl_first_note (int _nargs){
void *_blobdata=0;
static char *_functionName = "first_note";

struct BINDING _fbind[1]={ /* 0 print_param_g */
{NULL,0,0,0,0,0,NULL}
};
char *_paramnames[1]={ /* 0 */ /*print_param_g */
0
};
 void *_objData[]={
NULL};
A4GLSTK_pushFunction_v2(_functionName,_paramnames,_nargs,_module_name,254,_objData);
if (_nargs!=0) {A4GL_set_status(-3002,0);A4GL_pop_args(_nargs);A4GLSTK_popFunction_nl(0,254);return -1;}

   init_module_variables();
   /* Print nullify */
   {int _lstatus=a4gl_status;
   A4GL_pop_params_and_save_blobs(_fbind,0,&_blobdata);
   if (_lstatus!=a4gl_status) { A4GL_chk_err(254,_module_name);  }
   }

   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_FETCH_CMD Line 255 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,255);
   aclfgli_clr_err_flg();
   {
   static struct BINDING obind[7]={

   {NULL,1,0,0,0,0,NULL},
   {NULL,1,0,0,0,0,NULL},
   {NULL,0,20,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,0,540,0,0,0,NULL},
   {NULL,0,1,0,0,0,NULL}
   };

   obind[0].ptr= & note.ticket_no;

   obind[1].ptr= & note.note_no;

   obind[2].ptr= & note.name;

   obind[3].ptr= & note.note_date;

   obind[4].ptr= & note.note_time;

   obind[5].ptr= & note.note;

   obind[6].ptr= & note.notify;
   A4GL_fetch_cursor(aclfgli_str_to_id("note_cursor"),1,1,7,obind);
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(255,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(255,_module_name); }
   A4GLSTK_popFunction_nl(0,257);
   A4GL_copy_back_blobs(_blobdata,0);
   return 0;
}




A4GL_FUNCTION int aclfgl_last_note (int _nargs){
void *_blobdata=0;
static char *_functionName = "last_note";

struct BINDING _fbind[1]={ /* 0 print_param_g */
{NULL,0,0,0,0,0,NULL}
};
char *_paramnames[1]={ /* 0 */ /*print_param_g */
0
};
 void *_objData[]={
NULL};
A4GLSTK_pushFunction_v2(_functionName,_paramnames,_nargs,_module_name,258,_objData);
if (_nargs!=0) {A4GL_set_status(-3002,0);A4GL_pop_args(_nargs);A4GLSTK_popFunction_nl(0,258);return -1;}

   init_module_variables();
   /* Print nullify */
   {int _lstatus=a4gl_status;
   A4GL_pop_params_and_save_blobs(_fbind,0,&_blobdata);
   if (_lstatus!=a4gl_status) { A4GL_chk_err(258,_module_name);  }
   }

   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_FETCH_CMD Line 259 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,259);
   aclfgli_clr_err_flg();
   {
   static struct BINDING obind[7]={

   {NULL,1,0,0,0,0,NULL},
   {NULL,1,0,0,0,0,NULL},
   {NULL,0,20,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,0,540,0,0,0,NULL},
   {NULL,0,1,0,0,0,NULL}
   };

   obind[0].ptr= & note.ticket_no;

   obind[1].ptr= & note.note_no;

   obind[2].ptr= & note.name;

   obind[3].ptr= & note.note_date;

   obind[4].ptr= & note.note_time;

   obind[5].ptr= & note.note;

   obind[6].ptr= & note.notify;
   A4GL_fetch_cursor(aclfgli_str_to_id("note_cursor"),1,-1,7,obind);
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(259,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(259,_module_name); }
   A4GLSTK_popFunction_nl(0,261);
   A4GL_copy_back_blobs(_blobdata,0);
   return 0;
}




A4GL_FUNCTION int aclfgl_show_note (int _nargs){
void *_blobdata=0;
static char *_functionName = "show_note";

struct BINDING _fbind[1]={ /* 0 print_param_g */
{NULL,0,0,0,0,0,NULL}
};
char *_paramnames[1]={ /* 0 */ /*print_param_g */
0
};
 void *_objData[]={
NULL};
A4GLSTK_pushFunction_v2(_functionName,_paramnames,_nargs,_module_name,262,_objData);
if (_nargs!=0) {A4GL_set_status(-3002,0);A4GL_pop_args(_nargs);A4GLSTK_popFunction_nl(0,262);return -1;}

   init_module_variables();
   /* Print nullify */
   {int _lstatus=a4gl_status;
   A4GL_pop_params_and_save_blobs(_fbind,0,&_blobdata);
   if (_lstatus!=a4gl_status) { A4GL_chk_err(262,_module_name);  }
   }

   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_FETCH_CMD Line 263 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,263);
   aclfgli_clr_err_flg();
   {
   static struct BINDING obind[7]={

   {NULL,1,0,0,0,0,NULL},
   {NULL,1,0,0,0,0,NULL},
   {NULL,0,20,0,0,0,NULL},
   {NULL,7,0,0,0,0,NULL},
   {NULL,0,5,0,0,0,NULL},
   {NULL,0,540,0,0,0,NULL},
   {NULL,0,1,0,0,0,NULL}
   };

   obind[0].ptr= & note.ticket_no;

   obind[1].ptr= & note.note_no;

   obind[2].ptr= & note.name;

   obind[3].ptr= & note.note_date;

   obind[4].ptr= & note.note_time;

   obind[5].ptr= & note.note;

   obind[6].ptr= & note.notify;
   A4GL_fetch_cursor(aclfgli_str_to_id("note_cursor"),2,0,7,obind);
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(263,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(263,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 264 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,264);
   aclfgli_clr_err_flg();
   /* done print expr */
   {int _retvars;A4GL_set_status(0,0);
   A4GLSTK_setCurrentLine(_module_name,264);
   _retvars=aclfgl_display_note(0);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(264,_module_name); }
   A4GLSTK_popFunction_nl(0,266);
   A4GL_copy_back_blobs(_blobdata,0);
   return 0;
}




A4GL_FUNCTION int aclfgl_display_note (int _nargs){
void *_blobdata=0;
static char *_functionName = "display_note";

struct BINDING _fbind[1]={ /* 0 print_param_g */
{NULL,0,0,0,0,0,NULL}
};
char *_paramnames[1]={ /* 0 */ /*print_param_g */
0
};
 void *_objData[]={
NULL};
A4GLSTK_pushFunction_v2(_functionName,_paramnames,_nargs,_module_name,267,_objData);
if (_nargs!=0) {A4GL_set_status(-3002,0);A4GL_pop_args(_nargs);A4GLSTK_popFunction_nl(0,267);return -1;}

   init_module_variables();
   /* Print nullify */
   {int _lstatus=a4gl_status;
   A4GL_pop_params_and_save_blobs(_fbind,0,&_blobdata);
   if (_lstatus!=a4gl_status) { A4GL_chk_err(267,_module_name);  }
   }

   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_IF_CMD Line 268 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,268);
   aclfgli_clr_err_flg();

   A4GL_push_int(note.note_no);
   A4GL_push_long(0);
   A4GL_pushop(OP_GREATER_THAN);
   if (A4GL_pop_bool()) {
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_DISPLAY_CMD Line 269 E_MET_FUNCTION_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,269);
      aclfgli_clr_err_flg();

      A4GL_push_int(note.ticket_no);

      A4GL_push_int(note.note_no);

      A4GL_push_variable(&note.name,0x140000);

      A4GL_push_variable(&note.note_date,0x7);

      A4GL_push_variable(&note.note_time,0x50000);

      A4GL_push_variable(&note.note,0x21c0000);

      A4GL_push_variable(&note.notify,0x10000);
      A4GL_disp_fields(7,0xffffffff,"ticket_no",0,"note_no",0,"name",0,"note_date",0,"note_time",0,"note",0,"notify",0,NULL);
         ERR_CHK_ERROR  { A4GL_chk_err(269,_module_name); }
   }
   A4GLSTK_popFunction_nl(0,272);
   A4GL_copy_back_blobs(_blobdata,0);
   return 0;
}

A4GL_REPORT void aclfgl_email_ticket_note (int _nargs,int acl_ctrl) {
static char *_reportName="email_ticket_note";

 static void *_objData[]={
NULL};
static void *_ordbind;
static int acl_rep_ordcnt=0; // 1
static int fgl_rep_orderby=2;
static struct rep_structure _rep;
static char _rout1[256];
static char _rout2[1024];
static int _useddata=0;
static int _started=0;
static void *_blobdata=0;
static int _assigned_ordbind=0;
static struct BINDING _rbind[1]={ /*print_param_g */
{NULL,0,0,0,0,0,NULL}
};
static char *_rbindvarname[1]={ /*print_param_g */
0
};
char *_paramnames[1]={ /* 0 */ /*print_param_g */
0
};
   init_module_variables();
    if (_assigned_ordbind==0) { _assigned_ordbind=1;
   }
   A4GL_register_module_objects(_reportName, _objData);
   if (acl_ctrl==REPORT_SENDDATA&&_started==0&&fgl_rep_orderby!=1) {
       A4GL_exitwith("A report cannot accept data as it has not been started");
       return;
       }
   if (_nargs!=0&&acl_ctrl==REPORT_SENDDATA) {
   A4GL_fglerror(ERR_BADNOARGS,ABORT);A4GL_pop_args(_nargs);return ;}
   if (acl_ctrl==REPORT_LASTDATA) {
      int _p;
      if (_useddata) {for (_p=acl_rep_ordcnt;_p>=1;_p--) aclfgl_email_ticket_note(_p,REPORT_AFTERGROUP);}
   }
   if (acl_ctrl==REPORT_SENDDATA) {
      int _g=0;int _p;
      if (_g>0&&_useddata) {for (_p=acl_rep_ordcnt;_p>=_g;_p--) aclfgl_email_ticket_note(_p,REPORT_AFTERGROUP);}
      A4GL_pop_params_and_save_blobs(_rbind,0,&_blobdata);
                  aclfgl_email_ticket_note(0,REPORT_AFTERDATA);
      if (_useddata==0) {_g=1;}
      if (_g>0) {
       A4GL_rep_print(&_rep,0,1,0,-1);
           _useddata=1;
           for (_p=_g;_p<=acl_rep_ordcnt;_p++)
                  aclfgl_email_ticket_note(_p,REPORT_BEFOREGROUP);
      }
      _useddata=1;
   goto report1_ctrl; /* G1 */
   }
   if (acl_ctrl==REPORT_FINISH) {
   }

   if (acl_ctrl==REPORT_CONVERT) {
   char *_f; char *_o; char *_l; int _to_pipe; _l=A4GL_char_pop(); _o=A4GL_char_pop(); _f=A4GL_char_pop(); _to_pipe=A4GL_pop_int();
   A4GL_convert_report(&_rep,_f,_o,_l,_to_pipe);
   A4GL_copy_back_blobs(_blobdata,0);
   return ;
   }
   if (acl_ctrl==REPORT_FREE) {
   A4GL_free_report(&_rep);
   A4GL_copy_back_blobs(_blobdata,0);
   return ;
   }
   if (acl_ctrl==REPORT_START||acl_ctrl==REPORT_RESTART) {
      A4GL_pop_char(_rout2,1023);A4GL_trim(_rout2);
      A4GL_pop_char(_rout1,254);A4GL_trim(_rout1);
      _useddata=0;
      _started=1;
   goto output_1;
   }
   goto report1_ctrl; /* G1 */
   output_1:
   _rep.lines_in_header=-1;
   _rep.lines_in_trailer=-1;
   _rep.lines_in_first_header=-1;
   _rep.print_section=0;
   _rep.rb_stack[0]=0;
   _rep.rb_stack_level=0;
   {
   long _pl; int _lm; int _rm; int _tm; int _bm;
   A4GL_push_long(999);
   _pl=A4GL_pop_long();
   A4GL_push_long(0);
   _lm=A4GL_pop_long();
   A4GL_push_long(132);
   _rm=A4GL_pop_long();
   A4GL_push_long(0);
   _tm=A4GL_pop_long();
   A4GL_push_long(3);
   _bm=A4GL_pop_long();
   _rep.top_margin= A4GL_set_report_dim_int("TOP MARGIN",_tm);
   _rep.bottom_margin= A4GL_set_report_dim_int("BOTTOM MARGIN",_bm);
   _rep.left_margin= A4GL_set_report_dim_int("LEFT MARGIN",_lm);
   _rep.right_margin= A4GL_set_report_dim_int("RIGHT MARGIN",_rm);
   _rep.page_length= A4GL_set_report_dim_int("PAGE LENGTH",_pl);
   strcpy(_rep.top_of_page, "");
   _rep.header=0;
   _rep.finishing=0;
   _rep.nblocks=0;
   _rep.blocks=0;
   _rep.repName=_reportName;
   _rep.modName=_module_name;
   _rep.page_no=0;
   _rep.printed_page_no=0;
   _rep.line_no=0;
   _rep.col_no=0;
   }
   if (strlen(_rout2)==0) {
   strcpy(_rep.output_loc_str,"stdout");
   } else {
        strcpy(_rep.output_loc_str,_rout2);
   }
   if (strlen(_rout1)==0){
   _rep.output_mode='F';
   } else {
   _rep.output_mode=_rout1[0];
   }
   _rep.report=(void *)&aclfgl_email_ticket_note;
   A4GL_trim(_rep.output_loc_str);
   A4GL_rep_print(&_rep,-1,-1,-1,-1);
   goto report1_ctrl; /* G1 */

   rep_ctrl1_0:
      A4GL_push_report_section(&_rep,_module_name,_reportName,281,'p',"HEADER",0);
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_PRINT_CMD Line 282 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,282);
      aclfgli_clr_err_flg();
      A4GL_push_char("From: Phillip Heller <pheller@gmail.com>");
      A4GL_rep_print(&_rep,1,1,0,0);
      A4GL_rep_print(&_rep,0,0,0,-1);
         ERR_CHK_ERROR  { A4GL_chk_err(282,_module_name); }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_PRINT_CMD Line 283 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,283);
      aclfgli_clr_err_flg();
      A4GL_push_char("To: Phillip Heller <pheller@me.com>");
      A4GL_rep_print(&_rep,1,1,0,1);
      A4GL_rep_print(&_rep,0,0,0,-1);
         ERR_CHK_ERROR  { A4GL_chk_err(283,_module_name); }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_PRINT_CMD Line 284 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,284);
      aclfgli_clr_err_flg();
      A4GL_push_char("Subject: Ticket #");
      A4GL_rep_print(&_rep,1,1,0,2);

      A4GL_push_int(ticket.ticket_no);
      A4GL_rep_print(&_rep,1,1,0,3);
      A4GL_push_char(" - Note #");
      A4GL_rep_print(&_rep,1,1,0,4);

      A4GL_push_int(note.note_no);
      A4GL_rep_print(&_rep,1,1,0,5);
      A4GL_rep_print(&_rep,0,0,0,-1);
         ERR_CHK_ERROR  { A4GL_chk_err(284,_module_name); }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_PRINT_CMD Line 285 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,285);
      aclfgli_clr_err_flg();
      A4GL_push_empty_char();
      A4GL_rep_print(&_rep,1,1,0,6);
      A4GL_rep_print(&_rep,0,0,0,-1);
         ERR_CHK_ERROR  { A4GL_chk_err(285,_module_name); }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_PRINT_CMD Line 286 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,286);
      aclfgli_clr_err_flg();
      A4GL_push_char("Note:");
      A4GL_rep_print(&_rep,1,1,0,7);
      A4GL_rep_print(&_rep,0,0,0,-1);
         ERR_CHK_ERROR  { A4GL_chk_err(286,_module_name); }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_PRINT_CMD Line 287 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,287);
      aclfgli_clr_err_flg();

      A4GL_push_variable(&note.note,0x21c0000);
      A4GL_rep_print(&_rep,1,1,0,8);
      A4GL_rep_print(&_rep,0,0,0,-1);
         ERR_CHK_ERROR  { A4GL_chk_err(287,_module_name); }
      A4GL_pop_report_section(&_rep,0);
      goto report1_ctrl; /* G1 */
   report1_ctrl:
   if (_rep.lines_in_header      ==-1) _rep.lines_in_header=6;
   if (_rep.lines_in_first_header==-1) _rep.lines_in_first_header=0;
   if (_rep.lines_in_trailer     ==-1) _rep.lines_in_trailer=0;
   if (acl_rep_ordcnt==-1) {
   acl_rep_ordcnt=0;
   }
       if (acl_ctrl==REPORT_OPS_COMPLETE) return;
       if (acl_ctrl==REPORT_SENDDATA) {
      /* check for after group of */
          aclfgl_email_ticket_note(0,REPORT_DATA);
      /* check for before group of */
       }
   if (acl_ctrl==REPORT_FINISH && _started==0) {
       A4GL_exitwith("You cannot FINISH REPORT - because the report has not been started");
       return;
       }
   if (acl_ctrl==REPORT_FINISH && _started) {aclfgl_email_ticket_note(0,REPORT_LASTDATA);return;}
   if (acl_ctrl==REPORT_LASTDATA) {
     if (_useddata || A4GL_always_output_report(&_rep)) {
      /*if (_useddata)*/ {aclfgl_email_ticket_note(0,REPORT_LASTROW);}
      if (_rep.page_no<=1&&_rep.page_length>1 &&_rep.header ) {A4GL_rep_print(&_rep,0,1,0,-1);A4GL_rep_print(&_rep,0,0,0,-1);}
      _rep.finishing=1;
      A4GL_skip_top_of_page(&_rep,999);
   }
     _started=0;
     if (_rep.output) {A4GL_close_report_file(&_rep);}
     return;
   }
   if (acl_ctrl==REPORT_TERMINATE) {_started=0;if (_rep.output) {A4GL_close_report_file(&_rep);}return;}
       if (acl_ctrl==REPORT_AFTERDATA ) {
       return;
       }
    if (acl_ctrl==REPORT_START||acl_ctrl==REPORT_RESTART) {
   }
   if (acl_ctrl==REPORT_PAGEHEADER&&(_rep.page_no!=1||(_rep.page_no==1&&_rep.has_first_page==0))) {acl_ctrl=0;goto rep_ctrl1_0;}
   if (acl_ctrl==REPORT_DATA) {acl_ctrl=REPORT_NOTHING;
   goto report1_ctrl; /* G1 */
   }
A4GL_copy_back_blobs(_blobdata,0);
} /* end of report */
/* END OF MODULE */
