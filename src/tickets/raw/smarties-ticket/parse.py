#!/usr/bin/env python
# vim: set ts=4 sw=4 et:

import textfsm

template = textfsm.TextFSM(open("ticket_note_template"))

inputfile = open("smarties_notes.txt", "r")
notes = template.ParseText(inputfile.read())
    
for note in notes:
    noteno = int(note[0].strip())
    text = "".join(note[1])
    date = note[2].strip()
    time = note[3].strip()
    author = note[4].strip()

    print """
INSERT INTO ticket_notes (ticket_no, note_no, name, note_date, note_time, note) values (
    2678, %d, '%s', '%s', '%s', E'%s');
    """ % (noteno, author, date, time, text.replace("'", "\\'"))

