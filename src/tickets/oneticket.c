#include "a4gl_incl_4glhdr.h"
extern int a4gl_lib_status;
static char *_module_name="oneticket.4gl";


A4GL_MAIN int main(int argc,char *argv[]);
A4GL_REPORT void aclfgl_rep_oneticket (int n,int a);
#define fgldate long
static char const _rcsid[]="$FGLIdent: oneticket.4gl Compiler-1.240  Log:Not Set $";
static void a4gl_show_compiled_version(void) {
printf("Log: Not Set\n");
printf("Aubit4GL Version: 1.2.40\n");
exit(0);
}

static const char *_CompileTimeSQLType="pg8";
static const struct sDependantTable _dependantTables[]= {
  {0,0}
};

extern struct {
   long sqlcode ;
   char sqlerrm [72+1];
   char sqlerrp [8+1];
   long sqlerrd[6] ;
   char sqlawarn [8+1];
   char sqlstate [9+1];
} a4gl_sqlca;
extern long a4gl_status ;
extern long aiplib_status ;
extern long curr_form ;
extern long curr_hwnd ;
extern char err_file_name [32+1];
extern long err_line_no ;
extern long err_status ;
extern long int_flag ;
extern long quit_flag ;
static char mv_privateinfo [1+1];
static short mv_ticketno ;
static char mv_version [120+1];

static int _done_init_module_variables=1;
A4GL_INTERNAL_FUNCTION static void init_module_variables(void) {
 static void *_objData[]={
NULL};
   if (_done_init_module_variables==0) return;
   _done_init_module_variables=0;
   A4GL_register_module_objects(_module_name, _objData);
   A4GL_check_version(_module_name,"1.2",40);
   A4GL_check_dependant_tables(_module_name, _CompileTimeSQLType, _dependantTables);
   /* Print nullify */
   A4GL_setnull(0,&mv_privateinfo,0x1); /*1 */
   mv_ticketno=0;
   A4GL_setnull(0,&mv_version,0x78); /*1 */
   // Initialise the current global variables
   // No global variables in use
   // Initialise any other global variables
}




A4GL_MAIN int main(int argc,char *argv[]) {
char *_paramnames[1]={""};
void *_blobdata=0;
int _nargs=0;
static char *_functionName = "MAIN";
   struct {
      short tticket ;
      fglvarchar status [14+1];
      fglvarchar notifications [124+1];
      fglvarchar owner [14+1];
      fglvarchar ttype [14+1];
      fglvarchar scope [14+1];
      fgldate date_open ;
      fglvarchar time_open [9+1];
      fgldate date_closed ;
      fglvarchar time_closed [9+1];
      fglvarchar problem [304+1];
      fglvarchar solution [304+1];
      fglvarchar fixer [14+1];
      fglvarchar site [29+1];
      fglvarchar source [14+1];
      fgldate date_start ;
      fglvarchar time_start [9+1];
      fgldate date_end ;
      fglvarchar time_end [9+1];
      fglvarchar closecode [19+1];
      short nticket ;
      short note_no ;
      fglvarchar name [24+1];
      fgldate note_date ;
      fglvarchar note_time [9+1];
      fglvarchar note [544+1];
   } lr_data;
   long lv_cnt ;
   long lv_rid ;
    void *_objData[]={
   NULL};
   A4GLSTK_setCurrentLine(0,0);
   if (A4GL_fgl_start(argc,argv)) {a4gl_show_compiled_version();}
   aclfgli_clr_err_flg();
   {char *_s; char *_sqltype=NULL;
   A4GL_push_char("nearnet");
   _s=A4GL_char_pop();A4GL_trim(_s);
   A4GL_init_connection_with_sqltype(_s, _sqltype);
   free(_s);
   if (_sqltype) free(_sqltype);
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(8,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(8,_module_name); }
   if (a4gl_sqlca.sqlcode<0) A4GL_chk_err(0,_module_name);

   init_module_variables();
   /* Print nullify */
   lr_data.tticket=0;
   A4GL_setnull(13,&lr_data.status,0xe); /*1 */
   A4GL_setnull(13,&lr_data.notifications,0x7c); /*1 */
   A4GL_setnull(13,&lr_data.owner,0xe); /*1 */
   A4GL_setnull(13,&lr_data.ttype,0xe); /*1 */
   A4GL_setnull(13,&lr_data.scope,0xe); /*1 */
   A4GL_setnull(7,&lr_data.date_open,0x0); /*1 */
   A4GL_setnull(13,&lr_data.time_open,0x9); /*1 */
   A4GL_setnull(7,&lr_data.date_closed,0x0); /*1 */
   A4GL_setnull(13,&lr_data.time_closed,0x9); /*1 */
   A4GL_setnull(13,&lr_data.problem,0x130); /*1 */
   A4GL_setnull(13,&lr_data.solution,0x130); /*1 */
   A4GL_setnull(13,&lr_data.fixer,0xe); /*1 */
   A4GL_setnull(13,&lr_data.site,0x1d); /*1 */
   A4GL_setnull(13,&lr_data.source,0xe); /*1 */
   A4GL_setnull(7,&lr_data.date_start,0x0); /*1 */
   A4GL_setnull(13,&lr_data.time_start,0x9); /*1 */
   A4GL_setnull(7,&lr_data.date_end,0x0); /*1 */
   A4GL_setnull(13,&lr_data.time_end,0x9); /*1 */
   A4GL_setnull(13,&lr_data.closecode,0x13); /*1 */
   lr_data.nticket=0;
   lr_data.note_no=0;
   A4GL_setnull(13,&lr_data.name,0x18); /*1 */
   A4GL_setnull(7,&lr_data.note_date,0x0); /*1 */
   A4GL_setnull(13,&lr_data.note_time,0x9); /*1 */
   A4GL_setnull(13,&lr_data.note,0x220); /*1 */
   lv_cnt=0;
   lv_rid=0;
   A4GLSTK_initFunctionCallStack();
   A4GLSTK_pushFunction_v2(_functionName,_paramnames,_nargs,_module_name,8,_objData);

   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 41 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,41);
   aclfgli_clr_err_flg();

   lv_cnt=0;
      ERR_CHK_ERROR  { A4GL_chk_err(41,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 42 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,42);
   aclfgli_clr_err_flg();
   A4GL_push_long(1);
   { // FCALL 1
   int _retvars;
   _retvars=aclfgl_arg_val(1);
   {
   if (_retvars!= 1 && a4gl_status==0 ) {
   A4GL_set_status(-3001,0);
   A4GL_chk_err(42,_module_name);
   }
   }
   } // FCALL 2

   A4GL_pop_var2(&mv_ticketno,1,0);
      ERR_CHK_ERROR  { A4GL_chk_err(42,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_LET_CMD Line 43 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,43);
   aclfgli_clr_err_flg();
   A4GL_push_long(2);
   { // FCALL 1
   int _retvars;
   _retvars=aclfgl_arg_val(1);
   {
   if (_retvars!= 1 && a4gl_status==0 ) {
   A4GL_set_status(-3001,0);
   A4GL_chk_err(43,_module_name);
   }
   }
   } // FCALL 2

   A4GL_pop_var2(&mv_privateinfo,0,1);
      ERR_CHK_ERROR  { A4GL_chk_err(43,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_DECLARE_CMD Line 46 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,46);
   aclfgli_clr_err_flg();
   {
   char *_sql;
   static struct BINDING ibind[1]={

   {NULL,1,0,0,0,0,NULL}
   };

   ibind[0].ptr=&mv_ticketno;
   _sql="SELECT tickets.ticket_no tticket,   status,   notifications,   owner,   ttype,   scope,   date_open,   time_open,   date_closed,   time_closed,   problem,   solution,   fixer,   site,   source,   date_start,   time_start,   date_end,   time_end,   closecode,   ticket_notes.ticket_no nticket,   note_no,   name,   note_date,   note_time,   note  FROM tickets,ticket_notes WHERE (tickets.ticket_no=ticket_notes.ticket_no AND tickets.ticket_no=?) ORDER BY tticket ASC,note_no ASC";
   A4GL_declare_cursor(0,A4GL_prepare_select(ibind,1,NULL,0,_sql,_module_name,46,0,0),0,A4GL_get_ident("oneticket","c_oneticket","a000c_onetick04577"));
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(46,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(46,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_START_CMD Line 49 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,49);
   aclfgli_clr_err_flg();
   A4GL_push_char("");
   A4GL_push_char("");
   {int _pl; int _rm; int _lm; int _tm; int _bm;
   A4GL_push_long(-1);
   _pl=A4GL_pop_long();
   A4GL_push_long(-1);
   _lm=A4GL_pop_long();
   A4GL_push_long(-1);
   _rm=A4GL_pop_long();
   A4GL_push_long(-1);
   _tm=A4GL_pop_long();
   A4GL_push_long(-1);
   _bm=A4GL_pop_long();
   A4GL_set_report_dim(_pl,_lm,_rm,_tm,_bm,"");
   aclfgl_rep_oneticket(2,REPORT_START);
      ERR_CHK_SQLERROR  { A4GL_chk_err(49,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(49,_module_name); }
   }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_FOREACH_CMD Line 51 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,51);
   {
      int _cursoropen=0;
      int _fetcherr=0;
      int _fetchstatus=0;
      A4GLSQL_set_sqlca_sqlcode(0);
      aclfgli_clr_err_flg();
      A4GL_open_cursor(A4GL_get_ident("oneticket","c_oneticket","a000c_onetick04577"),0,0);
         ERR_CHK_SQLERROR  { A4GL_chk_err(51,_module_name); }
         ERR_CHK_ERROR  { A4GL_chk_err(51,_module_name); }
      if (a4gl_sqlca.sqlcode!=0) {
      goto END_BLOCK_1;
      }
      _cursoropen=1;
      while (1) {
      int _dobreak=0;
         static struct BINDING obind[26]={

         {NULL,1,0,0,0,0,NULL},
         {NULL,13,14,0,0,0,NULL},
         {NULL,13,124,0,0,0,NULL},
         {NULL,13,14,0,0,0,NULL},
         {NULL,13,14,0,0,0,NULL},
         {NULL,13,14,0,0,0,NULL},
         {NULL,7,0,0,0,0,NULL},
         {NULL,13,9,0,0,0,NULL},
         {NULL,7,0,0,0,0,NULL},
         {NULL,13,9,0,0,0,NULL},
         {NULL,13,304,0,0,0,NULL},
         {NULL,13,304,0,0,0,NULL},
         {NULL,13,14,0,0,0,NULL},
         {NULL,13,29,0,0,0,NULL},
         {NULL,13,14,0,0,0,NULL},
         {NULL,7,0,0,0,0,NULL},
         {NULL,13,9,0,0,0,NULL},
         {NULL,7,0,0,0,0,NULL},
         {NULL,13,9,0,0,0,NULL},
         {NULL,13,19,0,0,0,NULL},
         {NULL,1,0,0,0,0,NULL},
         {NULL,1,0,0,0,0,NULL},
         {NULL,13,24,0,0,0,NULL},
         {NULL,7,0,0,0,0,NULL},
         {NULL,13,9,0,0,0,NULL},
         {NULL,13,544,0,0,0,NULL}
         };

         obind[0].ptr= & lr_data.tticket;

         obind[1].ptr= & lr_data.status;

         obind[2].ptr= & lr_data.notifications;

         obind[3].ptr= & lr_data.owner;

         obind[4].ptr= & lr_data.ttype;

         obind[5].ptr= & lr_data.scope;

         obind[6].ptr= & lr_data.date_open;

         obind[7].ptr= & lr_data.time_open;

         obind[8].ptr= & lr_data.date_closed;

         obind[9].ptr= & lr_data.time_closed;

         obind[10].ptr= & lr_data.problem;

         obind[11].ptr= & lr_data.solution;

         obind[12].ptr= & lr_data.fixer;

         obind[13].ptr= & lr_data.site;

         obind[14].ptr= & lr_data.source;

         obind[15].ptr= & lr_data.date_start;

         obind[16].ptr= & lr_data.time_start;

         obind[17].ptr= & lr_data.date_end;

         obind[18].ptr= & lr_data.time_end;

         obind[19].ptr= & lr_data.closecode;

         obind[20].ptr= & lr_data.nticket;

         obind[21].ptr= & lr_data.note_no;

         obind[22].ptr= & lr_data.name;

         obind[23].ptr= & lr_data.note_date;

         obind[24].ptr= & lr_data.note_time;

         obind[25].ptr= & lr_data.note;
         A4GL_fetch_cursor(A4GL_get_ident("oneticket","c_oneticket","a000c_onetick04577"),2,1,26,obind); /* Foreach next */
         if (a4gl_sqlca.sqlcode<0||a4gl_sqlca.sqlcode==100) {_dobreak++;}
         if (_dobreak) {
         break;
         }
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_OUTPUT_CMD Line 52 E_MET_MAIN_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,52);
         aclfgli_clr_err_flg();

         A4GL_push_int(lr_data.tticket);

         A4GL_push_variable(&lr_data.status,0xe000d);

         A4GL_push_variable(&lr_data.notifications,0x7c000d);

         A4GL_push_variable(&lr_data.owner,0xe000d);

         A4GL_push_variable(&lr_data.ttype,0xe000d);

         A4GL_push_variable(&lr_data.scope,0xe000d);

         A4GL_push_variable(&lr_data.date_open,0x7);

         A4GL_push_variable(&lr_data.time_open,0x9000d);

         A4GL_push_variable(&lr_data.date_closed,0x7);

         A4GL_push_variable(&lr_data.time_closed,0x9000d);

         A4GL_push_variable(&lr_data.problem,0x130000d);

         A4GL_push_variable(&lr_data.solution,0x130000d);

         A4GL_push_variable(&lr_data.fixer,0xe000d);

         A4GL_push_variable(&lr_data.site,0x1d000d);

         A4GL_push_variable(&lr_data.source,0xe000d);

         A4GL_push_variable(&lr_data.date_start,0x7);

         A4GL_push_variable(&lr_data.time_start,0x9000d);

         A4GL_push_variable(&lr_data.date_end,0x7);

         A4GL_push_variable(&lr_data.time_end,0x9000d);

         A4GL_push_variable(&lr_data.closecode,0x13000d);

         A4GL_push_int(lr_data.nticket);

         A4GL_push_int(lr_data.note_no);

         A4GL_push_variable(&lr_data.name,0x18000d);

         A4GL_push_variable(&lr_data.note_date,0x7);

         A4GL_push_variable(&lr_data.note_time,0x9000d);

         A4GL_push_variable(&lr_data.note,0x220000d);
         aclfgl_rep_oneticket(26,REPORT_SENDDATA);
         if (aclfgli_get_err_flg()) {
            ERR_CHK_SQLERROR  { A4GL_chk_err(52,_module_name); }
            ERR_CHK_ERROR  { A4GL_chk_err(52,_module_name); }
         } else {
            ERR_CHK_ERROR  { A4GL_chk_err(52,_module_name); }
         }
         CONTINUE_BLOCK_1:;
      }
      END_BLOCK_1:;
      if (_cursoropen) {
         A4GL_close_cursor(A4GL_get_ident("oneticket","c_oneticket","a000c_onetick04577"),1);
         if (a4gl_status == 0) { if (_fetcherr) {A4GL_set_status(_fetcherr,1);}}
         if (a4gl_status == 100) { if (_fetcherr) {a4gl_sqlca.sqlcode = a4gl_status=_fetcherr;} else {a4gl_sqlca.sqlcode = a4gl_status = 0; }}
      }
   }
      ERR_CHK_SQLERROR  { A4GL_chk_err(51,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(51,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_FINISH_CMD Line 55 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,55);
   aclfgli_clr_err_flg();
   aclfgl_rep_oneticket(0,REPORT_FINISH);
   if (aclfgli_get_err_flg()) {
      ERR_CHK_SQLERROR  { A4GL_chk_err(55,_module_name); }
      ERR_CHK_ERROR  { A4GL_chk_err(55,_module_name); }
   } else {
      ERR_CHK_ERROR  { A4GL_chk_err(55,_module_name); }
   }
   A4GLSTK_popFunction_nl(0, 58);
   A4GL_fgl_end_4gl_0();
   return 0;
}
A4GL_REPORT void aclfgl_rep_oneticket (int _nargs,int acl_ctrl) {
static char *_reportName="rep_oneticket";
static struct {
   short tticket ;
   fglvarchar status [14+1];
   fglvarchar notifications [124+1];
   fglvarchar owner [14+1];
   fglvarchar ttype [14+1];
   fglvarchar scope [14+1];
   fgldate date_open ;
   fglvarchar time_open [9+1];
   fgldate date_closed ;
   fglvarchar time_closed [9+1];
   fglvarchar problem [304+1];
   fglvarchar solution [304+1];
   fglvarchar fixer [14+1];
   fglvarchar site [29+1];
   fglvarchar source [14+1];
   fgldate date_start ;
   fglvarchar time_start [9+1];
   fgldate date_end ;
   fglvarchar time_end [9+1];
   fglvarchar closecode [19+1];
   short nticket ;
   short note_no ;
   fglvarchar name [24+1];
   fgldate note_date ;
   fglvarchar note_time [9+1];
   fglvarchar note [544+1];
} lr_data;

 static void *_objData[]={
NULL};
static struct BINDING _ordbind[2]={
{NULL,1,0,0,0,0,NULL},
{NULL,1,0,0,0,0,NULL}
};
static int acl_rep_ordcnt=2; // 2
static int fgl_rep_orderby=2;
static struct rep_structure _rep;
static char _rout1[256];
static char _rout2[1024];
static int _useddata=0;
static int _started=0;
static void *_blobdata=0;
static int _assigned_ordbind=0;
static struct BINDING _rbind[26]={ /*print_param_g */
{NULL,1,0,0,0,0,NULL}
,
{NULL,13,14,0,0,0,NULL}
,
{NULL,13,124,0,0,0,NULL}
,
{NULL,13,14,0,0,0,NULL}
,
{NULL,13,14,0,0,0,NULL}
,
{NULL,13,14,0,0,0,NULL}
,
{NULL,7,0,0,0,0,NULL}
,
{NULL,13,9,0,0,0,NULL}
,
{NULL,7,0,0,0,0,NULL}
,
{NULL,13,9,0,0,0,NULL}
,
{NULL,13,304,0,0,0,NULL}
,
{NULL,13,304,0,0,0,NULL}
,
{NULL,13,14,0,0,0,NULL}
,
{NULL,13,29,0,0,0,NULL}
,
{NULL,13,14,0,0,0,NULL}
,
{NULL,7,0,0,0,0,NULL}
,
{NULL,13,9,0,0,0,NULL}
,
{NULL,7,0,0,0,0,NULL}
,
{NULL,13,9,0,0,0,NULL}
,
{NULL,13,19,0,0,0,NULL}
,
{NULL,1,0,0,0,0,NULL}
,
{NULL,1,0,0,0,0,NULL}
,
{NULL,13,24,0,0,0,NULL}
,
{NULL,7,0,0,0,0,NULL}
,
{NULL,13,9,0,0,0,NULL}
,
{NULL,13,544,0,0,0,NULL}
};
static char *_rbindvarname[26]={ /*print_param_g */
"tticket"
,
"status"
,
"notifications"
,
"owner"
,
"ttype"
,
"scope"
,
"date_open"
,
"time_open"
,
"date_closed"
,
"time_closed"
,
"problem"
,
"solution"
,
"fixer"
,
"site"
,
"source"
,
"date_start"
,
"time_start"
,
"date_end"
,
"time_end"
,
"closecode"
,
"nticket"
,
"note_no"
,
"name"
,
"note_date"
,
"note_time"
,
"note"
};
char *_paramnames[27]={ /* 26 */ /*print_param_g */
  "lr_data.tticket",
  "lr_data.status",
  "lr_data.notifications",
  "lr_data.owner",
  "lr_data.ttype",
  "lr_data.scope",
  "lr_data.date_open",
  "lr_data.time_open",
  "lr_data.date_closed",
  "lr_data.time_closed",
  "lr_data.problem",
  "lr_data.solution",
  "lr_data.fixer",
  "lr_data.site",
  "lr_data.source",
  "lr_data.date_start",
  "lr_data.time_start",
  "lr_data.date_end",
  "lr_data.time_end",
  "lr_data.closecode",
  "lr_data.nticket",
  "lr_data.note_no",
  "lr_data.name",
  "lr_data.note_date",
  "lr_data.note_time",
  "lr_data.note",
0
};

_rbind[0].ptr= (&lr_data.tticket);

_rbind[1].ptr= (&lr_data.status);

_rbind[2].ptr= (&lr_data.notifications);

_rbind[3].ptr= (&lr_data.owner);

_rbind[4].ptr= (&lr_data.ttype);

_rbind[5].ptr= (&lr_data.scope);

_rbind[6].ptr= (&lr_data.date_open);

_rbind[7].ptr= (&lr_data.time_open);

_rbind[8].ptr= (&lr_data.date_closed);

_rbind[9].ptr= (&lr_data.time_closed);

_rbind[10].ptr= (&lr_data.problem);

_rbind[11].ptr= (&lr_data.solution);

_rbind[12].ptr= (&lr_data.fixer);

_rbind[13].ptr= (&lr_data.site);

_rbind[14].ptr= (&lr_data.source);

_rbind[15].ptr= (&lr_data.date_start);

_rbind[16].ptr= (&lr_data.time_start);

_rbind[17].ptr= (&lr_data.date_end);

_rbind[18].ptr= (&lr_data.time_end);

_rbind[19].ptr= (&lr_data.closecode);

_rbind[20].ptr= (&lr_data.nticket);

_rbind[21].ptr= (&lr_data.note_no);

_rbind[22].ptr= (&lr_data.name);

_rbind[23].ptr= (&lr_data.note_date);

_rbind[24].ptr= (&lr_data.note_time);

_rbind[25].ptr= (&lr_data.note);
   init_module_variables();
    if (_assigned_ordbind==0) { _assigned_ordbind=1;

   _ordbind[0].ptr=&lr_data.tticket;

   _ordbind[1].ptr=&lr_data.note_no;
   }
   A4GL_register_module_objects(_reportName, _objData);
   if (acl_ctrl==REPORT_SENDDATA&&_started==0&&fgl_rep_orderby!=1) {
       A4GL_exitwith("A report cannot accept data as it has not been started");
       return;
       }
   if (_nargs!=26&&acl_ctrl==REPORT_SENDDATA) {
   A4GL_fglerror(ERR_BADNOARGS,ABORT);A4GL_pop_args(_nargs);return ;}
   if (acl_ctrl==REPORT_LASTDATA) {
      int _p;
      if (_useddata) {for (_p=acl_rep_ordcnt;_p>=1;_p--) aclfgl_rep_oneticket(_p,REPORT_AFTERGROUP);}
   }
   if (acl_ctrl==REPORT_SENDDATA) {
      int _g=0;int _p;
      _g=A4GL_chk_params(_rbind,26,_ordbind,acl_rep_ordcnt);
      if (_g>0&&_useddata) {for (_p=acl_rep_ordcnt;_p>=_g;_p--) aclfgl_rep_oneticket(_p,REPORT_AFTERGROUP);}
      A4GL_pop_params_and_save_blobs(_rbind,26,&_blobdata);
                  aclfgl_rep_oneticket(0,REPORT_AFTERDATA);
      if (_useddata==0) {_g=1;}
      if (_g>0) {
       A4GL_rep_print(&_rep,0,1,0,-1);
           _useddata=1;
           for (_p=_g;_p<=acl_rep_ordcnt;_p++)
                  aclfgl_rep_oneticket(_p,REPORT_BEFOREGROUP);
      }
      _useddata=1;
   goto report1_ctrl; /* G1 */
   }
   if (acl_ctrl==REPORT_FINISH) {
   }

   if (acl_ctrl==REPORT_CONVERT) {
   char *_f; char *_o; char *_l; int _to_pipe; _l=A4GL_char_pop(); _o=A4GL_char_pop(); _f=A4GL_char_pop(); _to_pipe=A4GL_pop_int();
   A4GL_convert_report(&_rep,_f,_o,_l,_to_pipe);
   A4GL_copy_back_blobs(_blobdata,0);
   return ;
   }
   if (acl_ctrl==REPORT_FREE) {
   A4GL_free_report(&_rep);
   A4GL_copy_back_blobs(_blobdata,0);
   return ;
   }
   if (acl_ctrl==REPORT_START||acl_ctrl==REPORT_RESTART) {
      A4GL_pop_char(_rout2,1023);A4GL_trim(_rout2);
      A4GL_pop_char(_rout1,254);A4GL_trim(_rout1);
      _useddata=0;
      _started=1;
   goto output_1;
   }
   goto report1_ctrl; /* G1 */
   output_1:
   _rep.lines_in_header=-1;
   _rep.lines_in_trailer=-1;
   _rep.lines_in_first_header=-1;
   _rep.print_section=0;
   _rep.rb_stack[0]=0;
   _rep.rb_stack_level=0;
   {
   long _pl; int _lm; int _rm; int _tm; int _bm;
   A4GL_push_long(1);
   _pl=A4GL_pop_long();
   A4GL_push_long(0);
   _lm=A4GL_pop_long();
   A4GL_push_long(132);
   _rm=A4GL_pop_long();
   A4GL_push_long(0);
   _tm=A4GL_pop_long();
   A4GL_push_long(0);
   _bm=A4GL_pop_long();
   _rep.top_margin= A4GL_set_report_dim_int("TOP MARGIN",_tm);
   _rep.bottom_margin= A4GL_set_report_dim_int("BOTTOM MARGIN",_bm);
   _rep.left_margin= A4GL_set_report_dim_int("LEFT MARGIN",_lm);
   _rep.right_margin= A4GL_set_report_dim_int("RIGHT MARGIN",_rm);
   _rep.page_length= A4GL_set_report_dim_int("PAGE LENGTH",_pl);
   strcpy(_rep.top_of_page, "");
   _rep.header=0;
   _rep.finishing=0;
   _rep.nblocks=0;
   _rep.blocks=0;
   _rep.repName=_reportName;
   _rep.modName=_module_name;
   _rep.page_no=0;
   _rep.printed_page_no=0;
   _rep.line_no=0;
   _rep.col_no=0;
   }
   if (strlen(_rout2)==0) {
   strcpy(_rep.output_loc_str,"stdout");
   } else {
        strcpy(_rep.output_loc_str,_rout2);
   }
   if (strlen(_rout1)==0){
   _rep.output_mode='F';
   } else {
   _rep.output_mode=_rout1[0];
   }
   _rep.report=(void *)&aclfgl_rep_oneticket;
   A4GL_trim(_rep.output_loc_str);
   A4GL_rep_print(&_rep,-1,-1,-1,-1);
   goto report1_ctrl; /* G1 */

   rep_ctrl1_0:
      A4GL_push_report_section(&_rep,_module_name,_reportName,103,'P',"FIRST",0);
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_LET_CMD Line 104 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,104);
      aclfgli_clr_err_flg();
      A4GL_push_char("$Header: openticket.ace,v 1.4 90/05/14 22:17:30 long Exp $");

      A4GL_pop_var2(&mv_version,0,120);
         ERR_CHK_ERROR  { A4GL_chk_err(104,_module_name); }
      A4GL_pop_report_section(&_rep,0);
      goto report1_ctrl; /* G1 */

   rep_ctrl1_1:
      A4GL_push_report_section(&_rep,_module_name,_reportName,107,'B',"2",1);
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_PRINT_CMD Line 107 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,107);
      aclfgli_clr_err_flg();
      A4GL_push_char("Ticket Number:");
      A4GL_rep_print(&_rep,1,1,0,0);

      A4GL_push_int(lr_data.tticket);
      A4GL_push_char(" ####");
      A4GL_pushop(OP_USING);
      A4GL_rep_print(&_rep,1,1,0,1);
      A4GL_push_long(18);
      A4GL_add_spaces();
      A4GL_rep_print(&_rep,1,1,0,2);
      A4GL_rep_print(&_rep,0,1,0,-1);
         ERR_CHK_ERROR  { A4GL_chk_err(107,_module_name); }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_PRINT_CMD Line 108 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,108);
      aclfgli_clr_err_flg();
      A4GL_push_char("Ticket Status:   ");
      A4GL_rep_print(&_rep,1,1,0,3);

      A4GL_push_variable(&lr_data.status,0xe000d);
      A4GL_rep_print(&_rep,1,1,0,4);
      A4GL_rep_print(&_rep,0,0,0,-1);
         ERR_CHK_ERROR  { A4GL_chk_err(108,_module_name); }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_PRINT_CMD Line 109 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,109);
      aclfgli_clr_err_flg();
      A4GL_push_char("Ticket Type:   ");
      A4GL_rep_print(&_rep,1,1,0,5);

      A4GL_push_variable(&lr_data.ttype,0xe000d);
      A4GL_rep_print(&_rep,1,1,0,6);
      A4GL_push_long(12);
      A4GL_add_spaces();
      A4GL_rep_print(&_rep,1,1,0,7);
      A4GL_rep_print(&_rep,0,1,0,-1);
         ERR_CHK_ERROR  { A4GL_chk_err(109,_module_name); }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_PRINT_CMD Line 110 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,110);
      aclfgli_clr_err_flg();
      A4GL_push_char("Ticket Source:   ");
      A4GL_rep_print(&_rep,1,1,0,8);

      A4GL_push_variable(&lr_data.source,0xe000d);
      A4GL_rep_print(&_rep,1,1,0,9);
      A4GL_rep_print(&_rep,0,0,0,-1);
         ERR_CHK_ERROR  { A4GL_chk_err(110,_module_name); }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_PRINT_CMD Line 111 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,111);
      aclfgli_clr_err_flg();
      A4GL_push_char("Ticket Scope:  ");
      A4GL_rep_print(&_rep,1,1,0,10);

      A4GL_push_variable(&lr_data.scope,0xe000d);
      A4GL_rep_print(&_rep,1,1,0,11);
      A4GL_push_long(12);
      A4GL_add_spaces();
      A4GL_rep_print(&_rep,1,1,0,12);
      A4GL_rep_print(&_rep,0,1,0,-1);
         ERR_CHK_ERROR  { A4GL_chk_err(111,_module_name); }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_PRINT_CMD Line 112 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,112);
      aclfgli_clr_err_flg();
      A4GL_push_char("Site/Line:       ");
      A4GL_rep_print(&_rep,1,1,0,13);

      A4GL_push_variable(&lr_data.site,0x1d000d);
      A4GL_rep_print(&_rep,1,1,0,14);
      A4GL_rep_print(&_rep,0,0,0,-1);
         ERR_CHK_ERROR  { A4GL_chk_err(112,_module_name); }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_PRINT_CMD Line 113 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,113);
      aclfgli_clr_err_flg();
      A4GL_push_char("Ticket Owner:  ");
      A4GL_rep_print(&_rep,1,1,0,15);

      A4GL_push_variable(&lr_data.owner,0xe000d);
      A4GL_rep_print(&_rep,1,1,0,16);
      A4GL_push_long(12);
      A4GL_add_spaces();
      A4GL_rep_print(&_rep,1,1,0,17);
      A4GL_rep_print(&_rep,0,1,0,-1);
         ERR_CHK_ERROR  { A4GL_chk_err(113,_module_name); }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_PRINT_CMD Line 114 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,114);
      aclfgli_clr_err_flg();
      A4GL_push_char("Problem Fixer:   ");
      A4GL_rep_print(&_rep,1,1,0,18);

      A4GL_push_variable(&lr_data.fixer,0xe000d);
      A4GL_rep_print(&_rep,1,1,0,19);
      A4GL_rep_print(&_rep,0,0,0,-1);
         ERR_CHK_ERROR  { A4GL_chk_err(114,_module_name); }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_SKIP_CMD Line 115 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,115);
      aclfgli_clr_err_flg();
      A4GL_push_long(1);
      A4GL_aclfgli_skip_lines(&_rep);
         ERR_CHK_ERROR  { A4GL_chk_err(115,_module_name); }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_PRINT_CMD Line 116 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,116);
      aclfgli_clr_err_flg();
      A4GL_push_char("Ticket Opened: ");
      A4GL_rep_print(&_rep,1,1,0,20);

      A4GL_push_variable(&lr_data.date_open,0x7);
      A4GL_push_char("mm/dd/yy  ");
      A4GL_pushop(OP_USING);
      A4GL_rep_print(&_rep,1,1,0,21);

      A4GL_push_variable(&lr_data.time_open,0x9000d);
      A4GL_rep_print(&_rep,1,1,0,22);
      A4GL_rep_print(&_rep,0,1,0,-1);
         ERR_CHK_ERROR  { A4GL_chk_err(116,_module_name); }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_IF_CMD Line 117 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,117);
      aclfgli_clr_err_flg();

      A4GL_push_variable(&lr_data.date_start,0x7);
      A4GL_pushop(OP_ISNOTNULL);

      A4GL_push_variable(&lr_data.time_start,0x9000d);
      A4GL_pushop(OP_ISNOTNULL);
      A4GL_pushop(OP_AND);
      if (A4GL_pop_bool()) {
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_PRINT_CMD Line 118 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,118);
         aclfgli_clr_err_flg();
         A4GL_push_long(7);
         A4GL_add_spaces();
         A4GL_rep_print(&_rep,1,1,0,23);
         A4GL_push_char("Problem Started: ");
         A4GL_rep_print(&_rep,1,1,0,24);

         A4GL_push_variable(&lr_data.date_start,0x7);
         A4GL_push_char("mm/dd/yy  ");
         A4GL_pushop(OP_USING);
         A4GL_rep_print(&_rep,1,1,0,25);

         A4GL_push_variable(&lr_data.time_start,0x9000d);
         A4GL_rep_print(&_rep,1,1,0,26);
         A4GL_rep_print(&_rep,0,0,0,-1);
            ERR_CHK_ERROR  { A4GL_chk_err(118,_module_name); }
      }
       else {
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_PRINT_CMD Line 120 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,120);
         aclfgli_clr_err_flg();
         A4GL_push_empty_char();
         A4GL_rep_print(&_rep,1,1,0,27);
         A4GL_rep_print(&_rep,0,0,0,-1);
            ERR_CHK_ERROR  { A4GL_chk_err(120,_module_name); }
      }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_IF_CMD Line 123 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,123);
      aclfgli_clr_err_flg();

      A4GL_push_variable(&lr_data.date_closed,0x7);
      A4GL_pushop(OP_ISNOTNULL);

      A4GL_push_variable(&lr_data.time_closed,0x9000d);
      A4GL_pushop(OP_ISNOTNULL);
      A4GL_pushop(OP_AND);
      if (A4GL_pop_bool()) {
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_PRINT_CMD Line 124 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,124);
         aclfgli_clr_err_flg();
         A4GL_push_long(7);
         A4GL_add_spaces();
         A4GL_rep_print(&_rep,1,1,0,28);
         A4GL_push_char("Closed: ");
         A4GL_rep_print(&_rep,1,1,0,29);

         A4GL_push_variable(&lr_data.date_closed,0x7);
         A4GL_push_char("mm/dd/yy  ");
         A4GL_pushop(OP_USING);
         A4GL_rep_print(&_rep,1,1,0,30);

         A4GL_push_variable(&lr_data.time_closed,0x9000d);
         A4GL_rep_print(&_rep,1,1,0,31);
         A4GL_rep_print(&_rep,0,1,0,-1);
            ERR_CHK_ERROR  { A4GL_chk_err(124,_module_name); }
      }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_IF_CMD Line 127 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,127);
      aclfgli_clr_err_flg();

      A4GL_push_variable(&lr_data.date_end,0x7);
      A4GL_pushop(OP_ISNOTNULL);

      A4GL_push_variable(&lr_data.time_end,0x9000d);
      A4GL_pushop(OP_ISNOTNULL);
      A4GL_pushop(OP_AND);
      if (A4GL_pop_bool()) {
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_PRINT_CMD Line 128 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,128);
         aclfgli_clr_err_flg();
         A4GL_push_long(15);
         A4GL_add_spaces();
         A4GL_rep_print(&_rep,1,1,0,32);
         A4GL_push_char("Ended:");
         A4GL_rep_print(&_rep,1,1,0,33);
         A4GL_push_long(3);
         A4GL_add_spaces();
         A4GL_rep_print(&_rep,1,1,0,34);

         A4GL_push_variable(&lr_data.date_end,0x7);
         A4GL_push_char("mm/dd/yy  ");
         A4GL_pushop(OP_USING);
         A4GL_rep_print(&_rep,1,1,0,35);

         A4GL_push_variable(&lr_data.time_end,0x9000d);
         A4GL_rep_print(&_rep,1,1,0,36);
         A4GL_rep_print(&_rep,0,0,0,-1);
            ERR_CHK_ERROR  { A4GL_chk_err(128,_module_name); }
      }
       else {
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_PRINT_CMD Line 130 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,130);
         aclfgli_clr_err_flg();
         A4GL_push_empty_char();
         A4GL_rep_print(&_rep,1,1,0,37);
         A4GL_rep_print(&_rep,0,0,0,-1);
            ERR_CHK_ERROR  { A4GL_chk_err(130,_module_name); }
      }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_IF_CMD Line 133 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,133);
      aclfgli_clr_err_flg();

      A4GL_push_variable(&lr_data.closecode,0x13000d);
      A4GL_pushop(OP_ISNOTNULL);
      if (A4GL_pop_bool()) {
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_SKIP_CMD Line 134 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,134);
         aclfgli_clr_err_flg();
         A4GL_push_long(1);
         A4GL_aclfgli_skip_lines(&_rep);
            ERR_CHK_ERROR  { A4GL_chk_err(134,_module_name); }
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_PRINT_CMD Line 135 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,135);
         aclfgli_clr_err_flg();
         A4GL_push_char("Close Code:    ");
         A4GL_rep_print(&_rep,1,1,0,38);

         A4GL_push_variable(&lr_data.closecode,0x13000d);
         A4GL_rep_print(&_rep,1,1,0,39);
         A4GL_rep_print(&_rep,0,0,0,-1);
            ERR_CHK_ERROR  { A4GL_chk_err(135,_module_name); }
      }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_SKIP_CMD Line 138 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,138);
      aclfgli_clr_err_flg();
      A4GL_push_long(1);
      A4GL_aclfgli_skip_lines(&_rep);
         ERR_CHK_ERROR  { A4GL_chk_err(138,_module_name); }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_PRINT_CMD Line 139 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,139);
      aclfgli_clr_err_flg();
      A4GL_push_char("Problem Description:");
      A4GL_rep_print(&_rep,1,1,0,40);
      A4GL_rep_print(&_rep,0,0,0,-1);
         ERR_CHK_ERROR  { A4GL_chk_err(139,_module_name); }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_IF_CMD Line 140 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,140);
      aclfgli_clr_err_flg();

      A4GL_push_substr(lr_data.problem,19922957,1,300,0);
      A4GL_push_char(" ");
      A4GL_push_char("\\");
      A4GL_pushop(OP_MATCHES);A4GL_pushop(OP_NOT);
      if (A4GL_pop_bool()) {
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_PRINT_CMD Line 141 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,141);
         aclfgli_clr_err_flg();
         A4GL_push_long(17);
         A4GL_set_column(&_rep);
         A4GL_rep_print(&_rep,1,1,0,41);

         A4GL_push_substr(lr_data.problem,19922957,1,60,0);
         A4GL_rep_print(&_rep,1,1,0,42);
         A4GL_rep_print(&_rep,0,0,0,-1);
            ERR_CHK_ERROR  { A4GL_chk_err(141,_module_name); }
      }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_IF_CMD Line 144 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,144);
      aclfgli_clr_err_flg();

      A4GL_push_substr(lr_data.problem,19922957,61,300,0);
      A4GL_push_char(" ");
      A4GL_push_char("\\");
      A4GL_pushop(OP_MATCHES);A4GL_pushop(OP_NOT);
      if (A4GL_pop_bool()) {
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_PRINT_CMD Line 145 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,145);
         aclfgli_clr_err_flg();
         A4GL_push_long(17);
         A4GL_set_column(&_rep);
         A4GL_rep_print(&_rep,1,1,0,43);

         A4GL_push_substr(lr_data.problem,19922957,61,120,0);
         A4GL_rep_print(&_rep,1,1,0,44);
         A4GL_rep_print(&_rep,0,0,0,-1);
            ERR_CHK_ERROR  { A4GL_chk_err(145,_module_name); }
      }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_IF_CMD Line 148 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,148);
      aclfgli_clr_err_flg();

      A4GL_push_substr(lr_data.problem,19922957,121,300,0);
      A4GL_push_char(" ");
      A4GL_push_char("\\");
      A4GL_pushop(OP_MATCHES);A4GL_pushop(OP_NOT);
      if (A4GL_pop_bool()) {
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_PRINT_CMD Line 149 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,149);
         aclfgli_clr_err_flg();
         A4GL_push_long(17);
         A4GL_set_column(&_rep);
         A4GL_rep_print(&_rep,1,1,0,45);

         A4GL_push_substr(lr_data.problem,19922957,121,180,0);
         A4GL_rep_print(&_rep,1,1,0,46);
         A4GL_rep_print(&_rep,0,0,0,-1);
            ERR_CHK_ERROR  { A4GL_chk_err(149,_module_name); }
      }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_IF_CMD Line 152 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,152);
      aclfgli_clr_err_flg();

      A4GL_push_substr(lr_data.problem,19922957,181,300,0);
      A4GL_push_char(" ");
      A4GL_push_char("\\");
      A4GL_pushop(OP_MATCHES);A4GL_pushop(OP_NOT);
      if (A4GL_pop_bool()) {
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_PRINT_CMD Line 153 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,153);
         aclfgli_clr_err_flg();
         A4GL_push_long(17);
         A4GL_set_column(&_rep);
         A4GL_rep_print(&_rep,1,1,0,47);

         A4GL_push_substr(lr_data.problem,19922957,181,240,0);
         A4GL_rep_print(&_rep,1,1,0,48);
         A4GL_rep_print(&_rep,0,0,0,-1);
            ERR_CHK_ERROR  { A4GL_chk_err(153,_module_name); }
      }
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_IF_CMD Line 156 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,156);
      aclfgli_clr_err_flg();

      A4GL_push_substr(lr_data.problem,19922957,241,300,0);
      A4GL_push_char(" ");
      A4GL_push_char("\\");
      A4GL_pushop(OP_MATCHES);A4GL_pushop(OP_NOT);
      if (A4GL_pop_bool()) {
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_PRINT_CMD Line 157 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,157);
         aclfgli_clr_err_flg();
         A4GL_push_long(17);
         A4GL_set_column(&_rep);
         A4GL_rep_print(&_rep,1,1,0,49);

         A4GL_push_substr(lr_data.problem,19922957,241,300,0);
         A4GL_rep_print(&_rep,1,1,0,50);
         A4GL_rep_print(&_rep,0,0,0,-1);
            ERR_CHK_ERROR  { A4GL_chk_err(157,_module_name); }
      }
      A4GL_pop_report_section(&_rep,1);
      goto report1_ctrl; /* G1 */

   rep_ctrl1_2:
      A4GL_push_report_section(&_rep,_module_name,_reportName,161,'E',"EVERY",2);
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_IF_CMD Line 162 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,162);
      aclfgli_clr_err_flg();

      A4GL_push_int(lr_data.note_no);
      A4GL_pushop(OP_ISNOTNULL);
      if (A4GL_pop_bool()) {
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_SKIP_CMD Line 163 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,163);
         aclfgli_clr_err_flg();
         A4GL_push_long(2);
         A4GL_aclfgli_skip_lines(&_rep);
            ERR_CHK_ERROR  { A4GL_chk_err(163,_module_name); }
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_PRINT_CMD Line 164 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,164);
         aclfgli_clr_err_flg();
         A4GL_push_char("Note: ");
         A4GL_rep_print(&_rep,1,1,0,0);

         A4GL_push_int(lr_data.note_no);
         A4GL_push_char("<<<");
         A4GL_pushop(OP_USING);
         A4GL_rep_print(&_rep,1,1,0,1);
         A4GL_rep_print(&_rep,0,1,0,-1);
            ERR_CHK_ERROR  { A4GL_chk_err(164,_module_name); }
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_PRINT_CMD Line 165 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,165);
         aclfgli_clr_err_flg();
         A4GL_push_long(17);
         A4GL_set_column(&_rep);
         A4GL_rep_print(&_rep,1,1,0,2);

         A4GL_push_substr(lr_data.note,35651597,1,60,0);
         A4GL_rep_print(&_rep,1,1,0,3);
         A4GL_rep_print(&_rep,0,0,0,-1);
            ERR_CHK_ERROR  { A4GL_chk_err(165,_module_name); }
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_PRINT_CMD Line 166 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,166);
         aclfgli_clr_err_flg();

         A4GL_push_variable(&lr_data.note_date,0x7);
         A4GL_push_char("mm/dd/yy ");
         A4GL_pushop(OP_USING);
         A4GL_rep_print(&_rep,1,1,0,4);

         A4GL_push_variable(&lr_data.note_time,0x9000d);
         A4GL_rep_print(&_rep,1,1,0,5);
         A4GL_rep_print(&_rep,0,1,0,-1);
            ERR_CHK_ERROR  { A4GL_chk_err(166,_module_name); }
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_PRINT_CMD Line 167 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,167);
         aclfgli_clr_err_flg();
         A4GL_push_long(17);
         A4GL_set_column(&_rep);
         A4GL_rep_print(&_rep,1,1,0,6);

         A4GL_push_substr(lr_data.note,35651597,61,120,0);
         A4GL_rep_print(&_rep,1,1,0,7);
         A4GL_rep_print(&_rep,0,0,0,-1);
            ERR_CHK_ERROR  { A4GL_chk_err(167,_module_name); }
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_PRINT_CMD Line 168 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,168);
         aclfgli_clr_err_flg();

         A4GL_push_substr(lr_data.name,1572877,1,15,0);
         A4GL_rep_print(&_rep,1,1,0,8);
         A4GL_push_long(17);
         A4GL_set_column(&_rep);
         A4GL_rep_print(&_rep,1,1,0,9);

         A4GL_push_substr(lr_data.note,35651597,121,180,0);
         A4GL_rep_print(&_rep,1,1,0,10);
         A4GL_rep_print(&_rep,0,0,0,-1);
            ERR_CHK_ERROR  { A4GL_chk_err(168,_module_name); }
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_IF_CMD Line 169 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,169);
         aclfgli_clr_err_flg();

         A4GL_push_substr(lr_data.note,35651597,181,540,0);
         A4GL_push_char(" ");
         A4GL_push_char("\\");
         A4GL_pushop(OP_MATCHES);A4GL_pushop(OP_NOT);
         if (A4GL_pop_bool()) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_PRINT_CMD Line 170 E_MET_REPORT_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,170);
            aclfgli_clr_err_flg();
            A4GL_push_long(17);
            A4GL_set_column(&_rep);
            A4GL_rep_print(&_rep,1,1,0,11);

            A4GL_push_substr(lr_data.note,35651597,181,240,0);
            A4GL_rep_print(&_rep,1,1,0,12);
            A4GL_rep_print(&_rep,0,0,0,-1);
               ERR_CHK_ERROR  { A4GL_chk_err(170,_module_name); }
         }
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_IF_CMD Line 173 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,173);
         aclfgli_clr_err_flg();

         A4GL_push_substr(lr_data.note,35651597,241,540,0);
         A4GL_push_char(" ");
         A4GL_push_char("\\");
         A4GL_pushop(OP_MATCHES);A4GL_pushop(OP_NOT);
         if (A4GL_pop_bool()) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_PRINT_CMD Line 174 E_MET_REPORT_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,174);
            aclfgli_clr_err_flg();
            A4GL_push_long(17);
            A4GL_set_column(&_rep);
            A4GL_rep_print(&_rep,1,1,0,13);

            A4GL_push_substr(lr_data.note,35651597,241,300,0);
            A4GL_rep_print(&_rep,1,1,0,14);
            A4GL_rep_print(&_rep,0,0,0,-1);
               ERR_CHK_ERROR  { A4GL_chk_err(174,_module_name); }
         }
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_IF_CMD Line 177 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,177);
         aclfgli_clr_err_flg();

         A4GL_push_substr(lr_data.note,35651597,301,540,0);
         A4GL_push_char(" ");
         A4GL_push_char("\\");
         A4GL_pushop(OP_MATCHES);A4GL_pushop(OP_NOT);
         if (A4GL_pop_bool()) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_PRINT_CMD Line 178 E_MET_REPORT_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,178);
            aclfgli_clr_err_flg();
            A4GL_push_long(17);
            A4GL_set_column(&_rep);
            A4GL_rep_print(&_rep,1,1,0,15);

            A4GL_push_substr(lr_data.note,35651597,301,360,0);
            A4GL_rep_print(&_rep,1,1,0,16);
            A4GL_rep_print(&_rep,0,0,0,-1);
               ERR_CHK_ERROR  { A4GL_chk_err(178,_module_name); }
         }
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_IF_CMD Line 181 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,181);
         aclfgli_clr_err_flg();

         A4GL_push_substr(lr_data.note,35651597,361,540,0);
         A4GL_push_char(" ");
         A4GL_push_char("\\");
         A4GL_pushop(OP_MATCHES);A4GL_pushop(OP_NOT);
         if (A4GL_pop_bool()) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_PRINT_CMD Line 182 E_MET_REPORT_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,182);
            aclfgli_clr_err_flg();
            A4GL_push_long(17);
            A4GL_set_column(&_rep);
            A4GL_rep_print(&_rep,1,1,0,17);

            A4GL_push_substr(lr_data.note,35651597,361,420,0);
            A4GL_rep_print(&_rep,1,1,0,18);
            A4GL_rep_print(&_rep,0,0,0,-1);
               ERR_CHK_ERROR  { A4GL_chk_err(182,_module_name); }
         }
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_IF_CMD Line 185 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,185);
         aclfgli_clr_err_flg();

         A4GL_push_substr(lr_data.note,35651597,421,540,0);
         A4GL_push_char(" ");
         A4GL_push_char("\\");
         A4GL_pushop(OP_MATCHES);A4GL_pushop(OP_NOT);
         if (A4GL_pop_bool()) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_PRINT_CMD Line 186 E_MET_REPORT_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,186);
            aclfgli_clr_err_flg();
            A4GL_push_long(17);
            A4GL_set_column(&_rep);
            A4GL_rep_print(&_rep,1,1,0,19);

            A4GL_push_substr(lr_data.note,35651597,421,480,0);
            A4GL_rep_print(&_rep,1,1,0,20);
            A4GL_rep_print(&_rep,0,0,0,-1);
               ERR_CHK_ERROR  { A4GL_chk_err(186,_module_name); }
         }
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_IF_CMD Line 189 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,189);
         aclfgli_clr_err_flg();

         A4GL_push_substr(lr_data.note,35651597,481,540,0);
         A4GL_push_char(" ");
         A4GL_push_char("\\");
         A4GL_pushop(OP_MATCHES);A4GL_pushop(OP_NOT);
         if (A4GL_pop_bool()) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_PRINT_CMD Line 190 E_MET_REPORT_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,190);
            aclfgli_clr_err_flg();
            A4GL_push_long(17);
            A4GL_set_column(&_rep);
            A4GL_rep_print(&_rep,1,1,0,21);

            A4GL_push_substr(lr_data.note,35651597,481,540,0);
            A4GL_rep_print(&_rep,1,1,0,22);
            A4GL_rep_print(&_rep,0,0,0,-1);
               ERR_CHK_ERROR  { A4GL_chk_err(190,_module_name); }
         }
      }
      A4GL_pop_report_section(&_rep,2);
      goto report1_ctrl; /* G1 */

   rep_ctrl1_3:
      A4GL_push_report_section(&_rep,_module_name,_reportName,197,'A',"2",3);
      A4GL_clr_ignore_error_list();
      /* CMD : E_CMD_IF_CMD Line 197 E_MET_REPORT_DEFINITION */
      A4GLSTK_setCurrentLine(_module_name,197);
      aclfgli_clr_err_flg();

      A4GL_push_variable(&lr_data.solution,0x130000d);
      A4GL_push_char(" ");
      A4GL_push_char("\\");
      A4GL_pushop(OP_MATCHES);A4GL_pushop(OP_NOT);
      if (A4GL_pop_bool()) {
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_SKIP_CMD Line 198 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,198);
         aclfgli_clr_err_flg();
         A4GL_push_long(1);
         A4GL_aclfgli_skip_lines(&_rep);
            ERR_CHK_ERROR  { A4GL_chk_err(198,_module_name); }
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_PRINT_CMD Line 199 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,199);
         aclfgli_clr_err_flg();
         A4GL_push_char("Problem Solution:");
         A4GL_rep_print(&_rep,1,1,0,0);
         A4GL_rep_print(&_rep,0,0,0,-1);
            ERR_CHK_ERROR  { A4GL_chk_err(199,_module_name); }
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_IF_CMD Line 200 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,200);
         aclfgli_clr_err_flg();

         A4GL_push_substr(lr_data.solution,19922957,1,300,0);
         A4GL_push_char(" ");
         A4GL_push_char("\\");
         A4GL_pushop(OP_MATCHES);A4GL_pushop(OP_NOT);
         if (A4GL_pop_bool()) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_PRINT_CMD Line 201 E_MET_REPORT_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,201);
            aclfgli_clr_err_flg();
            A4GL_push_long(17);
            A4GL_set_column(&_rep);
            A4GL_rep_print(&_rep,1,1,0,1);

            A4GL_push_substr(lr_data.solution,19922957,1,60,0);
            A4GL_rep_print(&_rep,1,1,0,2);
            A4GL_rep_print(&_rep,0,0,0,-1);
               ERR_CHK_ERROR  { A4GL_chk_err(201,_module_name); }
         }
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_IF_CMD Line 204 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,204);
         aclfgli_clr_err_flg();

         A4GL_push_substr(lr_data.solution,19922957,61,300,0);
         A4GL_push_char(" ");
         A4GL_push_char("\\");
         A4GL_pushop(OP_MATCHES);A4GL_pushop(OP_NOT);
         if (A4GL_pop_bool()) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_PRINT_CMD Line 205 E_MET_REPORT_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,205);
            aclfgli_clr_err_flg();
            A4GL_push_long(17);
            A4GL_set_column(&_rep);
            A4GL_rep_print(&_rep,1,1,0,3);

            A4GL_push_substr(lr_data.solution,19922957,61,120,0);
            A4GL_rep_print(&_rep,1,1,0,4);
            A4GL_rep_print(&_rep,0,0,0,-1);
               ERR_CHK_ERROR  { A4GL_chk_err(205,_module_name); }
         }
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_IF_CMD Line 208 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,208);
         aclfgli_clr_err_flg();

         A4GL_push_substr(lr_data.solution,19922957,121,300,0);
         A4GL_push_char(" ");
         A4GL_push_char("\\");
         A4GL_pushop(OP_MATCHES);A4GL_pushop(OP_NOT);
         if (A4GL_pop_bool()) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_PRINT_CMD Line 209 E_MET_REPORT_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,209);
            aclfgli_clr_err_flg();
            A4GL_push_long(17);
            A4GL_set_column(&_rep);
            A4GL_rep_print(&_rep,1,1,0,5);

            A4GL_push_substr(lr_data.solution,19922957,121,180,0);
            A4GL_rep_print(&_rep,1,1,0,6);
            A4GL_rep_print(&_rep,0,0,0,-1);
               ERR_CHK_ERROR  { A4GL_chk_err(209,_module_name); }
         }
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_IF_CMD Line 212 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,212);
         aclfgli_clr_err_flg();

         A4GL_push_substr(lr_data.solution,19922957,181,300,0);
         A4GL_push_char(" ");
         A4GL_push_char("\\");
         A4GL_pushop(OP_MATCHES);A4GL_pushop(OP_NOT);
         if (A4GL_pop_bool()) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_PRINT_CMD Line 213 E_MET_REPORT_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,213);
            aclfgli_clr_err_flg();
            A4GL_push_long(17);
            A4GL_set_column(&_rep);
            A4GL_rep_print(&_rep,1,1,0,7);

            A4GL_push_substr(lr_data.solution,19922957,181,240,0);
            A4GL_rep_print(&_rep,1,1,0,8);
            A4GL_rep_print(&_rep,0,0,0,-1);
               ERR_CHK_ERROR  { A4GL_chk_err(213,_module_name); }
         }
         A4GL_clr_ignore_error_list();
         /* CMD : E_CMD_IF_CMD Line 216 E_MET_REPORT_DEFINITION */
         A4GLSTK_setCurrentLine(_module_name,216);
         aclfgli_clr_err_flg();

         A4GL_push_substr(lr_data.solution,19922957,241,300,0);
         A4GL_push_char(" ");
         A4GL_push_char("\\");
         A4GL_pushop(OP_MATCHES);A4GL_pushop(OP_NOT);
         if (A4GL_pop_bool()) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_PRINT_CMD Line 217 E_MET_REPORT_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,217);
            aclfgli_clr_err_flg();
            A4GL_push_long(17);
            A4GL_set_column(&_rep);
            A4GL_rep_print(&_rep,1,1,0,9);

            A4GL_push_substr(lr_data.solution,19922957,241,300,0);
            A4GL_rep_print(&_rep,1,1,0,10);
            A4GL_rep_print(&_rep,0,0,0,-1);
               ERR_CHK_ERROR  { A4GL_chk_err(217,_module_name); }
         }
      }
      A4GL_pop_report_section(&_rep,3);
      goto report1_ctrl; /* G1 */
   report1_ctrl:
   if (_rep.lines_in_header      ==-1) _rep.lines_in_header=0;
   if (_rep.lines_in_first_header==-1) _rep.lines_in_first_header=0;
   if (_rep.lines_in_trailer     ==-1) _rep.lines_in_trailer=0;
       if (acl_ctrl==REPORT_OPS_COMPLETE) return;
       if (acl_ctrl==REPORT_SENDDATA) {
      /* check for after group of */
          aclfgl_rep_oneticket(0,REPORT_DATA);
      /* check for before group of */
       }
   if (acl_ctrl==REPORT_FINISH && _started==0) {
       A4GL_exitwith("You cannot FINISH REPORT - because the report has not been started");
       return;
       }
   if (acl_ctrl==REPORT_FINISH && _started) {aclfgl_rep_oneticket(0,REPORT_LASTDATA);return;}
   if (acl_ctrl==REPORT_LASTDATA) {
     if (_useddata || A4GL_always_output_report(&_rep)) {
      /*if (_useddata)*/ {aclfgl_rep_oneticket(0,REPORT_LASTROW);}
      if (_rep.page_no<=1&&_rep.page_length>1 &&_rep.header ) {A4GL_rep_print(&_rep,0,1,0,-1);A4GL_rep_print(&_rep,0,0,0,-1);}
      _rep.finishing=1;
      A4GL_skip_top_of_page(&_rep,999);
   }
     _started=0;
     if (_rep.output) {A4GL_close_report_file(&_rep);}
     return;
   }
   if (acl_ctrl==REPORT_TERMINATE) {_started=0;if (_rep.output) {A4GL_close_report_file(&_rep);}return;}
       if (acl_ctrl==REPORT_AFTERDATA ) {
       return;
       }
    if (acl_ctrl==REPORT_START||acl_ctrl==REPORT_RESTART) {
   }
   if (acl_ctrl==REPORT_PAGEHEADER&&_rep.page_no==1) {acl_ctrl=0;goto rep_ctrl1_0;}
   if (acl_ctrl==REPORT_BEFOREGROUP&&_nargs==1) {_nargs=-1*_nargs;goto rep_ctrl1_1;}
   if (acl_ctrl==REPORT_DATA) {acl_ctrl=REPORT_NOTHING; goto rep_ctrl1_2;}
   if (acl_ctrl==REPORT_AFTERGROUP&&_nargs==1) {_nargs=-1*_nargs;goto rep_ctrl1_3;}
A4GL_copy_back_blobs(_blobdata,0);
} /* end of report */
/* END OF MODULE */
