# vim: set ts=2 sw=2 et nu colorcolumn=80:
GLOBALS
  DEFINE ticket RECORD
    ticket_no SMALLINT,
    status CHAR(10),
    ttype CHAR(10),
    source CHAR(10),
    scope CHAR(10),
    site CHAR(25),
    owner CHAR(10),
    date_open DATE,
    time_open CHAR(5),
    date_start DATE,
    time_start CHAR(5),
    date_closed DATE,
    time_closed CHAR(5),
    date_end DATE,
    time_end CHAR(5),
    notifications CHAR(120),
    problem CHAR(300),
    closecode CHAR(15),
    fixer CHAR(10),
    solution CHAR(300),
    notify CHAR(1)
  END RECORD
  DEFINE note RECORD
    ticket_no SMALLINT,
    note_no SMALLINT,
    name CHAR(20),
    note_date DATE,
    note_time CHAR(5),
    note CHAR(540),
    notify CHAR(1)
  END RECORD
END GLOBALS
