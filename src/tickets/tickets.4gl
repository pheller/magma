# vim: set ts=2 sw=2 et nu colorcolumn=80:
DATABASE nearnet
MAIN
  DEFINE ticket RECORD
        ticket_no SMALLINT,
        status CHAR(10),
        ttype CHAR(10),
        source CHAR(10),
        scope CHAR(10),
        site CHAR(25),
        owner CHAR(10),
        date_open DATE,
        time_open CHAR(5),
        date_start DATE,
        time_start CHAR(5),
        date_closed DATE,
        time_closed CHAR(5),
        date_end DATE,
        time_end CHAR(5),
        notifications CHAR(120),
        problem CHAR(300),
        closecode CHAR(15),
        fixer CHAR(10),
        solution CHAR(300),
        notify CHAR(1)
  END RECORD
  DEFINE note RECORD LIKE ticket_notes.*
  DEFINE criteria CHAR(32000)
  DEFINE query CHAR(32767)
  DEFINE cur_ticket_no INT
  DEFINE cur_note_no INT
  DEFINE view CHAR(32)
  DEFINE ticket_cursor_open INT
  DEFINE note_cursor_open INT
  DEFINE next_ticket_no SMALLINT
  DEFINE next_note_no SMALLINT
  DEFINE username CHAR(20)

  LET username = aclfgl_get_user()
  
  LET view = "tickets"
  LET cur_ticket_no = 0
  LET cur_note_no = 0

  LET ticket_cursor_open = FALSE
  LET note_cursor_open = FALSE

  OPTIONS ACCEPT KEY ESC

  CALL form_is_compiled(tickets, "MEMPACKED", "GENERIC")
  CALL form_is_compiled(notes, "MEMPACKED", "GENERIC")

  OPEN FORM tickets FROM "tickets"
  OPEN FORM notes FROM "notes"

  DISPLAY FORM tickets

  MENU "Perform"
    COMMAND KEY ("Q") "Query" "Searches the active database table."
      CONSTRUCT criteria ON tickets.* FROM tickets.*
      LET query="select ticket_no, status, ttype, source, scope, site, owner, date_open, time_open, date_start, time_start, date_closed, time_closed, date_end, time_end, notifications, problem, closecode, fixer, solution, notify from tickets where ", criteria
      PREPARE statement FROM query
      DECLARE cur CURSOR FOR statement
      OPEN cur
      EXECUTE statement
      FETCH NEXT cur INTO ticket
      LET cur_ticket_no = ticket.ticket_no
      LET ticket_cursor_open = TRUE
      DISPLAY BY NAME ticket.*
    COMMAND KEY("N") "Next"
      CASE view
        WHEN "tickets"
          IF ticket_cursor_open = TRUE THEN
            FETCH NEXT cur INTO ticket
            LET cur_ticket_no = ticket.ticket_no
            LET cur_note_no = 0
            DISPLAY BY NAME ticket.*
          END IF
        EXIT CASE
        WHEN "ticket_notes"
          IF note_cursor_open = TRUE THEN
            FETCH NEXT ncur INTO note
            LET cur_note_no = note.note_no
            DISPLAY BY NAME note.*
          END IF
        EXIT CASE
      END CASE
    COMMAND KEY("P") "Previous"
      CASE view
        WHEN "tickets"
          IF ticket_cursor_open = TRUE THEN
            FETCH PREVIOUS cur INTO ticket
            LET cur_ticket_no = ticket.ticket_no
            LET cur_note_no = 0
            DISPLAY BY NAME ticket.*
          END IF
        EXIT CASE
        WHEN "ticket_notes"
          IF note_cursor_open = TRUE THEN
            FETCH PREVIOUS ncur INTO note
            LET cur_note_no = note.note_no
            DISPLAY BY NAME note.*
          END IF
        EXIT CASE      
      END CASE
    COMMAND KEY("D")
      LET view = "ticket_notes"
      DISPLAY FORM notes
      IF cur_ticket_no > 0 THEN
        LET query = "select * from ticket_notes where ticket_notes.ticket_no=", cur_ticket_no
        PREPARE statement FROM query
        DECLARE ncur CURSOR FOR statement
        OPEN ncur
        EXECUTE statement
        IF cur_note_no > 0 THEN
          FETCH ABSOLUTE cur_note_no ncur INTO note
        ELSE
          FETCH NEXT ncur INTO note
        END IF
        IF cur_note_no == 0 THEN
          LET note.note_no = ""
        END IF
        LET note_cursor_open = TRUE
        LET note.ticket_no = cur_ticket_no
        DISPLAY BY NAME note.*
      END IF
    COMMAND KEY("M")
      LET view = "tickets"
      DISPLAY FORM tickets
      IF cur_ticket_no > 0 THEN
        DISPLAY BY NAME ticket.*
      END IF
    COMMAND KEY("V") "View"
    COMMAND KEY("A") "Add"
      CASE view
        WHEN "tickets"
          LET ticket.status = "open"
          LET ticket.ttype = "unplanned"
          LET ticket.source = "noc"
          LET ticket.scope = "1site"
          LET ticket.owner = username
          LET ticket.date_open = today
          LET ticket.time_open = time
          LET ticket.date_start = today
          LET ticket.time_start = time
          LET ticket.notifications = "nearnet-ops,nearnet-outages"
          LET ticket.notify = "N"
          CLEAR tickets.*
          INPUT BY NAME ticket.* WITHOUT DEFAULTS
            BEFORE FIELD ticket_no NEXT FIELD NEXT
            AFTER FIELD notify NEXT FIELD status
          END INPUT

          SELECT (1 + MAX(ticket_no)) INTO next_ticket_no FROM tickets
          LET ticket.ticket_no = next_ticket_no;
          INSERT INTO tickets ( ticket_no, status, ttype, source, scope, site,
            owner, date_open, time_open, date_start, time_start, date_closed,
            time_closed, date_end, time_end, notifications, problem, closecode,
            fixer, solution, notify
          ) VALUES (
            ticket.ticket_no, ticket.status, ticket.ttype, ticket.source,
            ticket.scope, ticket.site, ticket.owner, ticket.date_open,
            ticket.time_open, ticket.date_start, ticket.time_start,
            ticket.date_closed, ticket.time_closed, ticket.date_end,
            ticket.time_end, ticket.notifications, ticket.problem,
            ticket.closecode, ticket.fixer, ticket.solution, ticket.notify
          )

          SELECT ticket_no, status, ttype, source, scope, site, owner, date_open, time_open, date_start, time_start, date_closed, time_closed, date_end, time_end, notifications, problem, closecode, fixer, solution, notify INTO ticket FROM tickets WHERE ticket_no=next_ticket_no
          
          LET cur_ticket_no = next_ticket_no
          DISPLAY BY NAME ticket.*
        EXIT CASE
        WHEN "ticket_notes"
          LET note.ticket_no = cur_ticket_no
          LET note.name = username
          LET note.note_date = today
          LET note.note_time = time
          LET note.note = ""
          LET note.notify = "Y"
          INPUT BY NAME note.* WITHOUT DEFAULTS
            BEFORE FIELD ticket_no NEXT FIELD NEXT
            BEFORE FIELD note_no NEXT FIELD NEXT
            AFTER FIELD notify NEXT FIELD name
          END INPUT

          SELECT (1 + MAX(note_no)) INTO next_note_no FROM ticket_notes
            WHERE ticket_no = cur_ticket_no

          IF next_note_no IS NULL THEN
            LET next_note_no = 1
          END IF

          LET note.note_no = next_note_no
          INSERT 
            INTO ticket_notes (
              ticket_no, note_no, name, note_date, note_time, note, notify
            ) VALUES (
              note.ticket_no, note.note_no, note.name, note.note_date,
              note.note_time, note.note, note.notify
            )

          SELECT ticket_no, note_no, name, note_date, note_time, note, notify
            INTO note
            FROM ticket_notes
            WHERE ticket_no=cur_ticket_no AND note_no=next_note_no
          LET cur_note_no = next_note_no
          DISPLAY BY NAME note.*
        EXIT CASE
      END CASE
      
    COMMAND KEY("U") "Update"
    COMMAND KEY("R") "Remove"
    COMMAND KEY("T") "Table"
    COMMAND KEY("S") "Screen"
    COMMAND KEY("O") "Output"
    COMMAND KEY("C") "Current"
    COMMAND KEY("E") "Exit"
      EXIT MENU
  END MENU
  CLEAR SCREEN
END MAIN


FUNCTION query()
END FUNCTION
