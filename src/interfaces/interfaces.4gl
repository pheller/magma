MAIN
  OPTIONS ACCEPT KEY ESC
  CALL form_is_compiled(interfaces1, "MEMPACKED", "GENERIC")
  CALL form_is_compiled(interfaces2, "MEMPACKED", "GENERIC")
  OPEN FORM interfaces1 FROM "interfaces1"
  OPEN FORM interfaces2 FROM "interfaces2"
  DISPLAY FORM interfaces1
  MENU "Perform"
    COMMAND "Query" "Searches the active database table"
      CALL query()
    COMMAND KEY ("Q")
      CALL query()
    COMMAND "page-fWd"
      CALL fwd()
    COMMAND KEY("W")
      CALL fwd()
    COMMAND "page-Back"
      CALL back()
    COMMAND KEY("B")
      call back()
    COMMAND "Next"
    COMMAND "Previous"
    COMMAND "View"
    COMMAND "Add"
    COMMAND "Update"
    COMMAND "Delete"
    COMMAND "Exit"
      EXIT MENU
  END MENU
  CLEAR SCREEN
END MAIN

FUNCTION query()
  DEFINE qry CHAR(200)
  CONSTRUCT BY NAME qry ON
    formonly.gateway_name,
    formonly.interface_name,
    formonly.ip_address,
    formonly.type,
    formonly.remote_ip_address,
    formonly.vpi,
    formonly.dlci_vci,
    formonly.remote_vpi,
    formonly.remote_dlci_vpi,
    formonly.subnet_mask,
    formonly.vlan,
    formonly.smds_address,
    formonly.role,
    formonly.encapsulation,
    formonly.purpose,
    formonly.status,
    formonly.available,
    formonly.ticket,
    formonly.comments1,
    formonly.speed,
    formonly.comments2,
    formonly.limit,
    formonly.circuit_id,
    formonly.channel,
    formonly.panel,
    formonly.jack,
    formonly.cabinet,
    formonly.ne,
    formonly.ne_tx,
    formonly.ne_rx,
    formonly.snmp,
    formonly.duplex,
    formonly.vpop,
    formonly.drop_and_insert,
    formonly.stats_name,
    formonly.t1_agg_group,
    formonly.ifindex,
    formonly.isdn,
    formonly.ping,
    formonly.poll,
    formonly.stats,
    formonly.publicize
END FUNCTION

FUNCTION fwd()
    DISPLAY FORM interfaces2
END FUNCTION

FUNCTION back()
    DISPLAY FORM interfaces1
END FUNCTION
