#include "a4gl_incl_4glhdr.h"
extern int a4gl_lib_status;
static char *_module_name="interfaces.4gl";


A4GL_MAIN int main(int argc,char *argv[]);
A4GL_FUNCTION int aclfgl_query (int n);
A4GL_FUNCTION int aclfgl_fwd (int n);
A4GL_FUNCTION int aclfgl_back (int n);
extern char compiled_form_interfaces1[];
extern char compiled_form_interfaces2[];
#define fgldate long
static char const _rcsid[]="$FGLIdent: interfaces.4gl Compiler-1.240  Log:Not Set $";
static void a4gl_show_compiled_version(void) {
printf("Log: Not Set\n");
printf("Aubit4GL Version: 1.2.40\n");
exit(0);
}

static const char *_CompileTimeSQLType="pg8";
static const struct sDependantTable _dependantTables[]= {
  {0,0}
};

extern struct {
   long sqlcode ;
   char sqlerrm [72+1];
   char sqlerrp [8+1];
   long sqlerrd[6] ;
   char sqlawarn [8+1];
   char sqlstate [9+1];
} a4gl_sqlca;
extern long a4gl_status ;
extern long aiplib_status ;
extern long curr_form ;
extern long curr_hwnd ;
extern char err_file_name [32+1];
extern long err_line_no ;
extern long err_status ;
extern long int_flag ;
extern long quit_flag ;

static int _done_init_module_variables=1;
A4GL_INTERNAL_FUNCTION static void init_module_variables(void) {
 static void *_objData[]={
NULL};
   if (_done_init_module_variables==0) return;
   _done_init_module_variables=0;
   A4GL_register_module_objects(_module_name, _objData);
   A4GL_check_version(_module_name,"1.2",40);
   A4GL_check_dependant_tables(_module_name, _CompileTimeSQLType, _dependantTables);
   /* Print nullify */
   // Initialise the current global variables
   // No global variables in use
   // Initialise any other global variables
}




A4GL_MAIN int main(int argc,char *argv[]) {
char *_paramnames[1]={""};
void *_blobdata=0;
int _nargs=0;
static char *_functionName = "MAIN";
    void *_objData[]={
   NULL};
   A4GLSTK_setCurrentLine(0,0);
   if (A4GL_fgl_start(argc,argv)) {a4gl_show_compiled_version();}

   init_module_variables();
   /* Print nullify */
   A4GLSTK_initFunctionCallStack();
   A4GLSTK_pushFunction_v2(_functionName,_paramnames,_nargs,_module_name,1,_objData);

   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_OPTIONS_CMD Line 2 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,2);
   aclfgli_clr_err_flg();
   A4GL_set_option_value('A',A4GL_key_val("escape"));
      ERR_CHK_ERROR  { A4GL_chk_err(2,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 3 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,3);
   aclfgli_clr_err_flg();
   {char *_packer; char *_formtype;int _retvars=0;
   A4GL_push_char("MEMPACKED");
   _packer=A4GL_char_pop();
   A4GL_push_char("GENERIC");
   _formtype=A4GL_char_pop();
   A4GL_add_compiled_form("interfaces1",_formtype,_packer,compiled_form_interfaces1);
   free(_packer);free(_formtype);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(3,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CALL_CMD Line 4 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,4);
   aclfgli_clr_err_flg();
   {char *_packer; char *_formtype;int _retvars=0;
   A4GL_push_char("MEMPACKED");
   _packer=A4GL_char_pop();
   A4GL_push_char("GENERIC");
   _formtype=A4GL_char_pop();
   A4GL_add_compiled_form("interfaces2",_formtype,_packer,compiled_form_interfaces2);
   free(_packer);free(_formtype);
   /* pr 1 */
   CHECK_NO_RETURN;
   }
      ERR_CHK_ERROR  { A4GL_chk_err(4,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_OPEN_FORM_CMD Line 5 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,5);
   aclfgli_clr_err_flg();

   A4GL_push_char("interfaces1");A4GL_open_form("interfaces1");
      ERR_CHK_ERROR  { A4GL_chk_err(5,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_OPEN_FORM_CMD Line 6 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,6);
   aclfgli_clr_err_flg();

   A4GL_push_char("interfaces2");A4GL_open_form("interfaces2");
      ERR_CHK_ERROR  { A4GL_chk_err(6,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_DISPLAY_FORM_CMD Line 7 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,7);
   aclfgli_clr_err_flg();

   A4GL_disp_form("interfaces1",-1);
      ERR_CHK_ERROR  { A4GL_chk_err(7,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_MENU_CMD Line 29 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,29);
   aclfgli_clr_err_flg();
   {
   void *m_1=0;
   int cmd_no_1= -1; /* n=1 */
      MENU_START_1: ;
      while (cmd_no_1!= -3) {
          if (cmd_no_1==0) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_CALL_CMD Line 10 E_MET_MAIN_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,10);
            aclfgli_clr_err_flg();
            /* done print expr */
            {int _retvars;A4GL_set_status(0,0);
            A4GLSTK_setCurrentLine(_module_name,10);
            _retvars=aclfgl_query(0);
            /* pr 1 */
            CHECK_NO_RETURN;
            }
               ERR_CHK_ERROR  { A4GL_chk_err(10,_module_name); }
            /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_1= -4;goto MENU_START_1;  /* 1 */
         }
          if (cmd_no_1==1) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_CALL_CMD Line 12 E_MET_MAIN_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,12);
            aclfgli_clr_err_flg();
            /* done print expr */
            {int _retvars;A4GL_set_status(0,0);
            A4GLSTK_setCurrentLine(_module_name,12);
            _retvars=aclfgl_query(0);
            /* pr 1 */
            CHECK_NO_RETURN;
            }
               ERR_CHK_ERROR  { A4GL_chk_err(12,_module_name); }
            /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_1= -4;goto MENU_START_1;  /* 1 */
         }
          if (cmd_no_1==2) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_CALL_CMD Line 14 E_MET_MAIN_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,14);
            aclfgli_clr_err_flg();
            /* done print expr */
            {int _retvars;A4GL_set_status(0,0);
            A4GLSTK_setCurrentLine(_module_name,14);
            _retvars=aclfgl_fwd(0);
            /* pr 1 */
            CHECK_NO_RETURN;
            }
               ERR_CHK_ERROR  { A4GL_chk_err(14,_module_name); }
            /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_1= -4;goto MENU_START_1;  /* 1 */
         }
          if (cmd_no_1==3) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_CALL_CMD Line 16 E_MET_MAIN_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,16);
            aclfgli_clr_err_flg();
            /* done print expr */
            {int _retvars;A4GL_set_status(0,0);
            A4GLSTK_setCurrentLine(_module_name,16);
            _retvars=aclfgl_fwd(0);
            /* pr 1 */
            CHECK_NO_RETURN;
            }
               ERR_CHK_ERROR  { A4GL_chk_err(16,_module_name); }
            /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_1= -4;goto MENU_START_1;  /* 1 */
         }
          if (cmd_no_1==4) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_CALL_CMD Line 18 E_MET_MAIN_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,18);
            aclfgli_clr_err_flg();
            /* done print expr */
            {int _retvars;A4GL_set_status(0,0);
            A4GLSTK_setCurrentLine(_module_name,18);
            _retvars=aclfgl_back(0);
            /* pr 1 */
            CHECK_NO_RETURN;
            }
               ERR_CHK_ERROR  { A4GL_chk_err(18,_module_name); }
            /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_1= -4;goto MENU_START_1;  /* 1 */
         }
          if (cmd_no_1==5) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_CALL_CMD Line 20 E_MET_MAIN_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,20);
            aclfgli_clr_err_flg();
            /* done print expr */
            {int _retvars;A4GL_set_status(0,0);
            A4GLSTK_setCurrentLine(_module_name,20);
            _retvars=aclfgl_back(0);
            /* pr 1 */
            CHECK_NO_RETURN;
            }
               ERR_CHK_ERROR  { A4GL_chk_err(20,_module_name); }
            /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_1= -4;goto MENU_START_1;  /* 1 */
         }
          if (cmd_no_1==6) {
            /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_1= -4;goto MENU_START_1;  /* 1 */
         }
          if (cmd_no_1==7) {
            /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_1= -4;goto MENU_START_1;  /* 1 */
         }
          if (cmd_no_1==8) {
            /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_1= -4;goto MENU_START_1;  /* 1 */
         }
          if (cmd_no_1==9) {
            /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_1= -4;goto MENU_START_1;  /* 1 */
         }
          if (cmd_no_1==10) {
            /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_1= -4;goto MENU_START_1;  /* 1 */
         }
          if (cmd_no_1==11) {
            /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_1= -4;goto MENU_START_1;  /* 1 */
         }
          if (cmd_no_1==12) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_EXT_CMD Line 27 E_MET_MAIN_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,27);
            cmd_no_1= -3;goto MENU_START_1;
            /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_1= -4;goto MENU_START_1;  /* 1 */
         }
          if (cmd_no_1==13) {
            A4GL_clr_ignore_error_list();
            /* CMD : E_CMD_EXT_CMD Line 29 E_MET_MAIN_DEFINITION */
            A4GLSTK_setCurrentLine(_module_name,29);
            cmd_no_1= -3;goto MENU_START_1;
            /* FORCE AROUND MENU LOOP AGAIN */ cmd_no_1= -4;goto MENU_START_1;  /* 1 */
         }
         if (cmd_no_1== -1) {
            char *_mntitle;
            char *_mnoptiontitle;
            /* PRINT EXPR */
            A4GL_push_char("Perform");
            /* END PRINT EXPR */
            _mntitle=A4GL_char_pop(); /* ... */
            m_1=(void *)A4GL_new_menu_create_with_attr(_mntitle,1,1,2,0,"","","","","");
            free(_mntitle);

            A4GL_add_menu_option(m_1, "Query","EMPTY","Searches the active database table",0,0);

            A4GL_add_menu_option(m_1, "","Q","",0,0);

            A4GL_add_menu_option(m_1, "page-fWd","EMPTY","",0,0);

            A4GL_add_menu_option(m_1, "","W","",0,0);

            A4GL_add_menu_option(m_1, "page-Back","EMPTY","",0,0);

            A4GL_add_menu_option(m_1, "","B","",0,0);

            A4GL_add_menu_option(m_1, "Next","EMPTY","",0,0);

            A4GL_add_menu_option(m_1, "Previous","EMPTY","",0,0);

            A4GL_add_menu_option(m_1, "View","EMPTY","",0,0);

            A4GL_add_menu_option(m_1, "Add","EMPTY","",0,0);

            A4GL_add_menu_option(m_1, "Update","EMPTY","",0,0);

            A4GL_add_menu_option(m_1, "Delete","EMPTY","",0,0);

            A4GL_add_menu_option(m_1, "Exit","EMPTY","",0,0);
            A4GL_add_menu_action(m_1, "fgl_exit_menu",13);
            A4GL_finish_create_menu(m_1);
            cmd_no_1= -2;
            continue;
         }
         CONTINUE_BLOCK_1:    ;

         A4GL_ensure_menu_option(0, m_1, "Query","EMPTY","Searches the active database table",0,0);

         A4GL_ensure_menu_option(1, m_1, "","Q","",0,0);

         A4GL_ensure_menu_option(2, m_1, "page-fWd","EMPTY","",0,0);

         A4GL_ensure_menu_option(3, m_1, "","W","",0,0);

         A4GL_ensure_menu_option(4, m_1, "page-Back","EMPTY","",0,0);

         A4GL_ensure_menu_option(5, m_1, "","B","",0,0);

         A4GL_ensure_menu_option(6, m_1, "Next","EMPTY","",0,0);

         A4GL_ensure_menu_option(7, m_1, "Previous","EMPTY","",0,0);

         A4GL_ensure_menu_option(8, m_1, "View","EMPTY","",0,0);

         A4GL_ensure_menu_option(9, m_1, "Add","EMPTY","",0,0);

         A4GL_ensure_menu_option(10, m_1, "Update","EMPTY","",0,0);

         A4GL_ensure_menu_option(11, m_1, "Delete","EMPTY","",0,0);

         A4GL_ensure_menu_option(12, m_1, "Exit","EMPTY","",0,0);
         cmd_no_1=A4GL_menu_loop_v2(m_1,0);
      }
      A4GL_free_menu(m_1);
   }
   END_BLOCK_1:    ;
      ERR_CHK_ERROR  { A4GL_chk_err(29,_module_name); }
   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CLEAR_CMD Line 30 E_MET_MAIN_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,30);
   aclfgli_clr_err_flg();
   A4GL_current_window("screen");
   A4GL_clr_window("screen");
      ERR_CHK_ERROR  { A4GL_chk_err(30,_module_name); }
   A4GLSTK_popFunction_nl(0, 32);
   A4GL_fgl_end_4gl_0();
   return 0;
}



A4GL_FUNCTION int aclfgl_query (int _nargs){
void *_blobdata=0;
static char *_functionName = "query";
char qry [200+1];

struct BINDING _fbind[1]={ /* 0 print_param_g */
{NULL,0,0,0,0,0,NULL}
};
char *_paramnames[1]={ /* 0 */ /*print_param_g */
0
};
 void *_objData[]={
NULL};
A4GLSTK_pushFunction_v2(_functionName,_paramnames,_nargs,_module_name,33,_objData);
if (_nargs!=0) {A4GL_set_status(-3002,0);A4GL_pop_args(_nargs);A4GLSTK_popFunction_nl(0,33);return -1;}

   init_module_variables();
   /* Print nullify */
   A4GL_setnull(0,&qry,0xc8); /*1 */
   {int _lstatus=a4gl_status;
   A4GL_pop_params_and_save_blobs(_fbind,0,&_blobdata);
   if (_lstatus!=a4gl_status) { A4GL_chk_err(33,_module_name);  }
   }

   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_CONSTRUCT_CMD Line 78 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,78);
   aclfgli_clr_err_flg();
   {
      static struct BINDING ibind[1]={

      {NULL,0,200,0,0,0,NULL}
      };
      struct s_constr_list constr_flds[42]={
      {"formonly","gateway_name"}
      ,
      {"formonly","interface_name"}
      ,
      {"formonly","ip_address"}
      ,
      {"formonly","type"}
      ,
      {"formonly","remote_ip_address"}
      ,
      {"formonly","vpi"}
      ,
      {"formonly","dlci_vci"}
      ,
      {"formonly","remote_vpi"}
      ,
      {"formonly","remote_dlci_vpi"}
      ,
      {"formonly","subnet_mask"}
      ,
      {"formonly","vlan"}
      ,
      {"formonly","smds_address"}
      ,
      {"formonly","role"}
      ,
      {"formonly","encapsulation"}
      ,
      {"formonly","purpose"}
      ,
      {"formonly","status"}
      ,
      {"formonly","available"}
      ,
      {"formonly","ticket"}
      ,
      {"formonly","comments1"}
      ,
      {"formonly","speed"}
      ,
      {"formonly","comments2"}
      ,
      {"formonly","limit"}
      ,
      {"formonly","circuit_id"}
      ,
      {"formonly","channel"}
      ,
      {"formonly","panel"}
      ,
      {"formonly","jack"}
      ,
      {"formonly","cabinet"}
      ,
      {"formonly","ne"}
      ,
      {"formonly","ne_tx"}
      ,
      {"formonly","ne_rx"}
      ,
      {"formonly","snmp"}
      ,
      {"formonly","duplex"}
      ,
      {"formonly","vpop"}
      ,
      {"formonly","drop_and_insert"}
      ,
      {"formonly","stats_name"}
      ,
      {"formonly","t1_agg_group"}
      ,
      {"formonly","ifindex"}
      ,
      {"formonly","isdn"}
      ,
      {"formonly","ping"}
      ,
      {"formonly","poll"}
      ,
      {"formonly","stats"}
      ,
      {"formonly","publicize"}
      };
      int _attr=-1;
      int _fld_dr= -100;int _exec_block= 0;char *_fldname;int _sf;
      char _sio_2[610]; char *_curr_win=0; char _inp_io_type='C'; char *_sio_kw_2="s_screenio";
      void *_filterfunc=NULL;
      int _forminit=1;
      static struct aclfgl_event_list _sio_evt[]={
      {-94,1,0,""},
       {0}};
      struct s_field_name _fldlist[]={
         {"gateway_name",1},
         {"interface_name",1},
         {"ip_address",1},
         {"type",1},
         {"remote_ip_address",1},
         {"vpi",1},
         {"dlci_vci",1},
         {"remote_vpi",1},
         {"remote_dlci_vpi",1},
         {"subnet_mask",1},
         {"vlan",1},
         {"smds_address",1},
         {"role",1},
         {"encapsulation",1},
         {"purpose",1},
         {"status",1},
         {"available",1},
         {"ticket",1},
         {"comments1",1},
         {"speed",1},
         {"comments2",1},
         {"limit",1},
         {"circuit_id",1},
         {"channel",1},
         {"panel",1},
         {"jack",1},
         {"cabinet",1},
         {"ne",1},
         {"ne_tx",1},
         {"ne_rx",1},
         {"snmp",1},
         {"duplex",1},
         {"vpop",1},
         {"drop_and_insert",1},
         {"stats_name",1},
         {"t1_agg_group",1},
         {"ifindex",1},
         {"isdn",1},
         {"ping",1},
         {"poll",1},
         {"stats",1},
         {"publicize",1},
         {NULL,0}
      };

      ibind[0].ptr=&qry;
      memset(_sio_2,0,sizeof(_sio_2));
      while(_fld_dr!=0){
         if (_exec_block == 0) {
            _curr_win=A4GL_get_currwin_name();
            SET("s_screenio",_sio_2,"vars",ibind);
            SET("s_screenio",_sio_2,"novars",42);
            SET("s_screenio",_sio_2,"attrib",_attr);
            SET("s_screenio",_sio_2,"currform",A4GL_get_curr_form(1));
            SET("s_screenio",_sio_2,"currentfield",0);
            SET("s_screenio",&_sio_2,"help_no",0);
            SET("s_screenio",_sio_2,"currentmetrics",0);
            SET("s_screenio",_sio_2,"constr",constr_flds);
            SET("s_screenio",_sio_2,"mode",3);
            SET("s_screenio",_sio_2,"processed_onkey",0);
            SET("s_screenio",_sio_2,"field_list",0);
            SET("s_screenio",&_sio_2,"current_field_display",A4GL_get_option_value('c'));
            SET("s_screenio",_sio_2,"callback_function", _filterfunc);
            SET("s_screenio",_sio_2,"nfields",A4GL_gen_field_chars((void ***)GETPTR("s_screenio",_sio_2,"field_list"),(void *)GET("s_screenio",_sio_2,"currform"),"gateway_name",0,"interface_name",0,"ip_address",0,"type",0,"remote_ip_address",0,"vpi",0,"dlci_vci",0,"remote_vpi",0,"remote_dlci_vpi",0,"subnet_mask",0,"vlan",0,"smds_address",0,"role",0,"encapsulation",0,"purpose",0,"status",0,"available",0,"ticket",0,"comments1",0,"speed",0,"comments2",0,"limit",0,"circuit_id",0,"channel",0,"panel",0,"jack",0,"cabinet",0,"ne",0,"ne_tx",0,"ne_rx",0,"snmp",0,"duplex",0,"vpop",0,"drop_and_insert",0,"stats_name",0,"t1_agg_group",0,"ifindex",0,"isdn",0,"ping",0,"poll",0,"stats",0,"publicize",0,NULL));
            _sf=A4GL_set_fields(&_sio_2); A4GL_debug("_sf=%d",_sf);if(_sf==0) {_fld_dr=0;break;}
            _fld_dr= -1;
         } /* end of initialization */
         if (_exec_block== 1) { break; }
         A4GL_ensure_current_window_is(_curr_win);
         _exec_block = A4GL_form_loop_v2(&_sio_2,_forminit,_sio_evt);_forminit=0;
         if (_exec_block>0) _fld_dr=A4GL_get_event_type(_sio_evt,_exec_block); else _fld_dr= -1;
         CONTINUE_BLOCK_2:    ;   /* add_continue */
      }
      END_BLOCK_2:    ;   /* add_continue */
       A4GL_push_constr(&_sio_2);
       A4GL_pop_params(ibind,1);
      A4GL_finish_screenio(&_sio_2,_sio_kw_2);
   }
      ERR_CHK_ERROR  { A4GL_chk_err(78,_module_name); }
   A4GLSTK_popFunction_nl(0,79);
   A4GL_copy_back_blobs(_blobdata,0);
   return 0;
}




A4GL_FUNCTION int aclfgl_fwd (int _nargs){
void *_blobdata=0;
static char *_functionName = "fwd";

struct BINDING _fbind[1]={ /* 0 print_param_g */
{NULL,0,0,0,0,0,NULL}
};
char *_paramnames[1]={ /* 0 */ /*print_param_g */
0
};
 void *_objData[]={
NULL};
A4GLSTK_pushFunction_v2(_functionName,_paramnames,_nargs,_module_name,80,_objData);
if (_nargs!=0) {A4GL_set_status(-3002,0);A4GL_pop_args(_nargs);A4GLSTK_popFunction_nl(0,80);return -1;}

   init_module_variables();
   /* Print nullify */
   {int _lstatus=a4gl_status;
   A4GL_pop_params_and_save_blobs(_fbind,0,&_blobdata);
   if (_lstatus!=a4gl_status) { A4GL_chk_err(80,_module_name);  }
   }

   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_DISPLAY_FORM_CMD Line 81 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,81);
   aclfgli_clr_err_flg();

   A4GL_disp_form("interfaces2",-1);
      ERR_CHK_ERROR  { A4GL_chk_err(81,_module_name); }
   A4GLSTK_popFunction_nl(0,83);
   A4GL_copy_back_blobs(_blobdata,0);
   return 0;
}




A4GL_FUNCTION int aclfgl_back (int _nargs){
void *_blobdata=0;
static char *_functionName = "back";

struct BINDING _fbind[1]={ /* 0 print_param_g */
{NULL,0,0,0,0,0,NULL}
};
char *_paramnames[1]={ /* 0 */ /*print_param_g */
0
};
 void *_objData[]={
NULL};
A4GLSTK_pushFunction_v2(_functionName,_paramnames,_nargs,_module_name,84,_objData);
if (_nargs!=0) {A4GL_set_status(-3002,0);A4GL_pop_args(_nargs);A4GLSTK_popFunction_nl(0,84);return -1;}

   init_module_variables();
   /* Print nullify */
   {int _lstatus=a4gl_status;
   A4GL_pop_params_and_save_blobs(_fbind,0,&_blobdata);
   if (_lstatus!=a4gl_status) { A4GL_chk_err(84,_module_name);  }
   }

   A4GL_clr_ignore_error_list();
   /* CMD : E_CMD_DISPLAY_FORM_CMD Line 85 E_MET_FUNCTION_DEFINITION */
   A4GLSTK_setCurrentLine(_module_name,85);
   aclfgli_clr_err_flg();

   A4GL_disp_form("interfaces1",-1);
      ERR_CHK_ERROR  { A4GL_chk_err(85,_module_name); }
   A4GLSTK_popFunction_nl(0,87);
   A4GL_copy_back_blobs(_blobdata,0);
   return 0;
}

/* END OF MODULE */
